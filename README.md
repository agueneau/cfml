% CFML : a characteristic formula generator for OCaml
% Arthur Charguéraud, with contributions from others.
% License : GNU GPL v3.

Description
===========

CFML consists of two parts: 
- a tool, implemented in OCaml, which parses OCaml source code
  and generates Coq files containing characteristic formulae;
- a Coq library, which exports definitions, lemmas and tactics 
  used to manipulate characteristic formulae.

Documentation
-

See doc/doc.html.

