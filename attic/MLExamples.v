(**

This file formalizes examples from standard Separation Logic, 
as described in Arthur Charguéraud's lecture notes.

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Require Import MLCF MLCFLifted.
Open Scope trm_scope.
Open Scope heap_scope.
Open Scope charac.

Ltac auto_star ::= jauto.



(*
Notation "[ ]" := nil (format "[ ]") : liblist_scope. 
Notation "[ x ]" := (cons x nil) : liblist_scope.
Notation "[ x ; y ; .. ; z ]" :=  (cons x (cons y .. (cons z nil) ..)) : liblist_scope.
Open Scope liblist_scope.
*)

(* ********************************************************************** *)
(* * Mutable lists *)

(** Layout in memory:

  type 'a mlist = { mutable hd : 'a ; mutable tl : 'a mlist }
  // empty lists represented using null

*)

Definition hd : field := 0%nat.
Definition tl : field := 1%nat.

Fixpoint MList (L:list val) (p:loc) : hprop :=
  match L with
  | nil => \[p = null]
  | x::L' => Hexists (p':loc), (p ~> record`{ hd := x; tl := val_loc p' }) \* (p' ~> MList L')
  end.

Section Properties.
Implicit Types L : list val.

(** Conversion lemmas for empty lists *)

Lemma MList_nil : forall p,
  p ~> MList nil = \[p = null].
Proof using. intros. xunfold~ MList. Qed.

Lemma MList_unnil : 
  \[] ==> null ~> MList nil.
Proof using. intros. rewrite MList_nil. hsimpl~. Qed.

Lemma MList_null : forall L,
  (null ~> MList L) = \[L = nil].
Proof using. 
  intros. destruct L.
  { xunfold MList. applys himpl_antisym; hsimpl~. }
  { xunfold MList. applys himpl_antisym. 
    { hpull ;=> p'. hchange (hrecord_not_null null); auto_false.
      hpull; auto_false. }
    { hpull. } } 
Qed.  

Lemma MList_null_keep : forall L,
  null ~> MList L ==>
  null ~> MList L \* \[L = nil].
Proof using. 
  intros. destruct L.
  { hsimpl~. }
  { rewrite MList_null. hsimpl. }
Qed.

(** Conversion lemmas for non-empty lists *)

Lemma MList_cons : forall p x L',
  p ~> MList (x::L') = 
  Hexists p', (p ~> record`{ hd := x; tl := val_loc p' }) \* p' ~> MList L'.
Proof using. intros. xunfold MList at 1. simple~. Qed.

Lemma MList_uncons : forall p p' x L',
  (p ~> record`{ hd := x; tl := val_loc p' }) \* p' ~> MList L' ==> 
  p ~> MList (x::L').
Proof using. intros. rewrite MList_cons. hsimpl. Qed.

Lemma MList_not_null_keep : forall p L,
  p <> null -> 
  p ~> MList L ==> p ~> MList L \* \[L <> nil].
Proof using. 
  intros. destruct L.
  { hchanges -> (MList_nil p). }
  { hsimpl. auto_false. }
Qed.

Lemma MList_not_null : forall p L,
  p <> null -> 
  p ~> MList L ==> Hexists x p' L',  
       \[L = x::L'] 
    \* (p ~> record`{ hd := x; tl := val_loc p' }) 
    \* p' ~> MList L'.
Proof using.
  intros. hchange~ (@MList_not_null_keep p). hpull. intros. 
  destruct L; tryfalse.
  hchange (MList_cons p). hsimpl~.
Qed.

End Properties.

Implicit Arguments MList_null_keep [].
Implicit Arguments MList_cons [].
Implicit Arguments MList_uncons [].
Implicit Arguments MList_not_null [].
Implicit Arguments MList_not_null_keep [].

Global Opaque MList.



(** Temporary auxiliary specification *)

Parameter rule_get_tl : forall p x p',
  triple (trm_app prim_get [val_loc p; val_field tl]) 
    (p ~> record `{ hd := x; tl := p'})
    (fun r => \[r = p'] \* p ~> record `{ hd := x; tl := p'}).
(* TODO: this specification will be computed on the fly using a tactic *)





(* ********************************************************************** *)
(* * Definition of the list increment program *)

(** Program variables *)

Definition Mlist_length : var := 1%nat.
Definition X : var := 3%nat.
Definition P : var := 4%nat.
Definition Q : var := 5%nat.
Definition N : var := 6%nat.

(** Program code *)

Definition mlist_length : val :=
  Fix Mlist_length [P] :=   
    Let X := trm_app prim_neq [val_var P; val_loc null] in
    If_ val_var X Then (
      Let Q := prim_get [val_var P; val_field tl] in
      Let N := trm_app (val_var Mlist_length) [val_var Q] in
      trm_app prim_add [val_var N; val_int 1] 
    ) Else (
      val_int 0
    ).

(** Helper for simplifying substitutions *)


Lemma mlist_length_closed : 
  closed_val mlist_length.
Proof using. reflexivity. Qed.

Lemma subst_val_mlist_length : forall E,
  subst_val E mlist_length = mlist_length.
Proof using. intros. applys subst_val_closed. applys mlist_length_closed. Qed.

Hint Rewrite subst_val_mlist_length : rew_subst.

(* short version to avoid stating the two lemmas above:
   Lemma subst_val_closed' : forall v, closed_val v -> forall E,
    subst_val E v = v.
   Proof using. intros. applys subst_val_closed. Qed.
   Hint Rewrite (@subst_val_closed' mlist_length (refl_equal _)) : rew_subst.
*)

Ltac simpl_subst := 
  simpl; autorewrite with rew_subst.

(* ********************************************************************** *)
(* * Mutable list increment verification, using triples *)

Section ProofWithTriples.

Lemma mlist_incr_spec : forall L p, 
  triple (trm_app mlist_length [val_loc p]) 
    (p ~> MList L)
    (fun r => \[r = val_int (length L)] \* p ~> MList L).
Proof using.
  intros L. induction_wf: list_sub_wf L. intros p. 
  applys rule_app; try reflexivity. 
  Transparent mlist_length.
  simpl_subst.
  Opaque mlist_length.
  applys rule_let. { xapplys rule_neq. }
  simpl_subst. intros X. xpull. intros EX. 
  applys rule_if_val. subst X.
  case_if as C1; case_if as C2; tryfalse.
  { inverts C2. xchange MList_null_keep. 
    xpull. intros EL. applys rule_val. hsimpl. subst~. }
  { xchange (MList_not_null p). { auto. } 
    xpull. intros x p' L' EL.
    applys rule_let.
    { xapplys rule_get_tl. }
    { simpl_subst. intros p''. xpull. intros E. subst p''. 
      applys rule_let. 
      { simpl_subst. xapplys IH. { subst~. } }
      { simpl_subst. intros r. xpull. intros Er. xchange (MList_uncons p). 
        subst r. xapplys rule_add. subst. rew_length. fequals. math. } } }
Qed.

End ProofWithTriples.


(* ********************************************************************** *)
(* * Demos of characteristic formulae *)

Module MLCFDemo.
Import MLCFTactics.
Open Scope charac.


Hint Extern 1 (RegisterSpec (app (val_prim prim_get) [_;val_field tl] _ _)) =>
  Provide rule_get_tl.


(* ********************************************************************** *)
(* * Mutable list increment verification, using characteristic formulae, 
     without tactics. *)

(** Observe how, in the proof below, the deep embedding is never revealed. *)

Lemma mlist_incr_spec' : forall L p, 
  app mlist_length [val_loc p]
    (p ~> MList L)
    (fun r => \[r = val_int (length L)] \* p ~> MList L).
Proof using.
  intros L. induction_wf: list_sub_wf L. intros p. 
  applys sound_for_cf_app1 20%nat. reflexivity.
  simpl; fold mlist_length. 
  applys local_erase. esplit. split. 
  { applys local_erase. xapplys rule_neq. }
  intros X. xpull. intros EX.
  subst X. applys local_erase.
  split; intros C; case_if as C'; clear C.
  { xchange (MList_not_null p). { auto. } 
    xpull. intros x p' L' EL.
    applys local_erase. esplit. split.
    { applys local_erase. xapplys rule_get_tl. }
    intros p''. xpull. intros E. subst p''.
    applys local_erase. esplit. split.
    { applys local_erase. xapplys IH. { subst~. } }
    { intros r. xpull. intros Er. xchange (MList_uncons p). 
      subst r. applys local_erase. xapplys rule_add.
      subst. rew_length. fequals. math. } }  
  { applys local_erase. unfolds. inverts C'. xchange MList_null_keep.  
    hpull. intros EL. hsimpl. subst~. }
Qed.


(* ********************************************************************** *)
(* * Mutable list increment verification, using characteristic formulae *)

(** Remark: in Ltac script below, "=>" is an alias for "intros" *)

Lemma mlist_incr_spec'' : forall L p, 
  app mlist_length [val_loc p]
    (p ~> MList L)
    (fun r => \[r = val_int (length L)] \* p ~> MList L).
Proof using.
  intros L. induction_wf: list_sub_wf L. intros p. xcf.
  xlet. { xapp. } 
  intros X. xpull ;=> EX.
  subst X. xif ;=> C; case_if as C'; clear C.
  { xchange (MList_not_null p). { auto. } xpull ;=> x p' L' EL.
    xlet. { xapp. }
    intros p''. xpull ;=> E. subst p''.
    xlet. { xapp IH. { subst~. } }
      { intros r. xpull ;=> Er. xchange (MList_uncons p). 
        subst r. xapp. subst. rew_length. fequals. math. } }  
  { xval. inverts C'. xchange MList_null_keep.  
    hpull ;=> EL. hsimpl. subst~. }
Qed.

End MLCFDemo.



(* ********************************************************************** *)
(* * Demos of characteristic formulae *)

Module MLCFLiftedDemo.
Import MLCFLiftedTactics.
Open Scope charac.



(* ********************************************************************** *)
(* * Mutable lists *)

(** Layout in memory:

  type 'a mlist = { mutable hd : 'a ; mutable tl : 'a mlist }
  // empty lists represented using null

*)



(** Record contents *)

(* TODO: use MLCFlifted *)

Notation "`'{ f1 := x1 }" := 
  ((f1, Dyn x1)::nil)
  (at level 0, f1 at level 0) 
  : trm_scope.
Notation "`'{ f1 := x1 ; f2 := x2 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::nil)
  (at level 0, f1 at level 0, f2 at level 0) 
  : trm_scope.
Notation "`'{ f1 := x1 ; f2 := x2 ; f3 := x3 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::(f3, Dyn x3)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0) 
  : trm_scope.
Notation "`'{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::(f3, Dyn x3)::(f4, Dyn x4)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0)
  : trm_scope.
Open Scope trm_scope.

(*
Definition hd : field := 0.
Definition tl : field := 1.
*)

Fixpoint MList A `{EA:Enc A} (L:list A) (p:loc) : hprop :=
  match L with
  | nil => \[p = null]
  | x::L' => Hexists (p':loc), (p ~> Record`'{ hd := x; tl := p' }) \* (p' ~> MList L')
  end.

Section Properties.
Context (A:Type) `{EA:Enc A}.
Implicit Types L : list A.
Implicit Types p : loc.

(** Conversion lemmas for empty lists *)

Lemma MList_nil : forall p,
  p ~> MList nil = \[p = null].
Proof using. intros. xunfold~ MList. Qed.

Lemma MList_unnil : 
  \[] ==> null ~> MList nil.
Proof using. intros. rewrite MList_nil. hsimpl~. Qed.

Lemma MList_null : forall L,
  (null ~> MList L) = \[L = nil].
Proof using. 
  intros. destruct L.
  { xunfold MList. applys himpl_antisym; hsimpl~. }
  { xunfold MList. applys himpl_antisym. 
    { hpull ;=> p'. hchange (hRecord_not_null null); auto_false.
      hpull; auto_false. }
    { hpull. } } 
Qed.  

Lemma MList_null_keep : forall L,
  null ~> MList L ==>
  null ~> MList L \* \[L = nil].
Proof using. 
  intros. destruct L.
  { hsimpl~. }
  { rewrite MList_null. hsimpl. }
Qed.

(** Conversion lemmas for non-empty lists *)

Lemma MList_cons : forall p x L',
  p ~> MList (x::L') = 
  Hexists p', (p ~> Record`'{ hd := x; tl := p' }) \* p' ~> MList L'.
Proof using. intros. xunfold MList at 1. simple~. Qed.

Lemma MList_uncons : forall p p' x L',
  (p ~> Record`'{ hd := x; tl := p' }) \* p' ~> MList L' ==> 
  p ~> MList (x::L').
Proof using. intros. rewrite MList_cons. hsimpl. Qed.

Lemma MList_not_null_keep : forall p L,
  p <> null -> 
  p ~> MList L ==> p ~> MList L \* \[L <> nil].
Proof using. 
  intros. destruct L.
  { hchanges -> (MList_nil p). }
  { hsimpl. auto_false. }
Qed.

Lemma MList_not_null : forall p L,
  p <> null -> 
  p ~> MList L ==> Hexists x p' L',  
       \[L = x::L'] 
    \* (p ~> Record`'{ hd := x; tl := p' }) 
    \* p' ~> MList L'.
Proof using.
  intros. hchange~ (@MList_not_null_keep p). hpull. intros. 
  destruct L; tryfalse.
  hchange (MList_cons p). hsimpl~.
Qed.

End Properties.

Implicit Arguments MList_null_keep [A [EA]].
Implicit Arguments MList_cons [A [EA]].
Implicit Arguments MList_uncons [A [EA]].
Implicit Arguments MList_not_null [A [EA]].
Implicit Arguments MList_not_null_keep [A [EA]].

Global Opaque MList.



(* ********************************************************************** *)
(* TODO: prove spec of primitives *)

Open Scope charac.

(** Args contents *)


Notation "`[ x1 x2 ]" :=
  ((Dyn x1)::(Dyn x2)::nil)
  (at level 0, x1 at level 0, x2 at level 0) 
  : trm_scope.
Notation "`[ x1 ]" := 
  ((Dyn x1)::nil)
  (at level 0, x1 at level 0) 
  : trm_scope.
(*
Notation "`'{ f1 := x1 ; f2 := x2 ; f3 := x3 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::(f3, Dyn x3)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0) 
  : trm_scope.
Notation "`'{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 }" :=
  ((f1, Dyn x1)::(f2, Dyn x2)::(f3, Dyn x3)::(f4, Dyn x4)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0)
  : trm_scope.
*)
Open Scope trm_scope.




Parameter Rule_add : forall (n1 n2:int),
  App prim_add `[n1 n2]
    \[] 
    (fun (r:int) => \[r = (n1 + n2)]).

Parameter Rule_neq : forall (v1 v2:loc),
  App prim_neq `[v1 v2]
    \[] 
    (fun (r:int) => \[r = (If v1 = v2 then 0 else 1)]).

Parameter Rule_get_tl : forall (p:loc) A `{EA:Enc A} (x:A) (p':loc),
  App prim_get `[p (tl:field)]
    (p ~> Record `'{ hd := x; tl := p'})
    (fun (r:loc) => \[r = p'] \* p ~> Record `'{ hd := x; tl := p'}).


Hint Extern 1 (RegisterSpec (App (val_prim prim_get) `[_ tl] _ _)) =>
  Provide Rule_get_tl.

Hint Extern 1 (RegisterSpec (App (val_prim prim_neq) `[_ _] _ _)) => 
  Provide Rule_neq.

Hint Extern 1 (RegisterSpec (App (val_prim prim_add) `[_ _] _ _)) =>
  Provide Rule_add.




(* ********************************************************************** *)
(* * Mutable list increment verification, using lifted characteristic formulae *)


Lemma mlist_incr_spec : forall A `{EA:Enc A} (L:list A) (p:loc), 
  App mlist_length `[p]
    (p ~> MList L)
    (fun (r:int) => \[r = length L] \* p ~> MList L).
Proof using.
  intros. gen p. induction_wf: list_sub_wf L; intros. xcf.
  xlet. { xapp. }
  intros X. xpull ;=> EX.
  subst X. xif ;=> C; case_if as C'; clear C.
  { xchange (MList_not_null p). { auto. } xpull ;=> x p' L' EL.
    xlet. { xapp. }
    intros p''. xpull ;=> E. subst p''.
    xlet. { xapp IH. { subst~. } }
      { intros r. xpull ;=> Er. xchange (MList_uncons p). 
        subst r. xapp. subst. rew_length. fequals. math. } }  
  { xval. xchanges MList_null_keep ;=> EL. hsimpl. subst~. }
Qed.



End MLCFLiftedDemo.




