(** 

This file formalizes characteristic formulae for
program verification using Separation Logic,
for the version that includes time credits.

Author: Arthur Charguéraud.
License: MIT.

*)


Set Implicit Arguments.
Require Export LibFix LambdaSepCredits.  (* MODIFIED FOR CREDITS *)
Open Scope heap_scope.

Implicit Types v w : val.


(* ********************************************************************** *)
(* * Type of a formula *)

(** A formula is a binary relation relating a pre-condition
    and a post-condition. *)

Definition formula := hprop -> (val -> hprop) -> Prop.

Global Instance Inhab_formula : Inhab formula.
Proof using. apply (prove_Inhab (fun _ _ => True)). Qed.


(* ********************************************************************** *)
(* * The [local] predicate *)


(** Nested applications [local] are redundant *)

Lemma local_local : forall B (F:~~B),
  local (local F) = local F.
Proof using.
  extens. intros H Q. iff M. 
  introv PH.
  lets (H1&H2&Q1&PH12&N&Qle): M (rm PH).
  lets (h1&h2&PH1&PH2&Ph12&Fh12): (rm PH12).
  lets (H1'&H2'&Q1'&PH12'&N'&Qle'): N (rm PH1).
  exists H1' (H2' \* H2) Q1'. splits.
  { rewrite <- hstar_assoc. exists~ h1 h2. }
  { auto. }
  { intros x. hchange (Qle' x). hchange (Qle x).
    rew_heap. rewrite~ htop_hstar_htop. }
  { apply~ local_erase. }
Qed.

(** A definition whose head is [local] satisfies [is_local] *)

Lemma is_local_local : forall B (F:~~B),
  is_local (local F).
Proof using. intros. unfolds. rewrite~ local_local. Qed.

Hint Resolve is_local_local.


(* ********************************************************************** *)
(* * Characteristic formula generator *)

(* ---------------------------------------------------------------------- *)
(* ** Input language for the characteristic formula generator,
      where functions are named by a let-binding. *)

(** Extended syntax of terms, with a new construct [Trm_let_fix]
    to represent the term [let rec f x = t1 in t2] *)

Inductive Trm : Type :=
  | Trm_val : val -> Trm
  | Trm_if_val : val -> Trm -> Trm -> Trm
  | Trm_let : var -> Trm -> Trm -> Trm 
  | Trm_let_fix : var -> var -> Trm -> Trm -> Trm 
  | Trm_app : val -> val -> Trm.

(** Definition of capture-avoiding substitution on [Trm] *)

Fixpoint Subst (y : var) (w : val) (t : Trm) : Trm :=
  match t with
  | Trm_val v => Trm_val v
  | Trm_if_val v t1 t2 => Trm_if_val v (Subst y w t1) (Subst y w t2)
  | Trm_let x t1 t2 => Trm_let x (Subst y w t1) (If x = y then t2 else Subst y w t2)
  | Trm_let_fix f x t1 t2 => Trm_let_fix f x 
      (if eq_var_dec x y then t1 else if eq_var_dec f y then t1 else Subst y w t1) 
      (if eq_var_dec f y then t2 else Subst y w t2)
  | Trm_app v1 v2 => Trm_app v1 v2
  end.

(** Translation from [Trm] to [trm], by encoding [Trm_let_fix]
    using [trm_let] and [val_fix]. *)

Fixpoint trm_of_Trm (t : Trm) : trm :=
  let aux := trm_of_Trm in
  match t with
  | Trm_val v => trm_val v
  | Trm_if_val v t1 t2 => trm_if v (aux t1) (aux t2)
  | Trm_let x t1 t2 => trm_let x (aux t1) (aux t2)
  | Trm_let_fix f x t1 t2 => trm_let f (trm_fix f x (aux t1)) (aux t2)
  | Trm_app v1 v2 => trm_app v1 v2
  end.

Coercion trm_of_Trm : Trm >-> trm.


(* ---------------------------------------------------------------------- *)
(** Size function used as measure for the CF generator:
    it computes the size of a term, where all values counting 
    for one unit, including closures viewed as values. *)

Fixpoint Trm_size (t:Trm) : nat :=
  match t with
  | Trm_val v => 1
  | Trm_if_val v t1 t2 => 1 + Trm_size t1 + Trm_size t2
  | Trm_let x t1 t2 => 1 + Trm_size t1 + Trm_size t2
  | Trm_let_fix f x t1 t2 => 1 + Trm_size t1 + Trm_size t2
  | Trm_app f v => 1
  end.

Lemma Trm_size_subst : forall x v t,
  Trm_size (Subst x v t) = Trm_size t.
Proof using.
  intros. induction t; simpl; repeat case_if; auto.
Qed.


(* ---------------------------------------------------------------------- *)
(* ** Definition of the [app] predicate *)

(** The proposition [app (f v) H Q] asserts that the application
    of [f] to [v] has [H] as pre-condition and [Q] as post-condition. *)

Definition app t H Q :=
  triple t H Q.


(* ---------------------------------------------------------------------- *)
(* ** Definition of CF blocks *)

(** These auxiliary definitions give the characteristic formula
    associated with each term construct. *)

Definition cf_val (v:val) : formula := fun H Q =>
  H ==> Q v.

Definition cf_if_val (v:val) (F1 F2:formula) : formula := fun H Q =>
  If (v = val_int 0) then F2 H Q else F1 H Q.

Definition cf_let (F1:formula) (F2of:val->formula) : formula := fun H Q =>
  exists Q1, 
      F1 H Q1 
   /\ (forall (X:val), (F2of X) (Q1 X) Q).

Definition cf_app (f:val) (v:val) : formula := fun H Q =>
  app (f v) H Q.

 (* MODIFIED FOR CREDITS *)  
Definition Tick (F:formula) := fun H Q => 
  exists H', pay_one H H' /\ F H' Q.

 (* MODIFIED FOR CREDITS *)  
Definition cf_fix (F1of:val->val->formula) 
                  (F2of:val->formula) : formula := fun H Q =>
  forall (F:val), 
  (forall X, (Tick (F1of F X)) ===> app (F X)) -> 
  (F2of F) H Q.


(* ---------------------------------------------------------------------- *)
(* ** Instance of [app] for primitive operations *)

Lemma app_ref : forall v,
  app (val_ref v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~~> v).
Proof using. applys rule_ref. Qed.

Lemma app_get : forall v l,
  app (val_get (val_loc l)) (l ~~~> v) (fun x => \[x = v] \* (l ~~~> v)).
Proof using. applys rule_get. Qed.

Lemma app_set : forall w l v, 
  app (val_set (val_loc l) w) (l ~~~> v) (fun r => \[r = val_unit] \* l ~~~> w).
Proof using. applys rule_set. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Definition of the CF generator *)

(** The CF generator is a recursive function, defined using the
    optimal fixed point combinator (from TLC). [cf_def] gives the
    function, and [cf] is then defined as the fixpoint of [cf_def]. 
    Subsequently, the fixed-point equation is established. *)

Definition cf_def cf (t:Trm) :=
  match t with
  | Trm_val v => local (cf_val v)
  | Trm_if_val v t1 t2 => local (cf_if_val v (cf t1) (cf t2))
  | Trm_let x t1 t2 => local (cf_let (cf t1) (fun X => cf (Subst x X t2)))
  | Trm_let_fix f x t1 t2 => local (cf_fix 
                                    (fun F X => cf (Subst f F (Subst x X t1)))  
                                    (fun F => cf (Subst f F t2)))
  | Trm_app f v => local (cf_app f v)
  end.

Definition cf := FixFun cf_def.

Ltac smath := simpl; math.
Hint Extern 1 (lt _ _) => smath.

Lemma cf_unfold : forall t,
  cf t = cf_def cf t.
Proof using.
  applys~ (FixFun_fix (measure Trm_size)). auto with wf.
  intros f1 f2 t IH. unfold measure in IH. unfold cf_def.
  destruct t; fequals.
  { rewrite~ IH. rewrite~ IH. }
  { rewrite~ IH. fequals.
    apply func_ext_1. intros X. rewrite~ IH. rewrite~ Trm_size_subst. }
  { fequals.
    { apply func_ext_2. intros F X. rewrite~ IH. do 2 rewrite~ Trm_size_subst. } 
    { apply func_ext_1. intros X. rewrite~ IH. rewrite~ Trm_size_subst. } }
Qed.

Ltac simpl_cf :=
  rewrite cf_unfold; unfold cf_def.


(* ********************************************************************** *)
(* * Soundness proof *)

(* ---------------------------------------------------------------------- *)
(* ** Two substitution lemmas for the soundness proof *)

Hint Extern 1 (measure Trm_size _ _) => hnf; simpl; math.

(** Substitution commutes with the translation from [Trm] to [trm] *)

Lemma subst_trm_of_Trm : forall y w (t:Trm),
  subst y w (trm_of_Trm t) = trm_of_Trm (Subst y w t).
Proof using.
  intros. induction t; simpl; auto.
  { rewrite IHt1, IHt2. auto. }
  { rewrite IHt1, IHt2. do 2 case_if~. }
  { rewrite IHt1, IHt2. do 2 case_if~. }
Qed.

(** The size of a [Trm] is preserved by substitution of 
    a variable by a value. *)

Lemma Trm_size_subst_value : forall y (w:val) (t:Trm),
  Trm_size (Subst y w t) = Trm_size t.
Proof using.
  intros. induction t; simpl; auto; repeat case_if; auto.
Qed.


(* ---------------------------------------------------------------------- *)
(* ** Soundness of the CF generator *)

Lemma is_local_cf : forall T,
  is_local (cf T).
Proof. intros. simpl_cf. destruct T; apply is_local_local. Qed.

Definition sound_for (t:trm) (F:formula) := 
  forall H Q, F H Q -> triple t H Q.

Lemma sound_for_local : forall t (F:formula),
  sound_for t F ->
  sound_for t (local F).
Proof using.
  unfold sound_for. introv SF. intros H Q M.
  rewrite is_local_triple. applys local_weaken_body M. applys SF.
Qed.

Notation "'trm_fix'' f x t1" := (trm_func (Rec f) x t1)
  (at level 69, f ident, x ident, t1 at level 0).

Lemma sound_for_cf : forall (t:Trm),
  sound_for t (cf t).
Proof using.
  intros t. induction_wf: Trm_size t. 
  rewrite cf_unfold. destruct t; simpl;
   applys sound_for_local; intros H Q P.
  { applys~ rule_val. }
  { hnf in P. applys rule_if_val. case_if; applys~ IH. }
  { destruct P as (Q1&P1&P2). applys rule_let Q1.
    { applys~ IH. }
    { intros X. rewrite subst_trm_of_Trm. 
      applys~ IH. hnf. rewrite~ Trm_size_subst_value. } } 
  { renames v to f, v0 to x. applys rule_let_fix. hnf in P.
    intros F HF. rewrite subst_trm_of_Trm. applys IH.
    { hnf. rewrite~ Trm_size_subst_value. }
    { applys P. intros X H' Q' (Q''&HP&HB). (* MODIFIED FOR CREDITS *)
      applys HF HP. 
      do 2 rewrite subst_trm_of_Trm. applys~ IH. 
      { hnf. rewrite Trm_size_subst_value.
       rewrite~ Trm_size_subst_value. } } }
  { applys P. }
Qed.

Theorem triple_of_cf : forall t H Q, 
  cf t H Q -> 
  triple t H Q.
Proof using. intros. applys* sound_for_cf. Qed.







