

(************************************************************)
(* ** [xgo] *)

Inductive Xhint_cmd :=
  | Xstop : Xhint_cmd
  | XstopNoclear : Xhint_cmd
  | XstopAfter : Xhint_cmd
  | XstopInside : Xhint_cmd
  | Xtactic : Xhint_cmd
  | XtacticNostop : Xhint_cmd
  | XtacticNoclear : Xhint_cmd
  | XsubstAlias : Xhint_cmd
  | XspecArgs : list Boxer -> list Boxer -> Xhint_cmd
  | Xargs : forall A, A -> Xhint_cmd
  | Xlet : forall A, A -> Xhint_cmd
  | Xlets : forall A, A -> Xhint_cmd
  | Xsimple : Xhint_cmd.

Inductive Xhint (a : tag_name) (h : Xhint_cmd) :=
  | Xhint_intro : Xhint a h.

Ltac add_hint a h :=
  let H := fresh "Hint" in
  lets H: (Xhint_intro a h).

Ltac clear_hint a :=
  match goal with H: Xhint a _ |- _ => clear H end.

Ltac clears_hint tt :=
  repeat match goal with H: Xhint _ _ |- _ => clear H end.

Ltac find_hint a :=
  match goal with H: Xhint a ?h |- _ => constr:(h) end.

Ltac xgo_default solver cont :=
  match ltac_get_tag tt with
  | tag_ret => xret; cont tt
  | tag_fail => xfail; cont tt
  | tag_done => xdone; cont tt
  | tag_apply => xapp
  | tag_seq => xseq; cont tt
  | tag_let_val => xval; cont tt
  | tag_let_trm => xlet; cont tt
  | tag_let_fun => fail
  | tag_body => fail
  | tag_letrec => fail
  | tag_case => xcases_real; cont tt 
  | tag_casewhen => fail 
  | tag_if => xif; cont tt
  | tag_alias => xalias; cont tt
  | tag_match ?n => xmatch; cont tt
  | tag_top_val => fail
  | tag_top_trm => fail
  | tag_top_fun => fail
  | tag_for => fail
  | tag_while => fail
  end.

Ltac xtactic tag := idtac.

Ltac run_hint h cont :=
  let tag := ltac_get_tag tt in
  match h with
  | Xstop => clears_hint tt; idtac
  | XstopNoclear => idtac
  | XstopAfter => 
      match tag with
      | tag_let_trm => fail (* todo: xlet_with cont ltac:(fun _ => idtac)*)
      | _ => xgo_default ltac:(fun _ => idtac) ltac:(fun _ => idtac) 
      end 
  | XstopInside => 
      match tag with
      | tag_let_trm => fail (*todo: xlet_with ltac:(fun _ => idtac) cont *)
      end
  | Xtactic => clears_hint tt; xtactic tag
  | XtacticNostop => xtactic tag; cont tt
  | XtacticNoclear => xtactic tag
  | XsubstAlias => xmatch_subst_alias; cont tt
  | Xargs ?E => 
      match tag with
      | tag_let_trm => fail (* todo!!*)
      | tag_apply => xapp E (*todo: not needed?*)
      end
  | XspecArgs (>> ?S) ?E => 
      match tag with
      | tag_let_trm =>  fail (* todo!!*)
      | tag_apply => xapp_spec S E (*todo: not needed?*)
      end 
  | Xlet ?S =>
     match tag with
     | tag_let_trm => xlet S; cont tt
     | tag_let_fun => xfun_noxbody S
     end
  | Xsimple => xmatch_simple; cont tt 
      (* todo : generalize
        | tag_case => xcases_real
        | tag_if => xif
        | tag_match ?n => xmatch
        *)
  end.

Ltac find_and_run_hint cont :=
  let a := ltac_get_label tt in
  let h := find_hint a in
  clear_hint a;
  first [ run_hint h cont | fail 1 ]. 

Tactic Notation "xhint" :=
  find_and_run_hint ltac:(fun _ => idtac).

Ltac xgo_core solver cont :=
  first [ find_and_run_hint cont
        | xgo_default solver cont ].

Ltac xgo_core_once solver :=
  xgo_core solver ltac:(fun _ => idtac).

Ltac xgo_core_repeat solver :=
  xgo_core solver ltac:(fun _ => instantiate; try solve [ solver tt ];
                          instantiate; try xgo_core_repeat solver).

Ltac xgo_pre tt :=
  first [ xcf; repeat progress(intros)
        | repeat progress(intros)
        | idtac ].

Ltac xgo_base solver :=
  xgo_pre tt; xgo_core_repeat solver.

Tactic Notation "xgo1" :=
  xgo_core_once ltac:(fun _ => idtac).

Tactic Notation "xgo" :=
  xgo_base ltac:(fun tt => idtac).
Tactic Notation "xgo" "~" := 
  xgo_base ltac:(fun tt => xauto~ ); instantiate; xauto~.
Tactic Notation "xgo" "*" := 
  xgo_base ltac:(fun tt => xauto* ); instantiate; xauto*. 

Tactic Notation "xgo" constr(a1) constr(h1) := 
  add_hint a1 h1; xgo.
Tactic Notation "xgo" constr(a1) constr(h1) "," constr(a2) constr(h2) := 
  add_hint a1 h1; add_hint a2 h2; xgo.
Tactic Notation "xgo" constr(a1) constr(h1) "," constr(a2) constr(h2) ","
  constr(a3) constr(h3) := 
  add_hint a1 h1; add_hint a2 h2; add_hint a3 h3; xgo.
Tactic Notation "xgo" constr(a1) constr(h1) "," constr(a2) constr(h2) ","
  constr(a3) constr(h3) "," constr(a4) constr(h4) := 
  add_hint a1 h1; add_hint a2 h2; add_hint a3 h3; add_hint a4 h4; xgo.

Tactic Notation "xgo" "~" constr(a1) constr(h1) := 
  add_hint a1 h1; xgo~.
Tactic Notation "xgo" "~" constr(a1) constr(h1) "," constr(a2) constr(h2) := 
  add_hint a1 h1; add_hint a2 h2; xgo~.
Tactic Notation "xgo" "~" constr(a1) constr(h1) "," constr(a2) constr(h2) ","
  constr(a3) constr(h3) := 
  add_hint a1 h1; add_hint a2 h2; add_hint a3 h3; xgo~.
Tactic Notation "xgo" "~" constr(a1) constr(h1) "," constr(a2) constr(h2) ","
  constr(a3) constr(h3) "," constr(a4) constr(h4) := 
  add_hint a1 h1; add_hint a2 h2; add_hint a3 h3; add_hint a4 h4; xgo~.

Tactic Notation "xgos" :=
  xgo; hsimpl.
Tactic Notation "xgos" "~" :=
  xgos; auto_tilde.
Tactic Notation "xgos" "*" :=
  xgos; auto_star.


