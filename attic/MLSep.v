(**

This file formalizes standard Separation Logic, like in 
ModelSepBasic, but for practical ML-style language rather 
than for a core lambda-calculus with references.

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Require Export MLSemantics SepFunctor.
Open Scope fmap_scope.

Ltac auto_star ::= jauto.


(* ********************************************************************** *)
(* * Construction of core of the logic *)

Module SepMLCore.


(* ---------------------------------------------------------------------- *)
(* ** Types *)

Definition heap : Type := (state)%type.

Definition hprop := heap -> Prop.


(* ---------------------------------------------------------------------- *)
(* ** Properties of heaps *)

(** Definitions used for uniformity with other
    instantiation of the functor *)

Notation "'heap_empty'" := (fmap_empty : heap) : heap_scope.
Open Scope heap_scope.

Notation "h1 \u h2" := (fmap_union h1 h2)
   (at level 51, right associativity) : heap_scope.

Definition heap_union_empty_l := fmap_union_empty_l.
Definition heap_union_empty_r := fmap_union_empty_r.
Definition heap_union_comm := fmap_union_comm_of_disjoint.


(* ---------------------------------------------------------------------- *)
(* ** Operators *)

(* \[] *)

Definition hempty : hprop := 
  fun h => h = heap_empty.

(* H1 \* H2 *)

Definition hstar (H1 H2 : hprop) : hprop := 
  fun h => exists h1 h2, H1 h1 
                      /\ H2 h2 
                      /\ (\# h1 h2)
                      /\ h = h1 \+ h2.

(* Hexists x, H *)

Definition hexists A (J : A -> hprop) : hprop := 
  fun h => exists x, J x h.

(* [P] *)

Definition hpure (P:Prop) : hprop := 
  hexists (fun (p:P) => hempty).

(* \Top = Hexists H, H *)

Definition htop := 
  hexists (fun (H:hprop) => H).


(* ---------------------------------------------------------------------- *)
(* ** Notation *)

Notation "\[]" := (hempty) 
  (at level 0) : heap_scope.
Notation "\[ P ]" := (hpure P) 
  (at level 0, P at level 99) : heap_scope.
Notation "H1 '\*' H2" := (hstar H1 H2)
  (at level 41, right associativity) : heap_scope.
Notation "Q \*+ H" := (fun x => hstar (Q x) H)
  (at level 40) : heap_scope.
Notation "\Top" := (htop) : heap_scope. 
Open Scope heap_scope.


(* ---------------------------------------------------------------------- *)
(* ** Tactic for automation *)

(* TODO: check how much is really useful *)

Hint Extern 1 (_ = _ :> heap) => fmap_eq.

Tactic Notation "fmap_disjoint_pre" :=
  subst; rew_disjoint; jauto_set.

Hint Extern 1 (\# _ _) => fmap_disjoint_pre.
Hint Extern 1 (\# _ _ _) => fmap_disjoint_pre.


(* ---------------------------------------------------------------------- *)
(* ** Properties of empty *)

Lemma hempty_intro : 
  \[] heap_empty.
Proof using. hnfs~. Qed.

Lemma hempty_inv : forall h,
  \[] h ->
  h = heap_empty.
Proof using. auto. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Properties of star *)

Section Properties.

Hint Resolve hempty_intro.

Lemma hstar_hempty_l : forall H,
  hempty \* H = H.
Proof using.
  intros. applys hprop_extens. intros h.
  iff (h1&h2&M1&M2&D&U) M.
  { forwards E: hempty_inv M1. subst.
    rewrite~ heap_union_empty_l. }
  { exists~ heap_empty h. }
Qed.

Lemma hstar_comm : forall H1 H2,
   H1 \* H2 = H2 \* H1.
Proof using. 
  intros H1 H2. unfold hprop, hstar. extens. intros h.
  iff (h1&h2&M1&M2&D&U); rewrite~ heap_union_comm in U; exists* h2 h1.
Qed.

Lemma hstar_assoc : forall H1 H2 H3,
  (H1 \* H2) \* H3 = H1 \* (H2 \* H3).
Proof using. 
  intros H1 H2 H3. applys hprop_extens. intros h. split.
  { intros (h'&h3&(h1&h2&M3&M4&D'&U')&M2&D&U). subst h'.
    exists h1 (h2 \+ h3). splits~. { exists* h2 h3. } }
  { intros (h1&h'&M1&(h2&h3&M3&M4&D'&U')&D&U). subst h'.
    exists (h1 \+ h2) h3. splits~. { exists* h1 h2. } }
Qed. 


(* ---------------------------------------------------------------------- *)
(* ** Interaction of star with other operators *)

Lemma hstar_hexists : forall A (J:A->hprop) H,
  (hexists J) \* H = hexists (fun x => (J x) \* H).
Proof using.
  intros. applys hprop_extens. intros h. iff M.
  { destruct M as (h1&h2&(x&M1)&M2&D&U). exists~ x h1 h2. }
  { destruct M as (x&(h1&h2&M1&M2&D&U)). exists h1 h2. splits~. exists~ x. }
Qed.

Lemma himpl_frame_l : forall H2 H1 H1',
  H1 ==> H1' -> 
  (H1 \* H2) ==> (H1' \* H2).
Proof using. introv W (h1&h2&?). exists* h1 h2. Qed.

Lemma local_local_aux : forall B,
  let local := fun (F:(hprop->(B->hprop)->Prop)) H Q =>
    ( forall h, H h -> exists H1 H2 Q1,
       (H1 \* H2) h
    /\ F H1 Q1
    /\ Q1 \*+ H2 ===> Q \*+ \Top) in
  (\Top \* \Top = \Top) ->
  forall F H Q,
  local (local F) H Q -> 
  local F H Q.
Proof using.
  intros B local R F H Q M. introv PH.
  lets (H1&H2&Q1&PH12&N&Qle): M (rm PH).
  lets (h1&h2&PH1&PH2&Ph12&Fh12): (rm PH12).
  lets (H1'&H2'&Q1'&PH12'&N'&Qle'): N (rm PH1).
  exists H1' (H2' \* H2) Q1'. splits.
  { rewrite <- hstar_assoc. exists~ h1 h2. }
  { auto. }
  { intros x. lets R1: himpl_frame_l \Top ((rm Qle) x).
    lets R2: himpl_frame_l H2 ((rm Qle') x).
    rewrite <- R. repeat rewrite hstar_assoc in *.
    applys himpl_trans R1. applys himpl_trans R2.
    rewrite~ (@hstar_comm H2). }
Qed.

End Properties.

End SepMLCore.


(* ********************************************************************** *)
(* * Properties of the logic *)

Module Export SepMLSetup := SepLogicSetup SepMLCore.


(* ---------------------------------------------------------------------- *)
(* ** Singleton heap *)

(** l . f ~> v *)

Definition hsingle (l:loc) (f:field) (v:val) : hprop := 
  fun h => h = fmap_single (l,f) v /\ l <> null.

(* Notation for parsing *)
Notation "l `.` f '~>' v" := (hsingle l f v)
  (at level 32, f at level 0, no associativity,
   format "l `.` f  '~>'  v") : heap_scope.

(* Optional notation for printing (note: doesn't work for parsing) 
Notation "l `. f '~>' v" := (hsingle l f v)
  (at level 32, f at level 0, no associativity,
   format "l `. f  '~>'  v") : heap_scope.
*)

(*
  Definition test_repr l f v := l `.` f ~> v.
  Print test_repr.
*)

Lemma hsingle_not_null : forall (l:loc) (f:field) (v:val),
  (l `.` f ~> v) ==> (l `.` f ~> v) \* \[l <> null].
Proof using.
  intros. unfold hsingle. intros h (Hh&Nl).
  exists h heap_empty. splits~. applys~ hpure_intro.
  applys~ hempty_intro.
Qed.

Implicit Arguments hsingle_not_null [].

Lemma hstar_single_same_loc_disjoint : forall (l:loc) (f:field) (v1 v2:val),
  (l `.` f ~> v1) \* (l`.`f ~> v2) ==> \[False].
Proof using.
  intros. unfold hsingle. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* fmap_disjoint_single_single_same_inv.
Qed.

Implicit Arguments hstar_single_same_loc_disjoint [].

Global Opaque hsingle.

(* ** Configure [hcancel] to make it aware of [hsingle] *)

Ltac hcancel_hook H ::= 
  match H with
  | hsingle _ _ => hcancel_try_same tt
  end.


(* ---------------------------------------------------------------------- *)
(* ** Record representation *)

(** Representation predicate [r ~> Record L], where [L]
    is an association list from fields to values. *)

Fixpoint record (L:record_fields) (r:loc) : hprop :=
  match L with
  | nil => \[]
  | (f, v)::L' => (r `.` f ~> v) \* (r ~> record L')
  end. 

Lemma hrecord_not_null : forall (r:loc) (L:record_fields),
  L <> nil ->
  (r ~> record L) ==> (r ~> record L) \* \[r <> null].
Proof using.
  intros. destruct L as [|(f,v) L']; tryfalse.
  xunfold record. hchange (hsingle_not_null r). hsimpl~.
Qed.

Global Opaque record.


(* ********************************************************************** *)
(* * Reasoning Rules *)


(* ---------------------------------------------------------------------- *)
(* ** Definition of triples *)

Definition triple t H Q :=
  forall H' h,  
  (H \* H') h -> 
  exists h' v, 
       red h t h' v
    /\ (Q v \* \Top \* H') h'.


(* ---------------------------------------------------------------------- *)
(* ** Structural rules *)

Lemma rule_extract_hexists : forall t (A:Type) (J:A->hprop) Q,
  (forall x, triple t (J x) Q) ->
  triple t (hexists J) Q.
Proof using.
  introv M. intros HF h N. rewrite hstar_hexists in N.
  destruct N as (x&N). applys* M.
Qed.

Lemma rule_extract_hprop : forall t (P:Prop) H Q,
  (P -> triple t H Q) ->
  triple t (\[P] \* H) Q.
Proof using.
  intros t. applys (rule_extract_hprop_from_extract_hexists (triple t)).
  applys rule_extract_hexists.
Qed.

Lemma rule_consequence : forall t H' Q' H Q,
  H ==> H' ->
  triple t H' Q' ->
  Q' ===> Q ->
  triple t H Q.
Proof using. 
  introv MH M MQ. intros HF h N.
  forwards (h'&v&R&K): (rm M) HF h. { hhsimpl~. }
  exists h' v. splits~. { hhsimpl~. }
Qed.

Lemma rule_frame : forall t H Q H',
  triple t H Q ->
  triple t (H \* H') (Q \*+ H').
Proof using. 
  introv M. intros HF h N. rewrite hstar_assoc in N.
  forwards (h'&v&R&K): (rm M) (H' \* HF) h. { hhsimpl~. }
  exists h' v. splits~. { hhsimpl~. }
Qed.

Lemma rule_htop_post : forall t H Q,
  triple t H (Q \*+ \Top) ->
  triple t H Q.
Proof using.
  introv M. intros HF h N. forwards* (h'&v&R&K): (rm M) HF h.  
  exists h' v. splits~. { rewrite <- htop_hstar_htop. hhsimpl. } 
Qed.

Lemma rule_htop_pre : forall t H Q,
  triple t H Q ->
  triple t (H \* \Top) Q.
Proof using.
  introv M. applys rule_htop_post. applys~ rule_frame.
Qed.


(* ---------------------------------------------------------------------- *)
(* ** Term rules *)

Lemma rule_val : forall v H Q,
  H ==> Q v ->
  triple (trm_val v) H Q.
Proof using.
  introv M. intros HF h N. exists h v. splits~.
  { applys red_val. }
  { hhsimpl. hchanges M. }
Qed.

Lemma rule_if_val : forall v t1 t2 H Q,
  triple (If v = val_int 0 then t2 else t1) H Q ->
  triple (trm_if (trm_val v) t1 t2) H Q.
Proof using.
Admitted. (* TODO *)

Lemma rule_if : forall t0 t1 t2 H Q0 Q,
  triple t0 H Q0 ->
  (forall (X:val), triple (trm_if X t1 t2) (Q0 X) Q) ->
  triple (trm_if t0 t1 t2) H Q.
Proof using.
Admitted. (* TODO *)
(* TODO
  introv M1 M2. intros HF h N.
  forwards* (h1'&v1'&R1&K1): (rm M1) HF h.
  forwards* (h2'&v2&R2&K2): (rm M2) (\Top \* HF) h1'.
  inverts R2 as R2'.
  exists h2' v2. splits~.
  { applys~ red_if R1. }
  { rewrite <- htop_hstar_htop. hhsimpl. }
*)

Lemma rule_seq' : forall t1 t2 H Q Q',
  triple t1 H Q' ->
  triple t2 (Q' val_unit) Q ->
  (forall v, Q' v ==> Q' v \* \[v = val_unit]) ->
  triple (trm_seq t1 t2) H Q.
Proof using.
  introv M1 M2 HQ'. intros HF h N.
  lets~ (h1'&v1&R1&K1): (rm M1) HF h.
  forwards* (h2'&v2&R2&K2): (rm M2) (\Top \* HF) h1'.
  { hhsimpl. hchange HQ'. hsimpl. hpull. intro_subst. hsimpl. }
  exists h2' v2. splits~.
  { applys~ red_seq R1 R2. }
  { rewrite <- htop_hstar_htop. hhsimpl. } 
Qed.

Lemma rule_seq : forall t1 t2 H Q H',
  triple t1 H (fun v => H') ->
  triple t2 H' Q ->
  triple (trm_seq t1 t2) H Q.
Proof using.
  introv M1 M2. intros HF h N.
  lets~ (h1'&v1&R1&K1): (rm M1) HF h.
  forwards* (h2'&v2&R2&K2): (rm M2) (\Top \* HF) h1'.
  exists h2' v2. splits~.
  { applys~ red_seq R1 R2. }
  { rewrite <- htop_hstar_htop. hhsimpl. } 
Qed.

Lemma rule_let : forall x t1 t2 H Q Q1,
  triple t1 H Q1 ->
  (forall (X:val), triple (subst_trm [(x,X)] t2) (Q1 X) Q) ->
  triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. intros HF h N.
  lets~ (h1'&v1&R1&K1): (rm M1) HF h.
  forwards* (h2'&v2&R2&K2): (rm M2) (\Top \* HF) h1'.
  exists h2' v2. splits~.
  { applys~ red_let R2. }
  { rewrite <- htop_hstar_htop. hhsimpl. } 
Qed.

Lemma rule_let_val : forall x v1 t2 H Q,
  (forall (X:val), X = v1 -> triple (subst_trm [(x,X)] t2) H Q) ->
  triple (trm_let x (trm_val v1) t2) H Q.
Proof using. 
  introv M. forwards~ M': (rm M).
  applys_eq~ (>> rule_let H (fun x => \[x = v1] \* H)) 2.
  { applys rule_val. hsimpl~. }
  { intros X. applys rule_extract_hprop. intro_subst. applys M'. }
Qed.

Lemma rule_while : forall t1 t2 H Q,
  (forall (k:trm),
     (forall H Q, triple (trm_if t1 (trm_seq t2 k) val_unit) H Q -> triple k H Q) ->
     triple k H Q) ->
  triple (trm_while t1 t2) H Q.
Proof using.
  introv M. applys (rm M). introv M.
  intros HF h N. lets~ (h'&v&R&K): (rm M) HF h.
  exists h' v. splits~.
  { applys~ red_while. }
Qed.

Lemma rule_app : forall f xs F Vs t1 H Q E,
  F = (val_fix f xs t1) ->
  List.length Vs = List.length xs ->
  E = List.combine (f::xs) (F::Vs) ->
  triple (subst_trm E t1) H Q ->
  triple (trm_app F Vs) H Q.
Proof using.
  introv EF EL EE M. subst F. intros HF h N.
  lets~ (h'&v&R&K): (rm M) HF h.
  exists h' v. splits~.
  { subst E. applys~ red_app. }
Qed.

Lemma rule_let_fix : forall f xs t1 t2 H Q,
  (forall (F:val), 
    (forall Xs H' Q' E, 
      List.length Xs = List.length xs ->
      E = List.combine (f::xs) (F::Xs) ->
      triple (subst_trm E t1) H' Q' ->
      triple (trm_app F Xs) H' Q') ->
    triple (subst_trm [(f,F)] t2) H Q) ->
  triple (trm_let f (trm_val (val_fix f xs t1)) t2) H Q.
Proof using.
  introv M. applys rule_let_val. intros F EF. 
  applys (rm M). clears H Q. intros X H Q E EL EE.
  applys* rule_app.
Qed.

(* Specification of primitive operations. *)
(* TODO *)

Parameter rule_add : forall n1 n2,
  triple (prim_add [val_int n1; val_int n2])
    \[] 
    (fun r => \[r = val_int (n1 + n2)]).

Parameter rule_eq : forall v1 v2, 
  triple (prim_eq [v1; v2])
    \[] 
    (fun r => \[r = val_int (If v1 = v2 then 1 else 0)]).

Parameter rule_neq : forall v1 v2,
  triple (prim_neq [v1; v2])
    \[] 
    (fun r => \[r = val_int (If v1 = v2 then 0 else 1)]).

(* Specification of record operations. *)
(* TODO *)

Parameter rule_new_record : forall (L:record_fields),
  triple (trm_new_record L) 
    \[] 
    (fun r => Hexists l, \[r = val_loc l] \* l ~> record L).

Parameter rule_get : forall l f v,
  triple (prim_get [val_loc l]) 
    (l `.` f ~> v) 
    (fun x => \[x = v] \* (l `.` f ~> v)).

Parameter rule_set : forall w l f v,
  triple (prim_set [val_loc l; w]) 
    (l `.` f ~> v) 
    (fun r => \[r = val_unit] \* (l `.` f ~> w)).




(* ---------------------------------------------------------------------- *)
(* ** Derived big-step specifications for get and set on records *)

(* TODO: 

  Definition field_eq_dec := Z.eq_dec.

  Fixpoint record_get_compute_dyn (f:field) (L:record_fields) : option val :=
    match L with
    | nil => None
    | (f',v')::T' => 
       if field_eq_dec f f' 
         then Some v' 
         else record_get_compute_dyn f T'
    end.

  Definition record_get_compute_spec (f:field) (L:record_fields) : option Prop :=
    match record_get_compute_dyn f L with
    | None => None 
    | Some v => Some (forall l,
       triple (prim_get [val_loc l; val_field f])
         (l ~> Record L) 
         (fun x => \[x = v] \* l ~> Record L))
    end.

  Lemma record_get_compute_spec_correct : forall f L (P:Prop),
    record_get_compute_spec f L = Some P -> P.
  (*
    introv M. unfolds record_get_compute_spec. 
    sets_eq <- do E: (record_get_compute_dyn f L).
    destruct do as [[T v]|]; inverts M.
    induction L as [|[f' [D' d']] T']; [false|]. 
    simpl in E.
    intros r. do 2 xunfold Record at 1. simpl. case_if.
    { inverts E.
      eapply local_frame. 
      { apply app_local. auto_false. }
      { apply record_get_spec. }
      { hsimpl. } 
      { hsimpl~. } }
    { specializes IHT' (rm E).
      eapply local_frame. 
      { apply app_local. auto_false. }
      { apply IHT'. }
      { hsimpl. } 
      { hsimpl~. } }
  Qed. (* todo: could use xapply in this proof *)
  *)

  Fixpoint record_set_compute_dyn (f:field) (v:val) (L:record_fields) : option record_descr :=
    match L with
    | nil => None
    | (f',v')::T' => 
       if field_eq_dec f f' 
         then Some ((f,v)::T') 
         else match record_set_compute_dyn f v T' with
              | None => None
              | Some L' => Some ((f',v')::L')
              end
    end.

  Definition record_set_compute_spec (f:field) (w:val) (L:record_fields) : option Prop :=
    match record_set_compute_dyn f w L with
    | None => None 
    | Some L' => Some (forall l,
       triple (prim_set [val_loc l; val_field f; w])
         (l ~> Record L) 
         (fun r => \[r = val_unit] \* l ~> Record L'))
    end.

  Lemma record_set_compute_spec_correct : forall f (w:val) L (P:Prop),
    record_set_compute_spec f w L = Some P -> P.
  (*
    introv M. unfolds record_set_compute_spec. 
    sets_eq <- do E: (record_set_compute_dyn f (dyn w) L).
    destruct do as [L'|]; inverts M. 
    gen L'. induction L as [|[f' [T' v']] T]; intros; [false|].
    simpl in E.
    xunfold Record at 1. simpl. case_if.
    { inverts E.
      eapply local_frame. 
      { apply app_local. auto_false. }
      { apply record_set_spec. }
      { hsimpl. } 
      { xunfold Record at 2. simpl. hsimpl~. } }
    { cases (record_set_compute_dyn f Dyn (w) T) as C; [|false].
      inverts E. specializes~ IHT r.
      eapply local_frame. 
      { apply app_local. auto_false. }
      { apply IHT. }
      { hsimpl. } 
      { xunfold Record at 2. simpl. hsimpl~. } }
  Qed. (* todo: could use xapply in this proof *)
  *)

  (** Auxiliary tactic to read the record state from the pre-condition *)

  Ltac xspec_record_repr_compute r H :=
    match H with context [ r ~> Record ?L ] => constr:(L) end.

  (** Tactic [xget] derives a record [get] specification *)

  Ltac xspec_record_get_compute_for f L :=
    let G := fresh in
    forwards G: (record_get_compute_spec_correct f L);
    [ reflexivity | revert G ].

  Ltac xspec_record_get_compute tt :=
    match goal with |- triple (trm_app (val_prim prim_get) [?r; ?f]) ?H _ =>
      let L := xspec_record_repr_compute r H in
      xspec_record_get_compute_for f L end.

  (** Tactic [sget] derives a record [set] specification *)

  Ltac xspec_record_set_compute_for f w L :=
    let G := fresh in
    forwards G: (record_set_compute_spec_correct f w L);
    [ reflexivity | revert G ].

  Ltac xspec_record_set_compute tt :=
    match goal with |- triple (trm_app (val_prim prim_set) [?r; ?f; ?w]) ?H _ =>
      let L := xspec_record_repr_compute r H in
      xspec_record_set_compute_for f w L end.

*)


(* ********************************************************************** *)
(* * Bonus *)

(* ---------------------------------------------------------------------- *)
(* ** Triples satisfy the [local] predicate *)

Lemma is_local_triple : forall t,
  is_local (triple t).
Proof using. 
  intros. applys prop_ext_2. intros H Q. iff M.
  { intros h Hh. forwards (h'&v&N1&N2): M \[] h. { hhsimpl. } 
    exists H \[] Q. splits~. { hhsimpl. } { hsimpl. } }
  { intros H' h Hh. lets (h1&h2&N1&N2&N3&N4): Hh. hnf in M.
    lets (H1&H2&Q1&R1&R2&R3): M N1.
    forwards (h'&v&S1&S2): R2 (H2\*H') h.
    { subst h. rewrite <- hstar_assoc. exists~ h1 h2. }
    exists h' v. splits~. rewrite <- htop_hstar_htop.
    applys himpl_inv S2.
    hchange (R3 v). rew_heap.
    rewrite (hstar_comm_assoc \Top H'). hsimpl. }  
Qed.

(** Make [xlocal] aware that triples are local 

Ltac xlocal_core tt ::=
  try first [ assumption | applys is_local_triple ].
*)


(* ---------------------------------------------------------------------- *)
(* ** Practical notation for triples *)

Notation "'PRE' H 'CODE' t 'POST' Q " :=
  (triple t H Q) (at level 69,
  format "'[v' '[' 'PRE'  H  ']'  '/' '[' 'CODE'  t  ']'  '/' '[' 'POST'  Q ']'  ']'")
  : heap_scope.








