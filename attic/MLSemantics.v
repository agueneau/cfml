(** 

This file describes the syntax and semantics of a
ML style calculus with mutable store. 

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Require Export LibCore Fmap.


(* ---------------------------------------------------------------------- *)
(* todo: reuse Coq's notation for lsits *)
Notation "'[' x1 ']'" := (x1::nil) : list_scope.
Notation "'[' x1 ; x2 ']'" := (x1::x2::nil) : list_scope.
Notation "'[' x1 ; x2 ; x3 ']'" := (x1::x2::x3::nil) : list_scope.
Notation "'[' x1 ; x2 ; x3 ; x4 ']'" := (x1::x2::x3::x4::nil) : list_scope.
Open Scope list_scope.


(* ********************************************************************** *)
(* * Source language syntax *)

(* ---------------------------------------------------------------------- *)
(** Representation of field names, variables and locations *)

Definition field := nat.
Definition fields := list field.

Definition var := nat.
Definition vars := list var.
Definition eq_var_dec := Nat.eq_dec.

Definition loc := nat.
Definition null : loc := 0%nat.

Global Opaque field var loc.


(* ---------------------------------------------------------------------- *)
(** Syntax of the source language *)

Inductive prim : Type :=
  | prim_alloc : prim
  | prim_get : prim
  | prim_set : prim
  | prim_add : prim
  | prim_eq : prim
  | prim_neq : prim

with val : Type := 
  | val_unit : val
  | val_int : int -> val
  | val_pair : val -> val -> val
  | val_var : var -> val
  | val_loc : loc -> val
  | val_prim : prim -> val
  | val_fix : var -> vars -> trm -> val

with trm : Type :=
  | trm_val : val -> trm
  | trm_if : trm -> trm -> trm -> trm
  | trm_seq : trm -> trm -> trm
  | trm_let : var -> trm -> trm -> trm
  | trm_app : val -> list val -> trm
  | trm_while : trm -> trm -> trm.

Definition vals := list val.


(** Auxiliary constructor to avoid mixing integers and fields 
    for record acccesses *)

(* LATER
Definition val_field (f:field) : val := 
  val_int (f:int).
*)

Notation "'val_field'" := val_int (only parsing).

(** Auxiliary constructor for records *)

Definition record_field : Type := field * val.
Definition record_fields : Type := list record_field.

Parameter trm_new_record : forall (L:record_fields), trm.
(* could be defined as a function if the language had lists,
   else needs to be viewed as a primitive. *)

(** Coercions *)

Coercion val_var : var >-> val.
Coercion val_int : Z >-> val.
Coercion val_loc : loc >-> val.
(* Coercion val_field : field >-> val. LATER *)
Coercion val_prim : prim >-> val.
Coercion trm_val : val >-> trm.
Coercion trm_app : val >-> Funclass.


(* ---------------------------------------------------------------------- *)
(** Computable version of list operations *)

Section Lists.

Fixpoint list_mem (y:var) (L:vars) :=
  match L with
  | nil => false
  | x::L' => if eq_var_dec x y then true else list_mem y L'
  end.

(** Remove [L] from [L'] *)
Fixpoint list_remove (L:vars) (L':vars) : vars := 
  match L' with
  | nil => nil
  | x::Q => let Q' := list_remove L Q in
            if list_mem x L then Q' else x::Q'
  end.

Fixpoint list_app (L:vars) (L':vars) : vars := 
  match L with
  | nil => L'
  | x::Q => x::(list_app Q L')
  end.

Local Infix "++" := list_app.

Fixpoint list_concat (Ls:list vars) : vars :=
  match Ls with
  | nil => nil
  | L::Ls' => L ++ (list_concat Ls')
  end.

End Lists.

Infix "++" := list_app : vars_scope.


(* ---------------------------------------------------------------------- *)
(** Computable version of association list operations *)

Definition ctx := list (var*val).

Fixpoint ctx_lookup (y:var) (E:ctx) : option val := 
  match E with
  | nil => None
  | (x,v)::E' => if eq_var_dec x y then Some v else ctx_lookup y E'
  end.

Fixpoint ctx_remove (L:vars) (E:ctx) : ctx := 
  match E with
  | nil => nil
  | (x,v)::E' => let E'' := ctx_remove L E' in
                 if list_mem x L then E'' else (x,v)::E''
  end.


(* ---------------------------------------------------------------------- *)
(** Definition of capture-avoiding substitution *)

Fixpoint subst_val (E:ctx) (v:val) : val :=
  match v with
  | val_var x => 
      match ctx_lookup x E with 
      | None => v 
      | Some w => w 
      end
  | val_fix f xs t => 
      let E' := ctx_remove (f::xs) E in 
      val_fix f xs (subst_trm E' t)
  | _ => v 
  end

with subst_trm (E:ctx) (t:trm) : trm :=
  match t with
  | trm_val v => trm_val (subst_val E v)
  | trm_if t0 t1 t2 => trm_if (subst_trm E t0) (subst_trm E t1) (subst_trm E t2)
  | trm_seq t1 t2 => trm_seq (subst_trm E t1) (subst_trm E t2)
  | trm_let x t1 t2 => let E' := (ctx_remove [x] E) in trm_let x (subst_trm E t1) (subst_trm E' t2)
  | trm_app v1 v2s => trm_app (subst_val E v1) (List.map (subst_val E) v2s)
  | trm_while t1 t2 => trm_while (subst_trm E t1) (subst_trm E t2)
  end.


(* ---------------------------------------------------------------------- *)
(** Free variables *)

Section Freevars.
Open Scope vars_scope.

Fixpoint freevars_val (v:val) : vars :=
  match v with
  | val_var x => x::nil
  | val_fix f xs t => list_remove (f::xs) (freevars_trm t)
  | _ => nil
  end

with freevars_trm (t:trm) : vars :=
  match t with
  | trm_val v => freevars_val v
  | trm_if t0 t1 t2 => freevars_trm t0 ++ freevars_trm t1 ++ freevars_trm t2
  | trm_seq t1 t2 => freevars_trm t1 ++ freevars_trm t2
  | trm_let x t1 t2 => freevars_trm t1 ++ (list_remove (x::nil) (freevars_trm t2))
  | trm_app v1 v2s => freevars_val v1 ++ list_concat (List.map freevars_val v2s)
  | trm_while t1 t2 => freevars_trm t1 ++ freevars_trm t2
  end.

End Freevars.

Definition closed_val (v:val) := 
  freevars_val v = nil.

Definition closed_trm (t:trm) := 
  freevars_trm t = nil.

Fixpoint val_size (v:val) : nat :=
  match v with
  | val_fix f xs t => 1 + trm_size t
  | _ => 1
  end

with trm_size (t:trm) : nat :=
  match t with
  | trm_val v => val_size v
  | trm_if t0 t1 t2 => 1 + trm_size t0 + trm_size t1 + trm_size t2
  | trm_seq t1 t2 => 1 + trm_size t1 + trm_size t2
  | trm_let x t1 t2 => 1 + trm_size t1 + trm_size t2
  | trm_app v1 v2s => 1 + val_size v1 + (List.fold_right (fun v n => (n + val_size v)%nat) O v2s)
  | trm_while t1 t2 => 1 + trm_size t1 + trm_size t2
  end.

Ltac simpl_meas := 
  match goal with |- measure _ _ _ => hnf; simpl; math end.

Ltac simpl_IH := 
  simpl; 
  match goal with H: context [subst_trm _ _ = _] |- _ =>
    repeat rewrite H;
    try simpl_meas end;
  try fequals.

Lemma ctx_lookup_remove : forall x L E, 
  ctx_lookup x E = None ->
  ctx_lookup x (ctx_remove L E) = None.
Proof using.
  intros. induction E as [| [a v] E']; simpls.
  { auto. }
  { do 2 case_if. { applys* IHE'. } { simpl. case_if. applys* IHE'. } }
Qed.

(* todo: mixed induction with subst_val *)
Lemma subst_trm_closed_ind : forall E t, 
  (forall x, list_mem x (freevars_trm t) -> ctx_lookup x E = None) ->
  subst_trm E t = t.
Proof using.
  intros E t. gen E. induction_wf IH: trm_size t.
  introv N. destruct t; simpls.
  { destruct v; simpls; auto.
    { specializes N v. destruct (ctx_lookup v E). { false* N. case_if~. } { auto. } }
    { do 2 fequals. simpl_IH. intros x K. tests: (list_mem x (v::v0)).
       { admit. }
       { applys ctx_lookup_remove. 
         (* skip_rewrite (ctx_lookup x (ctx_remove (v :: v0) E) = ctx_lookup x E). *)
         applys* N. admit. (* list_mem_remove *) } } } 
  { skip. }
  { simpl_IH. 
    { introv K. applys N. skip. } (* list_mem_app *)
    { introv K. applys N. skip. } (* list_mem_app *) }
  { skip. }
  { skip. }
  { skip. }
Qed.

(* TODO: rename remove to removes *)
(* TODO: ListExec library *)

Axiom subst_val_closed : forall E v,
  closed_val v -> 
  subst_val E v = v.

Lemma subst_trm_closed : forall E t, 
  closed_trm t -> 
  subst_trm E t = t.
Proof using.
  introv N. applys subst_trm_closed_ind. rewrite N. auto_false.
Qed.


(* ********************************************************************** *)
(* * Source language semantics *)


Section Red.
Local Open Scope fmap_scope.


Implicit Types t : trm.
Implicit Types v : val.


(* ---------------------------------------------------------------------- *)

Definition state := fmap (loc*field) val.

Definition fmap_fresh_loc l (m:state) :=
  l <> null /\ forall f v, \# (fmap_single (l,f) v) m.

Fixpoint fmap_record_key_values (l:loc) (ks:fields) (vs:vals) : state :=
  match ks,vs with
  | nil,nil => fmap_empty
  | (k::ks'),(v::vs') => fmap_union (fmap_single (l,k) v) (fmap_record_key_values l ks' vs')
  | _,_ => arbitrary
  end.

Parameter fmap_record : forall (l:loc) (n:Z), state.
(* todo: fold fmap_single *)


(* ---------------------------------------------------------------------- *)

Inductive red : state -> trm -> state -> val -> Prop :=
  | red_val : forall m v,
      red m v m v
  | red_if : forall m1 m2 m3 v r t0 t1 t2,
      red m1 t0 m2 v ->
      red m2 (If v = val_int 0 then t2 else t1) m3 r ->
      red m1 (trm_if t0 t1 t2) m3 r
  | red_seq : forall m1 m2 m3 t1 t2 v1 r,
      red m1 t1 m2 v1 ->
      red m2 t2 m3 r ->
      red m1 (trm_seq t1 t2) m3 r
  | red_let : forall m1 m2 m3 x t1 t2 v1 r,
      red m1 t1 m2 v1 ->
      red m2 (subst_trm [(x,v1)] t2) m3 r ->
      red m1 (trm_let x t1 t2) m3 r
  | red_app : forall m1 m2 v1 v2s f xs xvs t r,
      v1 = val_fix f xs t ->
      List.length xs = List.length v2s ->
      xvs = List.combine (f::xs) (v1::v2s) ->
      red m1 (subst_trm xvs t) m2 r ->
      red m1 (trm_app v1 v2s) m2 r
  | red_while : forall m1 m2 t1 t2 r,
      red m1 (trm_if t1 (trm_seq t2 (trm_while t1 t2)) val_unit) m2 r ->
      red m1 (trm_while t1 t2) m2 r
  | red_alloc : forall ma mb l (n:int),
      fmap_fresh_loc l ma ->
      mb = fmap_record l n ->
      red ma (prim_alloc [val_int n]) (mb \+ ma) (val_loc l)
  | red_get : forall m l k v,
      fmap_data m (l,k) = Some v ->
      red m (prim_get [val_field k; val_loc l]) m v
  | red_set : forall m m' l k v,
      m' = fmap_update m (l,k) v ->
      red m (prim_set [val_field k; val_loc l; v]) m' val_unit
  | red_add : forall m n1 n2 n',
      n' = n1 + n2 ->
      red m (prim_add [val_int n1; val_int n2]) m (val_int n')
  | red_eq : forall m v1 v2 n,
      n = (If v1 = v2 then 1 else 0) ->
      red m (prim_eq [v1; v2]) m (val_int n)
  | red_neq : forall m v1 v2 n,
      n = (If v1 = v2 then 0 else 1) ->
      red m (prim_neq [v1; v2]) m (val_int n).

End Red.

Local Open Scope fmap_scope.


(* TODO
Parameter red_new_record : forall kvs,
  red m (trm_new_record kvs) ...
*)


(* ---------------------------------------------------------------------- *)
(* ** Tactic [fmap_red] for proving [red] goals modulo 
      equalities between states *)

Ltac fmap_red_base tt ::=
  match goal with H: red _ ?t _ _ |- red _ ?t _ _ =>
    applys_eq H 2 4; try fmap_eq end.




(* ********************************************************************** *)
(* * Notations *)

Bind Scope trm_scope with trm.
Delimit Scope trm_scope with trm.

(** Core terms *)

Notation "'If_' x 'Then' F1 'Else' F2" :=
  (trm_if x F1 F2)
  (at level 69, x at level 0) : trm_scope.

Notation "'Let' x ':=' F1 'in' F2" :=
  (trm_let x F1 F2)
  (at level 69, x ident, right associativity,
  format "'[v' '[' 'Let'  x  ':='  F1  'in' ']'  '/'  '[' F2 ']' ']'") : trm_scope.

Notation "'Seq_' F1 ;; F2" :=
  (trm_seq F1 F2)
  (at level 68, right associativity,
   format "'[v' 'Seq_'  '[' F1 ']'  ;;  '/'  '[' F2 ']' ']'") : trm_scope.

Notation "F1 ;; F2" :=
  (trm_seq F1 F2)
  (at level 68, right associativity, only parsing,
   format "'[v' '[' F1 ']'  ;;  '/'  '[' F2 ']' ']'") : trm_scope.

Notation "'While' F1 'Do' F2 'Done_'" :=
  (trm_while F1 F2)
  (at level 69, F2 at level 68,
   format "'[v' 'While'  F1  'Do'  '/' '[' F2 ']' '/'  'Done_' ']'")
   : trm_scope.

(** Applications *)
(* LATER
Notation "'App' f x1 ;" :=
  (trm_app f [x1])
  (at level 68, f at level 0, x1 at level 0) : trm_scope.

Notation "'App' f x1 x2 ;" :=
  (trm_app f [x1; x2])
  (at level 68, f at level 0, x1 at level 0,
   x2 at level 0) : trm_scope.

Notation "'App' f x1 x2 x3 ;" :=
  (trm_app f [x1; x2; x3])
  (at level 68, f at level 0, x1 at level 0, x2 at level 0,
   x3 at level 0) : trm_scope.

Notation "'App' f x1 x2 x3 x4 ;" :=
  (trm_app f [x1; x2; x3; x4])
  (at level 68, f at level 0, x1 at level 0, x2 at level 0,
   x3 at level 0, x4 at level 0) : trm_scope.
*)

(** Recursive functions *)

Notation "'Fix' f [ x1 ] ':=' K" :=
  (val_fix f (x1:nil) K)
  (at level 69, f ident, x1 ident) : trm_scope.

Notation "'Fix' f [ x1 ] ':=' K" :=
  (val_fix f (x1::nil) K)
  (at level 69, f ident, x1 ident) : trm_scope.

Notation "'Fix' f [ x1 x2 ] ':=' K" :=
  (val_fix f (x1::x2::nil) K)
  (at level 69, f ident, x1 ident, x2 ident) : trm_scope.

Notation "'Fix' f [ x1 x2 x3 ] ':=' K" :=
  (val_fix f (x1::x2::x3::nil) K)
  (at level 69, f ident, x1 ident, x2 ident, x3 ident) : trm_scope.

Notation "'Fix' f [ x1 x2 x3 x4 ] ':=' K" :=
  (val_fix f (x1::x2::x3::x4::nil) K)
  (at level 69, f ident, x1 ident, x2 ident, x3 ident, x4 ident) : trm_scope.

(** Record operations *)

Notation "'Alloc' n" := 
  (trm_app prim_alloc n)
  (at level 69, no associativity, n at level 0,
   format "'Alloc'  n") : trm_scope.

Notation "'New' L" := 
  (trm_new_record L)
  (at level 69, no associativity, L at level 0,
   format "'New'  L") : trm_scope.

(** Notation for parsing *)
Notation "r `.` f" := 
  ((*trm_app*) prim_get [ r ; f ])
  (at level 69, no associativity, f at level 0,
   format "r `.` f") : trm_scope.

(** Notation for printing (note: doesn't seem to work for parsing) *)
Notation "r `. f" := 
  ((*trm_app*) prim_get [ r ; f ])
  (at level 69, no associativity, f at level 0,
   format "r `. f") : trm_scope.

(** Notation for parsing *)
Notation "r `.` f `<-` v" :=
  ((*trm_app*) prim_set [r ; f ; v ])
  (at level 69, no associativity, f at level 0,
   format "r `.` f `<-` v") : trm_scope.

(** Notation for printing (note: doesn't seem to work for parsing) *)
Notation "r `. f `<- v" :=
  ((*trm_app*) prim_set [r ; f ; v ])
  (at level 69, no associativity, f at level 0,
   format "r `. f `<- v") : trm_scope.

(* TESTING
  Open Scope trm_scope.
  Definition test_get r f := r `.` f.
  Definition test_set r f v := r `.` f `<-` v.
  Print test_get.
  Print test_set. (* note: format does not seem to be respected *)
*)


Notation "r `.` f" := 
  (trm_app prim_get [ r ; f ])
  (at level 69, no associativity, f at level 0) : trm_scope.

Notation "r `.` f `<- v" :=
  (trm_app prim_set [r ; f ; v ])
  (at level 69, no associativity, f at level 0) : trm_scope.

(*
Notation "r `. f" := 
  (trm_app prim_get [ r ; f ])
  (at level 69, no associativity, f at level 0, only parsing) : trm_scope.

Notation "r `. f `<- v" :=
  (trm_app prim_set [r ; f ; v ])
  (at level 69, no associativity, f at level 0, only parsing) : trm_scope.

Notation "'Get' r `. f" := 
  (trm_app prim_get [ r ; f ])
  (at level 69, no associativity, f at level 0,
   format "'Get'  r `. f") : trm_scope.

Notation "'Set' r `. f `<- v" :=
  (trm_app prim_set [r ; f ; v ])
  (at level 69, no associativity, f at level 0,
   format "'Set'  r `. f  `<-  v") : trm_scope.
*)


(** Record contents *)

Notation "`{ f1 := x1 }" := 
  ((f1, x1)::nil)
  (at level 0, f1 at level 0) 
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 }" :=
  ((f1, x1)::(f2, x2)::nil)
  (at level 0, f1 at level 0, f2 at level 0) 
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 }" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0) 
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 }" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::(f4, x4)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0)
  : trm_scope.

(* TODO: "f1 := x1" => record_entry f1 x1 := (f1,x1)  *)
