
Tactic Notation "exists_dep" "as" ident(E) :=
  match goal with |- exists (_:?T), _ => 
    assert (E:T); [ | exists E ] end.
Tactic Notation "exists_dep" "~" "as" ident(E) :=
  exists_dep as E; auto_tilde.
Tactic Notation "exists_dep" "*" "as" ident(E) :=
  exists_dep as E; auto_star.

Tactic Notation "exists_dep" :=
  let H := fresh "H" in exists_dep as H.
Tactic Notation "exists_dep" "~" :=
  exists_dep; auto_tilde.
Tactic Notation "exists_dep" "*" :=
  exists_dep; auto_star.

(* NOT NEEDED
  (** Equivalence with a direct definition of read-only singleton heap *)

  Definition hprop_single_ro (l:loc) (v:val) : hprop := 
    fun h =>
      let '(f,g) := heap_obj h in
      f = state_empty /\ g = state_single l v.

  Lemma RO_single : forall l v,
      (RO (hprop_single l v))
    = hprop_single_ro l v.
  Proof using. 
    Transparent hprop_single.
    intros. unfold hprop_single, hprop_single_ro.
    apply prop_ext_1. intros ((f,g)&D). 
    iff (M1&h1&g1&M2&(M3a&M3b)&M4) (M1&M2).
    { subst. split~. state_eq. }
    { split. auto. simpl. exists g f.
      exists_dep. state_disjoint.
      subst. splits~. state_eq. }
  Qed.
*)

(* NOT NEEDED
Lemma heap_union_transfer : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h1 (h2 \u h3).
Proof using.
  introv M1 M2. lets (N1&N2): heap_compat_union_l_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  

Lemma heap_union_transfer' : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h2 (h1 \u h3).
Proof using.
  introv M2 M1. lets (N1&N2): heap_compat_union_r_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  
*)
(* NOT NEEDED YET
Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* state_single_same_loc_disjoint.
Qed.
*)

(* NOT NEEDED YET
Lemma himpl_cancel : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using.
  introv W. intros ((f,g)&D) (f1&g1&f2&g2&D1&D2&M1&M2&M3&M4&M5).
  unf. exists f1 g1 f2 g2.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { applys_eq M2 1. fequals. }
  { applys W. applys_eq M3 1. fequals. }
Qed.
*)

(* NOT NEEDED?
Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
  introv M. intros ((f,g)&D) N. 
  exists f g state_empty state_empty. unf.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { split. 
    { applys state_agree_empty_r. }
    { state_disjoint. } }
  { applys M. applys_eq N 1. fequals. }
  { exists \[]. applys hprop_empty_prove. }
  { state_eq. }
  { state_eq. }
Qed.
*)






Lemma rule_get_traditional : forall v l,
  triple (trm_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* l ~~> v).
Proof using.
  (* LATER: derive this from above proof using frame-ro *)
  intros. intros h1 (E1&E2) h2 D. exists v h1. splits~.
  { rew_state; auto. applys red_get.
    { unstate. rewrite E1,E2. state_eq. }
    { unstate. rewrite E2. state_disjoint. } }
  { applys~ on_rw_sub_base. applys~ himpl_inst_prop (l ~~> v). split~. }
Qed.


Lemma rule_let_read_only : forall x t1 t2 H1 H2 Q Q1,
  triple t1 (H1 \* RO H2) Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X \* RO H2) Q) ->
  triple (trm_let x t1 t2) (H1 \* RO H2) Q.



(** More lemmas on [agree] *)

Lemma state_agree_transfer : forall f1 f2 f3,
  state_agree f1 f2 ->
  state_agree (f1 \+ f2) f3 ->
  state_agree f1 (f2 \+ f3).
Proof using.
  introv M1 M2. intros l v1 v2 E1 E2. 
  specializes M1 l v1 v2. specializes M2 l v1 v2.
  simpls. unfolds pfun_union.
  specializes M1 E1. rewrite E1 in M2. specializes~ M2 __.
  clear E1.
  cases (state_data f2 l). 
  { inverts E2. applys* M1. }
  { applys* M2. }
Qed.

Lemma state_agree_transfer' : forall f1 f2 f3,
  state_agree f1 f2 ->
  state_agree (f1 \+ f2) f3 ->
  state_agree f2 (f1 \+ f3).
Proof using.
  hint state_agree_sym.
  introv M1 M2. applys* state_agree_transfer.
  rewrite~ state_union_comm_agree. 
Qed.




Tactic Notation "exists_dep" "as" ident(E) :=
  match goal with |- exists (_:?T), _ => 
    assert (E:T); [ | exists E ] end.
Tactic Notation "exists_dep" "~" "as" ident(E) :=
  exists_dep as E; auto_tilde.
Tactic Notation "exists_dep" "*" "as" ident(E) :=
  exists_dep as E; auto_star.

Tactic Notation "exists_dep" :=
  let H := fresh "H" in exists_dep as H.
Tactic Notation "exists_dep" "~" :=
  exists_dep; auto_tilde.
Tactic Notation "exists_dep" "*" :=
  exists_dep; auto_star.

(* NOT NEEDED
  (** Equivalence with a direct definition of read-only singleton heap *)

  Definition hprop_single_ro (l:loc) (v:val) : hprop := 
    fun h =>
      let '(f,g) := heap_obj h in
      f = state_empty /\ g = state_single l v.

  Lemma RO_single : forall l v,
      (RO (hprop_single l v))
    = hprop_single_ro l v.
  Proof using. 
    Transparent hprop_single.
    intros. unfold hprop_single, hprop_single_ro.
    apply prop_ext_1. intros ((f,g)&D). 
    iff (M1&h1&g1&M2&(M3a&M3b)&M4) (M1&M2).
    { subst. split~. state_eq. }
    { split. auto. simpl. exists g f.
      exists_dep. state_disjoint.
      subst. splits~. state_eq. }
  Qed.
*)

(* NOT NEEDED
Lemma heap_union_transfer : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h1 (h2 \u h3).
Proof using.
  introv M1 M2. lets (N1&N2): heap_compat_union_l_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  

Lemma heap_union_transfer' : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h2 (h1 \u h3).
Proof using.
  introv M2 M1. lets (N1&N2): heap_compat_union_r_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  
*)
(* NOT NEEDED YET
Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* state_single_same_loc_disjoint.
Qed.
*)

(* NOT NEEDED YET
Lemma himpl_cancel : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using.
  introv W. intros ((f,g)&D) (f1&g1&f2&g2&D1&D2&M1&M2&M3&M4&M5).
  unf. exists f1 g1 f2 g2.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { applys_eq M2 1. fequals. }
  { applys W. applys_eq M3 1. fequals. }
Qed.
*)

(* NOT NEEDED?
Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
  introv M. intros ((f,g)&D) N. 
  exists f g state_empty state_empty. unf.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { split. 
    { applys state_agree_empty_r. }
    { state_disjoint. } }
  { applys M. applys_eq N 1. fequals. }
  { exists \[]. applys hprop_empty_prove. }
  { state_eq. }
  { state_eq. }
Qed.
*)






Lemma state_agree_union_lr_inv_agree_agree_disjoint : forall f1 f2 f3,
  state_agree (f1 \+ f2) f3 ->
  state_disjoint f1 f2 ->
  state_agree f2 f3.
Proof using.
  introv M D. rewrite~ (@state_union_comm_disjoint f1 f2) in M.
  applys* state_agree_union_ll_inv.
Qed.

Lemma state_agree_union_rr_inv_agree_disjoint : forall f1 f2 f3,
  state_agree f1 (f2 \+ f3) ->
  state_disjoint f2 f3 ->
  state_agree f1 f3.
Proof using.
  hint state_agree_union_lr_inv_agree_agree, state_disjoint, state_agree_sym. 
  eauto.
Qed.