


(* ********************************************************************** *)
(** * General definition of extensionality *)

(** The property [Extensional A] captures the fact that the type [A]
    features an extensional equality, in the sense that to prove the
    equality between two values of type [A] it suffices to prove that
    those two values are related by some binary relation. *)

Class Extensional (A:Type) := {
  extensional_hyp : A -> A -> Prop;
  extensional : forall x y : A, extensional_hyp x y -> x = y }.





