frederic.besson@inria.fr:

git clone https://gforge.inria.fr/git/ppsimpl/micromega.git

c’est un plugin micromega pour 8.5

Require Import Micromega.Lia

Il y a 2 options

Unset Lia Enum.
Pas de preuve par énumération.
Le prouveur devient incomplet sur Z — il raisonne comme si il était sur R.
Ça suffit pour ton but et je pense dans la plupart des cas.

Set Lia Depth n.
Qui limite la taille des preuve intermédaires.
Ton but passe avec profondeur 6…
Et ça échoue presque instantanément…

