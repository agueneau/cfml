exception EmptyStructure
exception BrokenInvariant
exception OutOfBound

val (!$) : 'a Lazy.t -> 'a
