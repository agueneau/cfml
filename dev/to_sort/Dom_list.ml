
type node = (node list) ref 

let c l : node =
  ref l

let rec find p (n:node) =
  match p with
  | [] -> n
  | x::p' -> find p' (List.nth !n x)

let append_child (n:node) m =
  n := !n @ [m]

let replace_child (n:node) i (m:node) =
  let rec aux i l = 
    match l with
    | [] -> failwith "error"
    | x::l' -> if i = 0 then m::l' else x::(aux (i-1) l')
    in
  n := aux i !n

let size (n:node) =
  let s = ref 0 in 
  let rec aux (n:node) : unit =
    incr s;
    List.iter aux !n
    in
  aux n;
  !s

let demo () =
  let r = c [ c[]; c[ c[ c[]; c[] ] ] ] in
  let s1 = size r in (* 6 *)
  let a = find [1;1] r in
  let b = find [1] r in
  append_child b a;
  replace_child b 1 (c[]);
  let s2 = size r in (* 7 *)
  (s1,s2) 


