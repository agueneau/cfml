#!/bin/bash

pwd
COQBIN=
if [ -f ./settings.sh ]
then
    source settings.sh 
fi
if [ -f ../settings.sh ]
then
    source ../settings.sh 
fi
if [ -f ../../settings.sh ]
then
    source ../../settings.sh 
fi
# TODO: improve with a while loop?


#echo coqbin=${COQBIN}

# todo: generalize to all
FILE=$1
if [ -f ${FILE} ]; then
    FILE=$*
else
    FILE=${FILE/.\//}
    FILE=`find . -name ${FILE}`
fi

COQINCLUDE="-R ${TLC} TLC -R ${CFML}/lib/coq CFML"
PWD=`pwd`

if [[ ${FILE} == *"examples/"* ]]
then
  EXAMPLE_PATH="$(dirname ${FILE})"
  COQINCLUDE="${COQINCLUDE} -R ${EXAMPLE_PATH} EXAMPLE"
fi
if [[ ${PWD} == *"examples/"* ]]
then
  EXAMPLE_PATH="${PWD}"
  COQINCLUDE="${COQINCLUDE} -R ${EXAMPLE_PATH} EXAMPLE"
fi

echo ${COQBIN}coqide ${COQINCLUDE} ${FILE}
${COQBIN}coqide ${COQINCLUDE} ${FILE}


#-dont-load-proofs  -async-proofs-j 1



