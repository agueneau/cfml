

let rec facto_rec n =
  if n <= 1 then 1 else n * (facto_rec (n-1))



let rec facto_tailrec_aux acc i n =
  if i = n then acc else facto_tailrec_aux ((i+1) * acc) (i+1) n

let facto_tailrec n = 
  facto_tailrec_aux 1 0 n



let facto_imp n =
  let m = ref 1 in
  let r = ref 1 in
  while !m <= n do
    r := !m * !r;
    incr m;
  done;
  !r





let facto_imp_down n =
  let m = ref n in
  let r = ref 1 in
  while !m > 1 do
    r := !m * !r;
    decr m;
  done;
  !r


let rec facto_tailrec_aux_down acc n =
  if n <= 1 then acc else facto_tailrec_aux_down (n * acc) (n-1)

let facto_tailrec_down n = 
  facto_tailrec_aux_down 1 n
