Set Implicit Arguments.
Require Import CFLib DynArray_ml Facts.

Generalizable Variables A.


(*******************************************************************)
(** * Instantiations *)

Notation "'RecDynArray'" := (Dyn_array Id Id Id).


(*******************************************************************)
(** * Invariants *)

Record inv (A:Type) {IA:Inhab A} (L:list A) size (D:list A) := build_inv {
  inv_size: size = len L;
  inv_items: forall i, index size i -> D[i] = L[i];
  inv_fit: len D >= size;
  inv_capa: len D > 0 }.

Definition hinv (A:Type) {IA:Inhab A} (L:list A) size data (default:A) D t :=
     t ~> RecDynArray default size data 
  \* data ~> Arr D 
  \* [inv L size D].

Definition DynArray (A:Type) {IA:Inhab A} (L:list A) (t:dyn_array A) :=
  Hexists size data default D, t ~> hinv L size data default D.


(*******************************************************************)
(** * Focus/unfocus lemmas *)

Lemma hinv_unfocus : forall t (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  inv L size D ->
  (t ~> RecDynArray default size data \* data ~> Arr D)
     ==> (t ~> hinv L size data default D).
Proof using.
  introv I. xunfold hinv. xsimpl. auto.
Qed.

Implicit Arguments hinv_unfocus [ A [IA] ].

Lemma DynArray_unfocus : forall t (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D,
  (t ~> hinv L size data default D)
     ==> (t ~> DynArray L).
Proof using.
  introv. xunfold DynArray. xsimpl.
Qed.

Implicit Arguments DynArray_unfocus [ A [IA] ].


(*******************************************************************)
(** * Verification *)


Lemma length_spec : forall A,
  Spec length_ (t:dyn_array A) |B>>
    forall {IA:Inhab A} (L:list A),
    keep B (t ~> DynArray L) (fun (x:int) => [x = len L]).
Proof using.
  xcf. introv.
  (* marche pas: unfold DynArray. 
     ou alors il faut rajouter "hdata_simpl." après *)
  (* maintenant, hunfold marche, grace au implicit arguments *)
  xunfold DynArray. xextract as size data default D.
  xunfold hinv at 1. xextract as IL.
  xapps. intros l. xextracts. xchange* (hinv_unfocus t).
  xsimpl. 
   (*unfold inv in IL.*) inverts IL. auto.
Qed.

Hint Extern 1 (RegisterSpec length_) => Provide length_spec.

Lemma transfer_spec : forall A,
  Spec transfer src dst nb |B>>
    forall {IA:Inhab A} (L1:list A) (L2:list A),
    len L1 >= nb -> len L2 >= nb -> nb >= 0 ->
    B (src ~> Arr L1 \* dst ~> Arr L2)
      (# Hexists L2', src ~> Arr L1
      \* dst ~> Arr L2'
      \* [len L2' = len L2] \* [forall k, index nb k -> L2'[k] = L1[k]]).
Proof using.
  intros. xcf. introv LL1 LL2 Nb.
  xfor. xname_post QF.
  cuts M: (forall i,
    S i (Hexists L2', [0 <= i <= nb] \*
          src ~> Arr L1 \*
          dst ~> Arr L2' \*
          [len L2' = len L2] \*
          [(forall k, index i k -> L2'[k] = L1[k])])
        QF).
   xapply M. xsimpl. math. auto. autos~. xsimpl.
   intros i. induction_wf IH: (int_upto_wf (nb+1)) i.
   hide IH. apply HS. clear HS. split. clearbody QF.  
     introv Inb. xextract as L2' Ii LL2' VL2'. xseq.
      xapps~. xapps~. show IH. xapply IH.
        autos~.
        xsimpl. 
          introv Ik. rew_arr. case_If. subst~. autos~.
          rew_arr~.
          autos~.
       xsimpl.
     introv Inb. xret. xextract as IM L2' EL2' VL2'.
      subst QF. xsimpl. (* auto~. ==> introv G. apply~ VL2'. *)
        asserts_rewrite~ (nb = i).
        auto.
Qed.

Hint Extern 1 (RegisterSpec transfer) => Provide transfer_spec.

Lemma resize_spec : forall A,
  Spec resize (t:dyn_array A) (new_capa:int) |B>>
    forall (L:list A) s d def D {IA:Inhab A},
    s >= 0 -> new_capa >= s -> new_capa > 0 ->
    B (t ~> hinv L s d def D)
      (# Hexists d' D', t ~> hinv L s d' def D' \* [len D' = new_capa]).
Proof using.
  xcf. introv Is Ic1 Ic2. xunfold hinv at 1. xextract as R.
  xapps. xapps. xapps~.
  rename _x2 into d'. intros D' (LD'&_). xapps. xapps. xapps. 
  invert R. introv Rs Ri Rf Rc. xapp~.
  xextract as D'' LD'' HD''. xchange* (hinv_unfocus t); [ | xsimpl~ ].
    constructor~. introv Ii. rewrite~ HD''.
    (* rewrite HD''; [ |auto]. rewrite~ Ri. *)
Qed.

Hint Extern 1 (RegisterSpec resize) => Provide resize_spec.

Lemma pop_spec : forall A,
  Spec pop (t:dyn_array A) |B>>
    forall (L:list A) (x:A) {IA:Inhab A},
    B (t ~> DynArray (L&x)) (fun y => [y = x] \* (t ~> DynArray L)).
Proof using.
  intros. xcf. intros. xunfold DynArray. sets_eq four: 4.
  xextract as s d def D. xunfold hinv at 1. xextract as ID. 
  destruct ID as [IDs IDi IDf IDc]. rew_list in IDs. 
  xapps. xname_pre Hcur. xseq Hcur.
  xif as C.
    xfail. math.
    xrets. 
  subst Hcur. 
  autos~.
  xapps. xapps. xapps. xapps. xapps~. xapps. xapps. xapps.
  xchange~ (hinv_unfocus t L). constructor~.
    introv Isi. rewrite IDi. rew_arr.
   case_If. false. math. auto. autos~.
(* TODO COQBUG
  xseq (# Hexists data' D', t ~> hinv L (s - 1) data' def D').
  xif as C.
    xapps (s - 1). auto. autos~. subst four. skip. autos~. intros (Is'&Gs').
    xapps~.
    xsimpl. 
    xrets. intros data' D'.
  xrets. rewrite IDi. rew_arr. case_If. auto. false. math. autos~.
*)
Qed.

Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.

Lemma push_spec : forall A,
  Spec push (t:dyn_array A) (x:A) |B>>
    forall {IA:Inhab A} (L:list A),
    B (t ~> DynArray L) (# t ~> DynArray (L&x)).
Proof using.
  xcf. intros. xunfold DynArray. sets_eq two: 2. xextract as size data def D.
  xunfold hinv at 1. xextract. introv IL. xapps. xapps. xapps. 
  xchange~ (hinv_unfocus t L). 
  xseq (Hexists data' D', t ~> hinv L size data' def D' 
         \* [len D' > size]). 
    lets IL2: IL. destruct IL as [ILs ILi ILf ILc]. xif. subst two.
      xapps~. xsimpl~. 
      xret. xsimpl~.
    intros t' D' I1. xunfold hinv at 1. xextract as ID'.
    destruct ID' as [IL's IL'i IL'f IL'c]. 
    xapps. xapps. xseq. xapps~. xapps. xapps.
    xchange (hinv_unfocus t); [ | xsimpl ]. constructor.
    rew_list~.
    introv Ii. rew_arr. case_If; case_If; autos~.
    rew_arr~.
    rew_arr~.
Qed.

Hint Extern 1 (RegisterSpec push) => Provide push_spec.


Lemma make_spec : forall A,
  Spec make (n:int) (def:A) |B>>
    forall {IA:Inhab A}, n >= 0 ->
    B \[] (fun t => Hexists L, t ~> DynArray L \* 
          [len L = n] \* [forall i, index n i -> L[i] = def]).
Proof using.
(* TODO COQBUG
  xcf. introv In. xapps. autos~. intros D (LD&VD).
  xapps. intros t.
  lets (X&D'&E): get_last D.
    intros E. lets LD': LibList.length_nil A. subst D. math.
    xchange~ (hinv_unfocus t D'). subst D. constructor~.
      autos*. (* rew_list in LD. auto~. *)
      introv Ii. rew_arr. case_If. invert Ii as Ii1 Ii2. rew_list in LD. subst i. false. math.
      auto.
    xchange (DynArray_unfocus t). subst D. rew_list in LD. xsimpl.
      (* todo: essayer d'utiliser "rewrites <- (>> VD i)" pour éviter le assert. *)
      introv Ii. asserts ED: ((D' & X)[i] = def). auto~. subst def. rew_arr.
       case_If. subst i. invert Ii as Ii1 Ii2. false. math. auto.
      autos~.
*)
Qed.


Lemma is_empty_2_spec : forall A,
  Spec is_empty_2 t |B>>
    forall {IA:Inhab A} (L:list A),
    keep B (t ~> DynArray L) (fun (x:bool) => [x = isTrue (L = nil)]).
Proof using.
  xcf. introv. xunfold DynArray. xextract as s data def D.
  xunfold hinv at 1. xextract as IL. xapps.
  xret. xchange* (hinv_unfocus t). xsimpl. 
  destruct IL as [ILs ILi ILf ILc]. subst s. 
   apply length_nil_eq. (*
   todo:  il fallait utiliser len et pas LibList.length dans le lemme 
    pour avoir une égalité au type Z et pas nat... *)
Qed.

Lemma is_empty_spec : forall A,
  Spec is_empty t |B>>
    forall {IA:Inhab A} (L:list A),
    keep B (t ~> DynArray L) (fun (x:bool) => [x = isTrue (L = nil)]).
Proof using.
  xcf. introv. xapps. xrets. apply length_nil_eq.
Qed.

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma get_spec : forall A,
  Spec get (t:dyn_array A) (i:int) |B>>
    forall {IA:Inhab A} (L:list A), index (len L) i ->
    keep B (t ~> DynArray L)
      (fun y:A => [y = L[i]]).
Proof using.
  (* lets: get_cf. ==> alt+maj+A *)
  xcf. introv Hi. xunfold DynArray. xextract as s d def D. hunfold hinv at 1. 
  xextract as IL. xapps. xif.
  xapps. xapps. invert~ IL. intros r. xextract. intros.
   xchange* (@hinv_unfocus t). hsimpl.
   subst r. destruct IL as [ELs ELD IDs ID]. apply~ ELD.
  xfail. invert Hi as a c. invert IL.  intros. invert C; intros; math.
Qed.

Hint Extern 1 (RegisterSpec get) => Provide get_spec.

(* Hint Resolve list_neq_len. *)

(* problème: le code n'est pas polymorphe
Lemma compare_spec : forall A,
  Spec compare (t1:dyn_array A) (t2:dyn_array A) |B>>
    forall {IA:Anhab A} (L1 L2:list A), (* len L1 = len L2 -> *)
    keep B (t1 ~> DynArray L1 \* t2 ~> DynArray L2)
      (fun b:bool => [b = isTrue (L1 = L2)]).
*)
Lemma compare_spec : 
  Spec compare (t1:dyn_array int) (t2:dyn_array int) |B>>
    forall (L1 L2:list int), (* len L1 = len L2 -> *)
    keep B (t1 ~> DynArray L1 \* t2 ~> DynArray L2)
      (fun b:bool => [b = isTrue (L1 = L2)]).
Proof using.
  xcf. intros. xapps. xapps. xif. xrets. xclean. apply~ list_neq_len.
  xapps. xapps. xseq. xwhile.
    cuts M: (forall vi vb,
     R ([0 <= vi <= len L1
         /\ vb = isTrue (forall k, index vi k -> L1[k] = L2[k])] \*
        i ~~> vi \*
        b ~~> vb \*
        t1 ~> DynArray L1 \*
        t2 ~> DynArray L2) 
       (# b ~~> isTrue (L1 = L2) \*
        t1 ~> DynArray L1 \*
        t2 ~> DynArray L2
      )).
    xapply M. hsimpl. split~. xclean. introv Ik. invert Ik. introv Ik1 Ik2.
     false. math. xok.
    skip_goal.
    intros. applys (rm HR). xextract as (Hi&Hb). xlet. xapps. xapps. xapps. xret.
    simpl. xextracts. xif.
      xseq (Hexists vi vb, 
        [0 <= vi <= len L1 /\ vb = isTrue (forall k, index vi k -> L1[k] = L2[k])] \*
          i ~~> vi \*
          b ~~> vb \*
          t1 ~> DynArray L1 \*
          t2 ~> DynArray L2) .
        xseq. xapps. intros i2. xextract as Ei. subst i2. xapps. constructor~.
        xapps. xapps. rewrite C in Hi. constructor~.
          xseq (Hexists vb, [vb = isTrue (forall k, index (vi + 1) k -> L1[k] = L2[k])]
                             \* i ~~> vi
                             \* b ~~> vb
                             \* t1 ~> DynArray L1
                             \* t2 ~> DynArray L2).
          xif. xapps. hsimpl. xclean. intros f. lets F': f vi __. math. false. 
          xrets. destruct C0. rewrite H in *. xclean. introv Ivi. tests Ek: (k = vi).
           auto. apply Hb. math.
          intros vb' Ivb. xapps. hsimpl. split. math. auto. 
        intros. xapply IH. hsimpl. auto. hsimpl.
      xrets. subst vb. extens. iff M. applys~ isTrue_eq_list. invert C0 as N. xclean.
       intros. false. asserts_rewrite~ (len L1 = vi). subst~.
  xapps. hsimpl~.
Qed.


Lemma rev_append_spec : forall A,
  Spec rev_append (t1:dyn_array A) (t2:dyn_array A) |B>>
    forall {IA:Inhab A} (L1 L2:list A), 
    B (t1 ~> DynArray L1 \* t2 ~> DynArray L2)
      (# t1 ~> DynArray (L1 ++ rev L2)).
Proof using.
  xcf. introv. xwhile. xname_post QF.
  cuts M: (forall L1' L2',  L1 ++ rev L2 = L1' ++ rev L2' ->
         R (t1 ~> DynArray L1' \* t2 ~> DynArray L2') QF).
  xapply M; [ | xsimpl | xsimpl ]. autos~.
  skip_goal.
  introv ELs. applys (rm HR). xlet. xapps. xrets. xextract as E. xif.
  subst QF. lets (X&L2''&EL2'): get_last L2'. autos~. subst.
  xapps. intro_subst. xapps. xapply IH; [ | xsimpl| ]. rew_list in *. auto. xsimpl. 
  xrets. subst QF. xsimpl. subst L2'. rew_list~ in *. 
(* cuts M: (R (Hexists L3, t1 ~> DynArray (L1 ++ rev L3)) QF). *)
(* je savais pas trop quoi faire... j'ai essayé de m'inspirer
   des deux spec ci-dessous *)


(* todo: faire un dessin décrivant la situation à une étape arbitraire de la boucle,
   à laquelle on trouve des L1' et des L2' dans t1 et t2, et  
   et exprimer des égalités bien senties entre : L1, L2, L2' et L2'.

   L'énoncé à prouver est de la forme: 
     cuts M: (forall L1' L2',  L1 ++ rev L2 = L1' ++ rev L2' ->
         (t1 ~> DynArray L1' \* t2 ~> DynArray L2') QF).
*)
Qed.



(* COQBUG:
Collision between bound variables of name Q1
*)
