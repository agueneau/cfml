module type ResizeableArray = sig
	type 'a t
	val length : 'a t -> int
	val make : int -> 'a -> 'a t
	val resize : 'a t -> int -> unit
	val get : 'a t -> int -> 'a
	val set : 'a t -> int -> 'a -> unit
	val iter : ('a t -> unit) -> 'a array -> unit
end


module ResizeableArray =
struct

type 'a t = {
   mutable default : 'a;
   mutable size : int;
   mutable data: 'a array ;
}

let length v = v.size

let make n d = { default = d; size = n; data = Array.make n d }

let get v i =
   if i < 0 || i >= v.size
      then invalid_arg "get"
      else v.data.(i)

let set v i elt =
   if i < 0 || i >= v.size
      then invalid_arg "set"
      else v.data.(i) <- elt

let resize v s =
   if s < v.size
   then Array.fill v.data s (v.size - s) v.default
   else
     begin
      (let n = Array.length v.data in
      if s > n || 2*n > Sys.max_array_length
      then begin
         let n2 = max (2*n) s in
         let v2 = Array.make n2 v.default in
         Array.blit v.data 0 v2 0 v.size;
         v.data <- v2
         end
      else invalid_arg "resize")
     end;
   v.size <- s

let iter f v = Array.iter f (v.data)

end




(* Modèle LIFO *)

module type Stack = sig
	type 'a t
	exception Empty
	val create : 'a -> 'a t
	val push : 'a -> 'a t -> unit
	val pop : 'a t -> 'a
end

module Stack_tab : Stack = struct

type 'a t = 'a ResizeableArray.t
exception Empty

let create elt = ResizeableArray.make 0 elt

let pop v = ResizeableArray.get v 0

let push elt v =
   let s = ResizeableArray.length v in
   begin
      ResizeableArray.resize v (s+1);
      ResizeableArray.set v s elt
   end

end

