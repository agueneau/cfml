Set Implicit Arguments.
Require Export CFLib.
Require Import 
  TestDepend_OnlyCoq 
  TestDepend_AuxCode_ml 
  TestDepend_AuxCode_proof
  TestDepend_MainCode_ml.

Lemma g_spec : 
  app g [tt] \[] \[= TestDepend_AuxCode_ml.C].
Proof.
  xcf. intros. xapp. xsimpl. unfolds same. auto.
Qed.

Hint Extern 1 (RegisterSpec g) => Provide g_spec. 