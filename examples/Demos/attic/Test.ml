type ('a,'s) step = | Done | Skip of 's | Yield of 'a * 's
type 'a t = | Sequence : 's * ('s -> ('a, 's) step) -> 'a t


(*
type 'a t = A of 'a 
type 'a mylist = 'a t list 
let empty : 'a mylist = []
-- TODO: ajouter cette exemple à Demos *)