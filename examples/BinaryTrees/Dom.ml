
type node = branches ref 
and branches = Branch0 | Branch2 of node * node 

let mk_leaf () = 
  ref Branch0
 
let mk_node a b =
  ref (Branch2 (a,b))

let size (n0:node) =
  let s = ref 0 in 
  let rec aux (n:node) : unit =
    incr s;
    match !n with
    | Branch0 -> ()
    | Branch2 (a,b) -> aux a; aux b
    in
  aux n0;
  !s

let rec find (p:bool list) (n:node) =
  match p with
  | [] -> n
  | x::p' -> 
    match !n with
    | Branch0 -> failwith "error"
    | Branch2 (a,b) -> if x then find p' a else find p' b

let set_children (n:node) (a:node) (b:node) =
  n := Branch2 (a,b)

let demo () =
  let r = mk_node (mk_leaf()) (mk_node (mk_node (mk_leaf()) (mk_leaf())) (mk_leaf())) in
  let s1 = size r in (* 7 *)
  let x = find [false; true; true] r in
  let y = find [false; true] r in
  let w = find [false] r in
  let z = find [false] w in
  set_children w (mk_leaf()) y;
  set_children x z (mk_leaf());
  let s2 = size r in (* 9 *)
  (s1,s2) 


