Set Implicit Arguments.
Require Import LibFix CFLib Dom_ml.


(**********************)

Inductive tree : Type :=
  | Cut : loc -> tree
  | Leaf : tree
  | Node : tree -> tree -> tree.

Instance tree_inhab : Inhab tree.
Proof using. intros. apply (prove_Inhab Leaf). Qed.

Inductive subtree : binary tree :=
  | subtree_node_a : forall a b, 
     subtree a (Node a b)
  | subtree_node_b : forall a b,
     subtree b (Node a b). 

Hint Constructors subtree.

Lemma subtree_wf : wf subtree.
Proof using.
  intros t. induction t; constructor; introv K; inversions~ K.
Qed.

(**********************)

Fixpoint Size (t:tree) : Z := 
  match t with 
  | Cut c => 0
  | Leaf => 1
  | Node a b => 1 + Size a + Size b
  end.

Fixpoint Full (t:tree) : Prop := 
  match t with 
  | Cut c => False
  | Leaf => True
  | Node a b => Full a /\ Full b
  end.

Definition Notcut (t:tree) : Prop := 
  match t with 
  | Cut c => False
  | _ => True
  end.

Fixpoint Subst (p:list bool) (u:tree) (t:tree) : tree := 
  match p with
  | nil => u
  | x::p' =>
    match t with 
    | Cut c => arbitrary
    | Leaf => arbitrary
    | Node a b => if x then Node (Subst p' u a) b
                       else Node a (Subst p' u b)
    end
  end.

Fixpoint Sub (p:list bool) (t:tree) : tree := 
  match p with
  | nil => t
  | x::p' =>
    match t with 
    | Cut c => arbitrary
    | Leaf => arbitrary
    | Node a b => Sub p' (if x then a else b)
    end
  end.

Inductive ispath : list bool -> tree -> tree -> Prop :=
  | ispath_nil : forall t,
     ispath nil t t
  | ispath_true : forall p a b t,
     ispath p t a ->
     ispath (true::p) t (Node a b)
  | ispath_false : forall p a b t,
     ispath p t b ->
     ispath (false::p) t (Node a b).


(**********************)

Fixpoint Tree (t:tree) (n:node) {struct t} : hprop :=
  match t with
  | Cut c => \[n = c]
  | Leaf => n ~~> Branch0
  | Node u v => Hexists a b,
     n ~~> Branch2 a b \* a ~> Tree u \* b ~> Tree v
  end.

Lemma Tree_focus : forall n t,
  n ~> Tree t ==>
  match t with
  | Cut c => \[n = c]
  | Leaf => n ~~> Branch0
  | Node t u => Hexists a b,
     n ~~> Branch2 a b \* a ~> Tree t \* b ~> Tree u
  end.
Proof using. 
  intros. unfold Tree at 1.
  Transparent hdata. unfold hdata at 1. Opaque hdata.
  apply pred_le_refl. destruct t; fequals. fequals.
  unfold Tree at 1. unfold Tree at 1. apply pred_le_refl.
Qed.

Lemma Tree_cut_focus : forall n c,
  n ~> Tree (Cut c) ==> \[n = c].
Proof using. intros. hunfold Tree. hsimpl~. Qed.

Lemma Tree_leaf_focus : forall n,
  n ~> Tree Leaf ==> n ~~> Branch0.
Proof using. intros. hunfold Tree. hsimpl~. Qed.

Lemma Tree_node_focus : forall n t u,
  n ~> Tree (Node t u) ==> 
  Hexists a b,
     n ~~> Branch2 a b \* a ~> Tree t \* b ~> Tree u.
Proof using. intros. hunfold Tree. hsimpl~. Qed.

Lemma Tree_cut_unfocus : forall n c,
  \[n = c] ==> n ~> Tree (Cut c).
Proof using. intros. hunfold Tree. hsimpl~. Qed.

Lemma Tree_leaf_unfocus : forall n,
  n ~~> Branch0 ==> n ~> Tree Leaf .
Proof using. intros. hunfold Tree. hsimpl~. Qed.

Lemma Tree_node_unfocus : forall n a b t u,
  n ~~> Branch2 a b \* a ~> Tree t \* b ~> Tree u ==> 
  n ~> Tree (Node t u).
Proof using. intros. hunfold Tree. hsimpl~. Qed.

Lemma Tree_cut_extract : forall n c,
  n ~> Tree (Cut c) ==> \[n = c] \* n ~> Tree (Cut c).
Proof using.
  intros. hchange (@Tree_cut_focus n). hsimpl~.
  hchange~ (@Tree_cut_unfocus n c). hsimpl~.
Qed.

(**********************)

Lemma mk_leaf_spec : 
  Spec mk_leaf () |R>> 
    R \[] (fun n => n ~> Tree Leaf).
Proof using. xgo*. Qed.

Lemma mk_node_spec : 
  Spec mk_node a b |R>> forall t u,
    R (a ~> Tree t \* b ~> Tree u)
      (fun n => n ~> Tree (Node t u)).
Proof using.
  xgo. intros n. hchange (@Tree_node_unfocus n); hsimpl. 
Qed.

Lemma size_spec : 
  Spec size (n:node) |R>> forall t, Full t ->
    keep R (n ~> Tree t) (fun s => \[s = Size t]).
Proof using.
  xcf. introv Ft. xapps.
  xfun_noxbody (fun aux => Spec aux (n:node) |R>> 
    forall t p, Full t ->
    R (s ~~> p \* n ~> Tree t) 
      (# s ~~> (p + Size t) \* n ~> Tree t)). 
    clears n0 t. xinduction_heap subtree_wf.
    xbody. intros IH p Fy. xapps.
    xchange (@Tree_focus n). destruct y; simpl in Fy; tryfalse.
    xapp_spec ml_get_spec. intro_subst_hyp. xmatch. xret~.
    xextract as a b. xapp_spec ml_get_spec. intro_subst_hyp.
     inverts Fy. xmatch. xapp~. xapp~.
     hchange (@Tree_node_unfocus n). hsimpl. math.
  xapp~. xapp~. hsimpl. math.
Qed.

Lemma set_children_spec : 
  Spec set_children n a b |R>> forall s t u, Notcut s ->
    R (n ~> Tree s \* a ~> Tree t \* b ~> Tree u)
      (# n ~> Tree (Node t u)).
Proof using. 
  xcf. introv Ns. xchange (@Tree_focus n). 
  destruct s; invert Ns.
  xapps. hchange (@Tree_node_unfocus n); hsimpl.
  xextract as a' b'. xapps. hchange (@Tree_node_unfocus n); hsimpl.
Qed.

Lemma find_spec : 
  Spec find p n |R>> forall t c,
    ispath p (Cut c) t ->
    keep R (n ~> Tree t) (fun x => \[x = c]).
Proof using. 
  xinduction (unproj21 node (@list_sub bool)).
  xcf. introv IH Pp. xmatch. 
  inverts Pp. xchange (@Tree_cut_extract n).
   xextract as E. subst. xret~.
  destruct t as [ | |a b]. inverts Pp. inverts Pp.
  hunfold Tree at 1. xextract as u v.
skip.
(* TODO
  (*xchange (@Tree_node_focus n) as u v. *)
  xapps.
  xmatch. inverts Pp as Pp'. xpost. xif.
     xapp. auto. apply Pp'. hsimpl.
     xapp. auto. apply Pp'. hsimpl~.
 intros r. hextract. intros. hchange (@Tree_node_unfocus n). hsimpl~.
*)
Qed.

Hint Extern 1 (RegisterSpec mk_node) => Provide mk_node_spec.
Hint Extern 1 (RegisterSpec mk_leaf) => Provide mk_leaf_spec.
Hint Extern 1 (RegisterSpec mk_node) => Provide mk_node_spec.
Hint Extern 1 (RegisterSpec set_children) => Provide set_children_spec.
Hint Extern 1 (RegisterSpec size) => Provide size_spec.
Hint Extern 1 (RegisterSpec find) => Provide find_spec.


(**********************)

Lemma tree_cut : forall n p u t,  
  ispath p u t ->
  n ~> Tree t ==>
  Hexists c, n ~> Tree (Subst p (Cut c) t) \* c ~> Tree u.
Proof using. 
  intros n p. gen n. induction p as [|x p']; introv P; simpl.
  apply (@pred_le_trans _ (n ~> Tree t \* n ~> (fun x => \[x = n]))).
   hdata_simpl. hsimpl~. hsimpl~. 
  inverts~ P.
  asserts (t1&t2&E): (exists t1 t2, t = Node t1 t2). inverts* P.
  subst t. hchange (@Tree_node_focus n). hextract as a b.
  inverts P.
    hchange* (IHp' a). hextract as c.
     hchange (@Tree_node_unfocus n). hsimpl~.
    hchange* (IHp' b). hextract as c.
     hchange (@Tree_node_unfocus n). hsimpl~.
Qed.

Lemma tree_uncut : forall n c t u p,
  ispath p (Cut c) t ->
  n ~> Tree t \* c ~> Tree u ==>
  n ~> Tree (Subst p u t).
Proof using. skip. Qed.

Hint Constructors ispath.

Lemma subst_ispath : forall t v u p,
  ispath p v t -> ispath p u (Subst p u t).
Proof using. introv P. induction P; simpl; eauto. Qed.

Lemma find_cut_spec : 
  Spec find p n |R>> forall t u,
    ispath p u t ->
    R (n ~> Tree t) (fun c => 
      n ~> Tree (Subst p (Cut c) t) \* c ~> Tree u).
Proof using. 
  xweaken find_spec as p n. introv Pu.
  xchange (@tree_cut n p u). eauto. xextract as c.
  xapply HR. applys subst_ispath Pu. hsimpl.
  hextract. intros. subst. hsimpl. (* todo: hextracts *)
Qed. 

(**********************)

Transparent Zplus.

Opaque Tree.

Lemma demo_spec : 
  Spec demo () |R>> R \[] (fun x => \[x = (7,9)]).
Proof using.
  xcf. xapp. xapp. xapp. xapp. do 3 xapp.
  xapp. simpl. auto. intros H. subst_hyp H.
  xchange (@tree_cut r (false::true::true::nil)). eauto. simpl.
   xextract as x'. simpl.
  xapp. eauto. intros. subst x'.
  xapp_spec* find_cut_spec. 
  xapp_spec* find_cut_spec.
  xapp_spec* find_cut_spec.
  xapp.
  xapp. simpl. auto.
  xapp. xapp. simpl. auto.
  xchange~ (@tree_uncut r w). simpl.
  xchange~ (@tree_uncut r x). simpl.
  xapps. simpl. auto.
  xret. hsimpl. auto.
Qed.

(*
let demo () =
  let r = mk_node (mk_leaf()) (mk_node (mk_node (mk_leaf()) (mk_leaf())) (mk_leaf())) in
  let s1 = size r in (* 7 *)
  let w = find [false] r in
  let z = find [false] w in
  let y = find [false; true] r in
  let x = find [false; true; true] r in
  set_children w (mk_leaf()) y;
  set_children x z (mk_leaf());
  let s2 = size r in (* 9 *)
  (s1,s2) 
*)





