module type Reducer =
  sig
    type item
    type meas
    val inhab : item
    val zero : meas
    val combine : meas -> meas -> meas
    val measure : item -> meas
  end
