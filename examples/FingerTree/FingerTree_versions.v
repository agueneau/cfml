Set Implicit Arguments.
Require Import CFLib LibSet LibMap LibArray FingerTree_ml.


(****************************************************)
(** Invariants *)

(** Invariants on trees *)

Inductive tree_inv (A:Type) : nat -> tree A -> list A -> Prop :=
  | tree_inv_leaf : forall v,
      tree_inv 0 (Tree_leaf v) (v::nil)
  | tree_inv_node2 : forall n t1 t2 L1 L2,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      tree_inv (S n) (Tree_node2 t1 t2) (L1 ++ L2)
  | tree_inv_node3 : forall n t1 t2 t3 L1 L2 L3,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      tree_inv n t3 L3 ->
      tree_inv (S n) (Tree_node3 t1 t2 t3) (L1 ++ L2 ++ L3).

(** Invariants on digits *)

Inductive digit_inv (A:Type) : nat -> digit (tree A) -> list A -> Prop :=
  | digit_inv_1 : forall n t1 L1,
      tree_inv n t1 L1 ->
      digit_inv n (Digit1 t1) L1 
  | digit_inv_2 : forall n t1 t2 L1 L2,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      digit_inv n (Digit2 t1 t2) (L1 ++ L2)
  | digit_inv_3 : forall n t1 t2 t3 L1 L2 L3,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      tree_inv n t3 L3 ->
      digit_inv n (Digit3 t1 t2 t3) (L1 ++ L2 ++ L3)
  | digit_inv_4 : forall n t1 t2 t3 t4 L1 L2 L3 L4,
      tree_inv n t1 L1 ->
      tree_inv n t2 L2 ->
      tree_inv n t3 L3 ->
      tree_inv n t4 L4 ->
      digit_inv n (Digit4 t1 t2 t3 t4) (L1 ++ L2 ++ L3 ++ L4).

(** Invariants on finger trees *)

Inductive ftree_inv (A:Type) : nat -> ftree A -> list A -> Prop :=
  | ftree_inv_empty : forall n,
      ftree_inv n Ftree_empty nil
  | ftree_inv_single : forall n t L,
      tree_inv n t L ->
      ftree_inv n (Ftree_single t) L
  | ftree_inv_deep : forall n dl fm dr Ll Lr Lm,
      digit_inv n dl Ll ->
      ftree_inv (S n) fm Lm ->
      digit_inv n dr Lr ->
      ftree_inv n (Ftree_deep dl fm dr) (Ll ++ Lm ++ Lr).

Definition finger_tree_inv (A:Type) := 
  @ftree_inv A 0.


(****************************************************)
(** Auxiliary *)

(** Well-founded order on trees *)

Inductive tree_sub (A:Type) : binary (tree A) :=
  | Tree_node2_1 : forall T1 T2,
      tree_sub T1 (Tree_node2 T1 T2)
  | Tree_node2_2 : forall T1 T2,
      tree_sub T2 (Tree_node2 T1 T2)
  | Tree_node3_1 : forall T1 T2 T3,
      tree_sub T1 (Tree_node3 T1 T2 T3)
  | Tree_node3_2 : forall T1 T2 T3,
      tree_sub T2 (Tree_node3 T1 T2 T3)
  | Tree_node3_3 : forall T1 T2 T3,
      tree_sub T3 (Tree_node3 T1 T2 T3).

Lemma tree_sub_wf : forall A, wf (@tree_sub A).
Proof.
  intros A T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Resolve tree_sub_wf : wf.

(** Well-founded order on ftrees *)

Inductive ftree_sub (A:Type) : binary (ftree A) :=
  | ftree_sub_intro : forall dl sub dr,
      ftree_sub sub (Ftree_deep dl sub dr).

Lemma ftree_sub_wf : forall A, wf (@ftree_sub A).
Proof.
  intros A T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Resolve ftree_sub_wf : wf.


(****************************************************)
(** Automation *)

Hint Constructors tree_inv ftree_inv Forall2 digit_inv.

Lemma ftree_inv_deep' : forall A n dl fm dr Ll Lr Lm (L:list A),
  digit_inv n dl Ll ->
  ftree_inv (S n) fm Lm ->
  digit_inv n dr Lr ->
  L = (Ll ++ Lm ++ Lr) ->
  ftree_inv n (Ftree_deep dl fm dr) L.
Proof. intros. subst. constructors*. Qed.

Hint Resolve ftree_inv_deep'.

Ltac auto_star ::= 
  try solve [ jauto | check_noevar_goal; rew_list; jauto ].


(****************************************************)
(** Auxiliary specifications *)

Inductive digit_full (A:Type) : digit A -> Prop :=
  | digit_full_intro : forall x1 x2 x3 x4,
      digit_full (Digit4 x1 x2 x3 x4).

Hint Constructors digit_full.

Definition tree_not_leaf (A:Type) (t:tree A) :=
  forall x, t <> Tree_leaf x.

Definition digit_not_one (A:Type) (d:digit A) :=
  forall x, d <> Digit1 x.


(****************************************************)
(** Verification of [digit_add] *)

Lemma digit_add_spec : forall A,
  Spec digit_add (t0:tree A) (d:digit (tree A)) |R>>
     forall n L L0, digit_inv n d L -> tree_inv n t0 L0 -> ~ digit_full d ->
     R \[] (fun d' => \[digit_inv n d' (L0 ++ L)]).
Proof.
  xcf. introv InvD InvT Nfull. xmatch; inverts InvD as.
  intros. xrets*.
  intros. xrets*.
  intros. xrets*.
  intros. xfail. false* Nfull.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec digit_add) => Provide digit_add_spec.


(****************************************************)
(** Verification of [ftree_add_aux] *)

Lemma ftree_add_aux_spec : forall A,
  Spec ftree_add_aux (t0:tree A) (f:ftree A) |R>>
     forall n L L0, ftree_inv n f L -> tree_inv n t0 L0 ->
     R \[] (fun f' => \[ftree_inv n f' (L0 ++ L)]).
Proof.
  intros. xinduction (unproj22 (tree A) (@ftree_sub A)).
  xcf. introv IH InvF InvT. xmatch; inverts InvF as.
  xrets. rew_list. constructors*.
  introv InvT2. xrets*.
  introv InvDl InvSub InvDr. xmatch.
    inverts InvDl as M1 M2 M3 M4.
     subst. xapp*. constructors. 
     introv InvSub2. xrets.
      applys* (>> ftree_inv_deep' (L0 ++ L1)).
    xapp*. intros N. inverts N. false* C1.
     introv InvD2. xrets. applys* ftree_inv_deep'.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec ftree_add_aux) => Provide ftree_add_aux_spec.


(****************************************************)
(** Verification of [ftree_add] *)

Lemma ftree_add_spec : forall A,
  Spec ftree_add (x:A) (f:ftree A) |R>>
     forall L, finger_tree_inv f L -> 
     R \[] (fun f' => \[finger_tree_inv f' (x::L)]).
Proof.
  xcf. introv InvT. xapp*. xsimpl*.
Qed.

Hint Extern 1 (RegisterSpec ftree_add) => Provide ftree_add_spec.


(****************************************************)
(** Verification of [ftree_of_digit] *)

Lemma ftree_of_digit_spec : forall A,
  Spec ftree_of_digit (d:digit (tree A)) |R>>
     forall n L, digit_inv n d L -> 
     R \[] (fun f => \[ftree_inv n f L]).
Proof.
  xcf. introv InvD. xvals. xmatch; inverts InvD as.
  intros. xrets*.
  intros. xrets*.
  intros. xrets. applys* ftree_inv_deep' (L1 ++ L2).
  intros. xrets. applys* ftree_inv_deep' (L1 ++ L2).
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec ftree_of_digit) => Provide ftree_of_digit_spec.


(****************************************************)
(** Verification of [digit_of_tree] *)

Lemma digit_of_tree_spec : forall A,
  Spec digit_of_tree (t:tree A) |R>>
     forall n L, tree_inv (S n) t L -> tree_not_leaf t ->
     R \[] (fun d => \[digit_inv n d L]).
Proof.
  xcf. introv InvT N. xmatch; inverts InvT; xrets*.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec digit_of_tree) => Provide digit_of_tree_spec.


(****************************************************)
(** Verification of [digit_pop] *)

Lemma digit_pop_spec : forall A,
  Spec digit_pop (d:digit(tree A)) |R>>
     forall n L, digit_inv n d L -> digit_not_one d ->
     R \[] (fun p => let '(t,d') := p in 
           Hexists L1 L2, \[tree_inv n t L1 /\ digit_inv n d' L2 /\ L = L1 ++ L2]).
Proof.
  xcf. introv InvD N. xvals. xmatch; inverts InvD; xrets*.
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec digit_pop) => Provide digit_pop_spec.


(****************************************************)
(** Verification of [ftree_pop_aux] *)

Lemma ftree_pop_aux_spec : forall A,
  Spec ftree_pop_aux (f:ftree A) |R>>
     forall n L, ftree_inv n f L -> 
     R \[] (fun o => 
        match o with
        | None => \[L = nil]
        | Some (t,f') => Hexists L1 L2,
            \[tree_inv n t L1 /\ ftree_inv n f' L2 /\ L = L1 ++ L2] 
        end).
Proof.
  xcf. introv InvT. xmatch; inverts InvT.
  xret*.
  xret*. hsimpl* L (@nil A).
  xmatch.
    skip.
    xapp*. xmatch. xret. hextract as L1 L2 (M1&M2&E). 
     subst. hsimpl* L1 (L2 ++ Lm ++ Lr).
Admitted. (* for faster compilation *)

Hint Extern 1 (RegisterSpec ftree_pop_aux) => Provide ftree_pop_aux_spec.


(****************************************************)
(** Verification of [ftree_pop] *)

Lemma ftree_pop_spec : forall A,
  Spec ftree_pop (f:ftree A) |R>>
     forall L, finger_tree_inv f L -> L <> nil ->
     R \[] (fun p => let '(x,f') := p in Hexists L',
           \[finger_tree_inv f' L' /\ L = x::L']).
Proof.
  xcf. introv InvT N. xapp* as p. xmatch.
  xret. hextract as L1 L2 (M1&M2&M3). inverts M1. 
   subst. hsimpl*.
  destruct p as [[t f']|]; xextract.
    intros L1 L2 (M1&M2&M3). xfail. inverts M1. false* C.
    intro_subst. xfail*.
Qed.

Hint Extern 1 (RegisterSpec ftree_pop) => Provide ftree_pop_spec.



(****************************************************)
(****************************************************)



type 'a tree = 
  | Tree_leaf of 'a
  | Tree_node2 of 'a tree * 'a tree 
  | Tree_node3 of 'a tree * 'a tree * 'a tree

type 'a digit = 
  | Digit1 of 'a
  | Digit2 of 'a * 'a
  | Digit3 of 'a * 'a * 'a
  | Digit4 of 'a * 'a * 'a * 'a

type 'a ftree = 
  | Ftree_empty
  | Ftree_single of 'a tree
  | Ftree_deep of ('a tree) digit * 'a ftree * ('a tree) digit

let digit_add x0 d = 
  match d with
  | Digit1 (x1) -> Digit2 (x0,x1)
  | Digit2 (x1,x2) -> Digit3 (x0,x1,x2)
  | Digit3 (x1,x2,x3) -> Digit4 (x0,x1,x2,x3)
  | Digit4 (_,_,_,_) -> assert false

let rec ftree_add_aux t0 f = 
  match f with
  | Ftree_empty -> Ftree_single t0
  | Ftree_single t1 -> Ftree_deep (Digit1 t0, Ftree_empty, Digit1 t1)
  | Ftree_deep (dl, fm, dr) ->
      match dl with
      | Digit4 (t1,t2,t3,t4) -> 
             let fm2 = ftree_add_aux (Tree_node3 (t2,t3,t4)) fm in
             Ftree_deep (Digit2 (t0,t1), fm2, dr) 
      | _ -> Ftree_deep (digit_add t0 dl, fm, dr) 

let ftree_add x0 f =
  ftree_add_aux (Tree_leaf x0) f

let ftree_of_digit d =
  match d with
  | Digit1 (a) -> Ftree_single a
  | Digit2 (a, b) -> Ftree_deep (Digit1 a, Ftree_empty, Digit1 b)
  | Digit3 (a, b, c) -> Ftree_deep (Digit2 (a, b), Ftree_empty, Digit1 c)
  | Digit4 (a, b, c, d) -> Ftree_deep (Digit2 (a, b), Ftree_empty, Digit2 (c, d))

let digit_of_tree t = 
  match t with
  | Tree_leaf _ -> assert false
  | Tree_node2 (a, b) -> Digit2 (a, b)
  | Tree_node3 (a, b, c) -> Digit3 (a, b, c)

let digit_pop d =
  match d with
  | Digit1 (a) -> assert false
  | Digit2 (a, b) -> (a, Digit1 (b))
  | Digit3 (a, b, c) -> (a, Digit2 (b, c))
  | Digit4 (a, b, c, d) -> (a, Digit3 (b, c, d))

let rec ftree_pop_aux f = 
  match f with
  | Ftree_empty -> None
  | Ftree_single t1 -> Some (t1, Ftree_empty)
  | Ftree_deep (dl, fm, dr) ->
    match dl with 
    | Digit1 (a) ->
       let t = match ftree_pop_aux fm with
         | None -> ftree_of_digit dr
         | Some (t2, fm2) -> Ftree_deep (digit_of_tree t2, fm2, dr) 
         in
       Some (a, t)
    | _ ->
       let (a, dl2) = digit_pop dl in
       Some (a, Ftree_deep (dl2, fm, dr))

let ftree_pop f =
  match ftree_pop_aux f with
  | Some (Tree_leaf x1, f2) -> (x1, f2)
  | _ -> assert false




(** -------------------------------



type ('a, 'c) tree = 'c * ('a, 'c ) tree_struct

and ('a,'c) tree_struct =
  | Tree_leaf of 'a
  | Tree_node2 of  ('a, 'c) tree * ('a, 'c) tree
  | Tree_node3 of ('a, 'c) tree * ('a, 'c) tree * ('a, 'c) tree

type ('a, 'c) digit = 'c * 'a digit_struct

and 'a digit_struct =
  | Digit1 of 'a
  | Digit2 of 'a * 'a
  | Digit3 of 'a * 'a * 'a
  | Digit4 of 'a * 'a * 'a * 'a

type ('a, 'c) ftree =
  | Ftree_empty
  | Ftree_single of ('a, 'c) tree
  | Ftree_deep of 'c * ('a tree, 'c) digit
                     * ('a, 'c) ftree
                     * ('a tree, 'c) digit




type ('a, 'c) reducer = {
  zero : 'c;
  combine : 'c -> 'c -> 'c;
  measure : 'a -> 'c;
}

type ('a, 'c) tree = 
  | Tree_leaf of 'c * 'a
  | Tree_node2 of 'c * 'a tree * 'a tree 
  | Tree_node3 of 'c * 'a tree * 'a tree * 'a tree

type ('a, 'c) digit = 
  | Digit1 of 'c * 'a
  | Digit2 of 'c * 'a * 'a
  | Digit3 of 'c * 'a * 'a * 'a
  | Digit4 of 'c * 'a * 'a * 'a * 'a

type ('a, 'c) ftree = 
  | Ftree_empty
  | Ftree_single of ('a, 'c) tree
  | Ftree_deep of 'c * ('a tree, 'c) digit 
                     * ('a, 'c) ftree 
                     * ('a tree, 'c) digit

let measure_node x =
  match x with
  | Tree_leaf (v, _) -> v
  | Tree_node2 (v, _, _) -> v
  | Tree_node3 (v, _, _, _) -> v
let measure_digit x =
  match x with
  | Digit1 (v, _) -> v
  | Digit2 (v, _, _) -> v
  | Digit3 (v, _, _, _) -> v
  | Digit4 (v, _, _, _, _) -> v
let measure_ftree r x =
  match x with
  | Ftree_empty -> r.zero
  | Ftree_single x -> measure_node x
  | Ftree_deep (v, _, _, _) -> v

let leaf r x = Tree_leaf (r.measure x, x)
let node2 r t1 t2 = 
  let v = r.combine (measure_node t1) (measure_node t2) in
  Tree_node2 (v, t1, t2)
let node3 r t1 t2 t3 = 
  let v = r.combine (measure_node t1) (measure_node t2) (measure_node t3) in
  Tree_node3 (v, t1, t2, t3)

let digit1 r t1 =
  Digit1 (measure_node t1, t1)
let digit2 r t1 t2 =
  let v = r.combine (measure_node t1) (measure_node t2) in
  Digit2 (v, t1)
let digit3 r t1 t2 t3 =
  let v1 = r.combine (measure_node t1) (measure_node t2) in
  let v2 = measure_node t3 in
  Digit3 (r.combine v1 v2, t1, t2, t3)
let digit4 r t1 t2 t3 =
  let v1 = r.combine (measure_node t1) (measure_node t2) in
  let v2 = r.combine (measure_node t3) (measure_node t4) in
  Digit4 (r.combine v1 v2, t1, t2, t3, t4)

let deep r df sub db =
  let v = r.combine (measure_digit df) (measure_digit db) in
  let v = r.combine v (measure_ftree r sub) in
  Deep (v, df, sub, db)

let digit_push_front r x0 = function 
  | Digit1 (_, x1) -> digit2 r x0 x1
  | Digit2 (_, x1, x2) -> digit3 r x0 x1 x2
  | Digit3 (_, x1, x2, x3) -> digit4 r x0 x1 x2 x3
  | Digit4 (_,_,_,_) -> assert false

let digit_push_back r x0 = function 
  | Digit1 (_, x1) -> digit2 r x1 x0
  | Digit2 (_, x1, x2) -> digit3 r x1 x2 x0
  | Digit3 (_, x1, x2, x3) -> digit4 r x1 x2 x3 x0
  | Digit4 (_,_,_,_) -> assert false

let rec ftree_push_front_aux r t0 = function
  | Ftree_empty -> Ftree_single t0
  | Ftree_single t1 -> 
    deep r (digit1 r t0, Ftree_empty, digit1 r t1)
  | Ftree_deep (_, df, sub, db) ->
      match df with
      | Digit4 (_,t1,t2,t3,t4) -> 
	let t = node3 r t2 t3 t4 in
        let sub2 = ftree_push_front_aux r t sub in
        deep r (digit2 r t0 t1, sub2, db) 
      | _ -> deep r (digit_push_front r t0 df, sub, db)

let ftree_push_front r x0 =
  ftree_push_front_aux r (leaf r x0)

let rec ftree_push_back_aux r t0 = function
  | Ftree_empty -> Ftree_single t0
  | Ftree_single t1 -> 
    deep r (digit1 r t1, Ftree_empty, digit1 r t0)
  | Ftree_deep (_, df, sub, db) ->
      match db with
      | Digit4 (_,t1,t2,t3,t4) -> 
   	let t = node3 r t1 t2 t3 in
        let sub2 = ftree_push_back_aux r t sub in
        deep r (digit2 r t4 t0, sub2, db) 
      | _ -> deep r (df, sub, digit_push_back r t0 db)

let ftree_push_back r x0 =
  ftree_push_back_aux r (leaf r.measure x0)

let to_tree_digit_node r d =
  match d with
  | Digit1 (_, a) -> Ftree_Single a
  | Digit2 (_, a, b) -> 
    deep r (digit1 r a, Ftree_empty, digit2 r b)
  | Digit3 (a, b, c) -> 
    deep r (digit2 r a b, Ftree_empty, digit1 r c)
  | Digit4 (a, b, c, d) -> 
    deep r (digit2 r a b, Ftree_empty, digit2 r c d)

let to_digit_node r t = function
  | Tree_leaf _ -> assert false
  | Tree_node2 (_, a, b) -> digit2 r a b
  | Tree_node3 (_, a, b, c) -> digit3 r a b c

let digit_front d =
  match d with
  | Digit1 (_, a) -> a
  | Digit2 (_, a, b) -> a
  | Digit3 (_, a, b, c) -> a
  | Digit4 (_, a, b, c, d) -> a

let digit_pop_front r d =
  match d with
  | Digit1 (_, a) -> assert false
  | Digit2 (_, a, b) -> digit1 r b
  | Digit3 (_, a, b, c) -> digit2 r b c
  | Digit4 (_, a, b, c, d) -> digit3 r b c d

let digit_back d =
  match d with
  | Digit1 (_, a) -> a
  | Digit2 (_, a, b) -> b
  | Digit3 (_, a, b, c) -> c
  | Digit4 (_, a, b, c, d) -> d

let digit_pop_back r d =
  match d with
  | Digit1 (_, a) -> assert false
  | Digit2 (_, a, b) -> digit1 r a
  | Digit3 (_, a, b, c) -> digit2 r a b
  | Digit4 (_, a, b, c, d) -> digit3 r a b c

type 'a stream = SNil | SCons 'a * 'a stream

let ftree_pop_front r t =
  match t with
  | Ftree_empty -> SNil
  | Ftree_single t1 -> SCons (t1, Ftree_empty)
  | Ftree_deep (_, Ftree_deep (_, Digit1 (_, x), sub, db)) ->
      let t = 
	match ftree_pop_front r sub with
	| SNil -> to_tree_digit_node r db
	| SCons (t2, sub2) -> 
	  deep r (to_digit_node r t2, sub2, db) in
      (x, t)
  | Ftree_deep (Ftree_deep (df, sub, db)) ->
      SCons (digit_front df, Deep (digit_pop_front r df, sub, db))

let ftree_pop_back r t= 
  match t with
  | Ftree_empty -> SNil
  | Ftree_single t1 -> SCons (t1, Ftree_empty)
  | Ftree_deep (_, Ftree_deep (_, df, sub, Digit1 (_, x))) ->
      let t = 
	match ftree_pop_back r sub with
	| SNil -> to_tree_digit_node r df
	| SCons (t2, sub2) -> 
	  deep r (df, sub2, to_digit_node r t2) in
      (x, t)
  | Ftree_deep (Ftree_deep (df, sub, db)) ->
      SCons (digit_back db, Deep (df, sub, digit_back r db))

type ('a, 'rest) split = Split of 'rest * 'a * 'rest

let digit_split r p i d = 
  match d with
    | Digit1 (_, a) -> Split ([], a, [])
    | Digit2 (_, a, b) ->
      let i' = r.combine i (r.measure a) in
      if p i' then Split ([], a, [b]) else
        Split ([a], b, [])
    | Digit3 (_, a, b, c) ->
      let i' = r.combine i (r.measure a) in
      if p i' then Split ([], a, [b; c]) else
        let i'' = r.combine i' (r.measure b) in
        if p i'' then Split ([a], b, [c]) else
          Split ([a; b], c, [])
    | Digit4 (_, a, b, c, d) ->
      let i' = r.combine i (r.measure a) in
      if p i' then Split ([], a, [b; c; d]) else
        let i'' = r.combine i' (r.measure b) in
        if p i'' then Split ([a], b, [c; d]) else
          let i''' = r.combine i'' (r.measure c) in
          if p i''' then Split ([a; b], c, [d]) else
            Split ([a; b; c], d, [])

let deep_front r df sub db =
  match df with
  | [] -> (
    match ftree_pop_front r sub with
    | SNil -> to_tree_digit r db
    | SCons (a, sub') -> deep r (to_digit_node a) sub' db)
  | _ ->
    deep r (to_digit_list r df) sub db

let deep_back r df sub db =
  match db with
  | [] -> (
    match ftree_pop_back r sub with
    | SNil -> to_tree_digit r df
    | SCons (a, sub') -> deep r (to_digit_node a) sub' db)
  | _ ->
    deep r df sub (to_digit_list r db)

let rec ftree_split_aux r p i t =
  match t with
  | Ftree_empty -> assert false
  | Ftree_single a -> Split (Ftree_empty, a, Ftree_empty)
  | Ftree_deep (v, df, sub, db) ->
    let vdf = r.combine i (measure_digit df) in
    if p vdf then
      let Split (dff, x, dfb) = digit_split r df in
      Split (to_tree_list r dff, x, deep_front r dfb sub db)
    else
      let vsub = r.combine vdf (measure_ftree r sub) in
      if p vsub then
	let Split (subf, m, subb) = ftree_split_aux r p vdf sub in
	let vsubf = measure_ftree subf in
	let dm = to_digit_node m in
	let Split (dff, x, dfb) = digit_split r p (r.combine vdf vsubf) dm in
	Split (deep_back r df subf dff, x, deep_front r dfb subb db)
      else
	let Split (dbf, x, dbb) = digit_split r db in
	Split (deep_back r df sub dbf, x, to_tree_list r dbb)

let ftree_split r p t =
  match t with
  | Ftree_empty -> (Ftree_empty, Ftree_empty)
  | _ ->
    if p (measure_tree r t) then
      let Split (f, x, b) = ftree_split_aux r p r.zero in
      (f, ftree_push_front r x b)
    else
      (t, Ftree_empty)

*)


