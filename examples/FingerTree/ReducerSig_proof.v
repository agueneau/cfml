Set Implicit Arguments.
Require Import CFLib ReducerSig_ml LibCore.

(* todo: move to LibList *)

Require Import LibStruct.

Record reducer_def (A B:Type) : Type := reducer_ { 
   reducer_reduce :> monoid_def B;
   reducer_map : A -> B }.

Definition Reducer A B (r:reducer_def A B) :=
  Monoid (reducer_reduce r).

Section Reduce.
Variables (A B : Type).
Parameter list_reduce : reducer_def A B -> list A -> B.
Parameter list_reduce_nil : forall r, 
  Reducer r ->
  list_reduce r nil = monoid_neutral r.
Parameter list_reduce_cons : forall x l r, 
  Reducer r ->
  list_reduce r (x::l) = monoid_oper r (reducer_map r x) (list_reduce r l).
Parameter list_reduce_one : forall x r, 
  Reducer r ->
  list_reduce r (x::nil) = reducer_map r x.
Parameter list_reduce_app : forall r l1 l2, 
  Reducer r ->
  list_reduce r (l1 ++ l2) = monoid_oper r (list_reduce r l1) (list_reduce r l2).
End Reduce.


(*--------------*)

Module Type ReducerSigSpec.

Declare Module R : MLReducer.
Import R.
Parameter Item : Type.
Parameter Rep : Item -> item -> Prop.
Parameter Red : reducer_def Item meas.
Parameter Red_Reducer : Reducer Red.

Parameter zero_spec : 
  zero = monoid_neutral Red.

Parameter combine_spec : 
  Spec combine (m1:meas) (m2:meas) |R>>
    R \[] (fun m => [m = monoid_oper Red m1 m2]).

Parameter measure_spec : 
  Spec measure (x:item) |R>>
    forall X, Rep X x ->
    R \[] (fun m => [m = reducer_map Red X]).

End ReducerSigSpec.
