
Set Implicit Arguments. 
Require Import CFLib Tuto_ml.
Require Import LibListZ Buffer.
Ltac auto_tilde ::= try solve [ intuition eauto with maths ].



(* ---------------------------------------------------------------------- *)
(** Definitions *)

Definition prime k := 
  k > 1 /\ forall r d, 1 < d < k -> k <> r * d.

Definition no_factor i n k :=
  k > 1 /\ (forall r d, 1 < r < i -> 1 < d < n -> k <> r*d).


(* ---------------------------------------------------------------------- *)
(** Simulate for credits pay *)

Require Import CFLibCredits.

Axiom xpay_simulate : \$1 ==>  \[].
Ltac xpay_ := xchange xpay_simulate.


(* ---------------------------------------------------------------------- *)
(** Mathematical result *)

(*
Axiom log : int -> int.
Axiom asymptotic_cst : int.
Axiom asymptotic_bound : forall n,
  pot_outer n 2 <= asymptotic_cst * n * log (log n).
*)


(* ---------------------------------------------------------------------- *)
(** Potential *)

Definition big_sum A (E:set A) (f:A->int) := reduce (monoid_ Z.add 0) f E.

Definition pot_outer n i := abs (n-i) + big_sum (filter prime (seg i n)) (fun p => n / p).
(* note: alternative max 0 (n-1) *)

Definition pot_inner n i r := n / i - r + 1.

Lemma pot_outer_step : forall n i, i < n ->
  pot_outer n i >= 1 + (If prime i then n/i else 0) + pot_outer n (i+1).
Proof using.
  introv N. unfold pot_outer. do 2 rewrite~ abs_pos.
  rewrite~ seg_first. autorewrite with rew_filter.
  unfold big_sum at 1.
  rewrite reduce_union; [|typeclass]. simpl. case_if.
    rewrite reduce_single; [|typeclass]. unfold big_sum. math.
    rewrite reduce_empty; [|typeclass]. simpl. unfold big_sum.
  math.
Qed.

Lemma pot_inner_step : forall r n i, r*i < n ->
  pot_inner n i r >= 1 + pot_inner n i (r+1).
Proof using. intros. unfold pot_inner. math. Qed.
Implicit Arguments pot_inner_step [r n i].
          

(* ---------------------------------------------------------------------- *)
(** Missing lemmas *)

Lemma credits_int_le' : forall (x y : int), 
  (x >= y)%I -> (0 <= y)%I -> \$ x ==> \$ y \* (Hexists G, G).
Proof using.
  intros. hchanges~ (>> credits_int_le x y).
Qed.

Axiom credits_int_le'' : forall (x y : int), 
  (x >= y)%I -> (y >= 0)%I -> \$ x ==> \$ y.

(* LATER
Lemma big_sum_nonneg : forall f (E:set int),
  (forall x, 0 <= f x) ->
  0 <= big_sum E f.
Proof using. intros. unfold big_sum. *)

Axiom seg_zero : forall a b,
  a >= b -> seg a b = \{}.

Lemma pot_outer_beyond : forall n i, 
  i >= n ->
  0 <= pot_outer n i.
Proof using.
  introv N. unfold pot_outer. rewrite~ seg_zero.
  unfold big_sum. rewrite filter_empty.
  rewrite reduce_empty; [|typeclass]. simpl. math.
Qed. 

Lemma pot_outer_case_nonneg : forall n i,
  0 < i -> 0 <= n ->
  0 <= (If prime i then n / i else 0).
Proof using.
  intros. case_if~. forwards~: Z_div_ge0 n i.
Qed.

Lemma pot_outer_nonneg : forall n i,
  i > 0 ->
  0 <= pot_outer n i.
Proof using. 
  introv. induction_wf IH: (int_upto_wf n) i; intros.
  tests: (i < n).
  { forwards~: pot_outer_step n i. forwards~: IH (i+1).  
    forwards~: pot_outer_case_nonneg n i. }
  { applys~ pot_outer_beyond. }
Qed.

Lemma pot_inner_nonneg : forall n i r,
  0 < i -> i*r < n ->
  0 <= pot_inner n i r.
Proof using. introv Hi N. unfolds pot_inner. math_dia. Qed.

Hint Resolve pot_outer_nonneg pot_inner_nonneg 
  pot_outer_case_nonneg : simpl_nonneg.

Hint Extern 1 (_ >= 0) => simpl_nonneg. 
Hint Extern 1 (0 <= _) => simpl_nonneg. 
Ltac auto_tilde ::= 
  try solve [ intuition eauto with maths simpl_nonneg ].


Tactic Notation "credits_split" :=
  credits_split.
Tactic Notation "credits_split" "~" :=
  credits_split; auto_tilde.
Tactic Notation "credits_split" "*" :=
  credits_split; auto_star.



Lemma no_factor_to_prime : forall n k i,
  k <= i -> i <= n ->
  no_factor i n k <-> prime k.
Proof using.
  intros. unfold prime. iff (L&G).
 { split. autos. introv Hd E. applys G r d; math_nia. }
 { split. autos. intros r d Hr1 Hr2 E. applys G r d; math_nia. }
Qed.

(* ---------------------------------------------------------------------- *)
(** Verification *)


Lemma sieve_spec : forall n,
  n >= 2 ->
  App sieve n; 
    (\$(pot_outer n 2 + abs n * array_make_cst + 1))
    (fun (t:loc) => Hexists M', t ~> Array M' \*
      \[forall k, 0 <= k < n -> M'[k] = isTrue (prime k)]).
Proof using.
introv Hn. xcf. credits_split~. xpay_.
xapp~. intros M0 HM0. xapp~. xapp~.
xapp~. xwhile_inv (fun b m => 
 Hexists iv M, (i ~~> iv) \* (t ~> Array M) \* 
  \[1 < iv <= n] \* \[length M = n] \* \$(pot_outer n iv) \*
  \[forall k, 0 <= k < n -> M[k] = isTrue(no_factor iv n k) ] \*
  \[b = isTrue(iv < n)] \* \[m = n - iv]); try clear M0 HM0.
 { exists_all. xsimpl~. introv Hk. subst M0. rew_array~.
	unfold no_factor. case_ifs; rew_logic~. }  
 { intros. xextract as iv M Hi LM HM Eb Em. xapps. xrets~. }
 { intros. xextract as iv M Hi LM HM Eb Em. xapps. xapps~.
	xseq (Hexists M', (i ~~> iv) \* (t ~> Array M') \*  
   \$(pot_outer n (iv+1)) \* \[length M' = n] \*
	 \[forall k, 0 <= k < n -> M'[k] = isTrue(no_factor (iv+1) n k)]).
  { lets~ G: pot_outer_step n iv __.
    xchange~ (credits_int_le' (rm G)). simpl_nonneg; autos.
    applys~ pot_outer_case_nonneg. (* simplify *)
    xextract as G'. xgc G'. credits_split~. 
    xpay_. xif.
	{ xapps. xapps. xgc.
    xframe (\$(pot_outer n (iv+1))). 
	  xwhile_inv (fun b m => 
		 Hexists rv M', (i ~~> iv) \* (r ~~> rv) \* (t ~> Array M') \* 
		 \[1 < rv <= n] \* \[length M' = n] \* \$(pot_inner n iv rv) \*
		 \[forall k, 0 <= k < n -> 
			 M'[k] = isTrue(no_factor iv n k /\ 
						 (forall d, 1 < d < rv -> k <> d*iv)) ] \*
		 \[b = isTrue(rv*iv < n)] \* \[m = n + iv - rv*iv]).
	  { exists_all. xsimpl_credits~.
      { asserts: (prime iv).
        { rewrite~ HM in C. logics.
          forwards~ (?&?): no_factor_to_prime n iv iv. }         
        case_if. unfold pot_inner. math. }
      { skip. } (* won't be needed *) 
      { introv Hk. rewrite~ HM.
  		  logics. iff (L&G) (G1&G2); unfolds~ no_factor. } }
	  { clears M. intros. xextract as rv M Hr LM HM Eb' Em'.
		 xapps. xapps. xrets~. }
	  { clears M. intros. xextract as rv M Hr LM HM Eb' Em'.
      xchange~ (credits_int_le'' (pot_inner_step Eb')). skip.
      credits_split~; [|skip]. xpay_. 
      xapps. asserts: (rv*iv>=0). { math_nia. }  
      xapps~. xapps~. xapps~. xsimpl_credits~.
		 { introv Hk. rew_array~. case_if.  
			{ sets_eq k: (rv * iv). rew_logic.
			  right. exists rv. rew_logic~. } 
			{ rewrite~ HM. logics. iff (G1&G2). 
			  { split. autos~. introv Hd.
				 unfolds no_factor. tests~: (d = rv). }
			  { split. autos~. intros. applys~ G2. } } }
		 { math_nia. }
		 { math_nia. } }
  { xok. }
	{ clears M. xextract as m' rv M Hr LM HM Eb' Em'. xsimpl~.
    (* not provable by SMT before the case analysis *)
	  introv Hk. rewrite~ HM. logics. iff ((G&E1)&E2) E1.  
	  { split. math. intros r' d' H1 H2. tests: (r' = iv).
		 (* next 5 lines provable by SMT *)
		 { intros E. applys E2 d'; math_nia. }
		 { applys~ E1. } }
	  { split. 
		  { unfolds~ no_factor. }
		  { introv Hd E. applys (proj2 E1) iv d; math_nia. } } } }
 { xrets~. introv Hk. rewrite~ HM. logics. iff (L&G). 
	 { split~. intros r' d' R1 R2. tests: (r' = iv).
	  (* subgoals provable by SMT *)
		{ intros E. rewrite~ HM in C. logics. 
		  unfold no_factor in C. rew_logic in C.
		  destruct C as [|(a&C)]. math. rew_logic in C.
		  destruct C as (b&C). rew_logic in C.
		  destruct C as (C1&C2&C3).
		  applys G a (b*d'); math_nia. } 
		{ applys~ G. } }
	 { split. math. intros r' d' R1 R2 E. applys~ G r' d'. } } 
   skip. skip. }
 { clears M. intros M LM HM. xapps~. xsimpl_credits~. } }
 { xextract as m iv M Hi LM HM N Em. xrets. 
 (* subgoals provable by SMT *)
 introv Hk. rewrite~ HM. logics. applys~ no_factor_to_prime. }
Qed.



