
let example_incr r =
  let x = !r in
  let i = ref x in  
  incr i;
  decr r;
  let y = !i in
  y

let example_array n = 
  let t = Array.make n true in
  t.(0) <- false;
  t.(1) <- false;
  t 

let example_loop n =
  let i = ref 0 in
  while !i < n do
    incr i; 
  done;
  i

let example_cross_even n t =
  let r = ref 0 in 
  while 2 * !r < n do 
    t.(2 * !r) <- false;
    incr r; 
  done

let sieve n =
  let t = Array.make n true in
  t.(0) <- false;
  t.(1) <- false;
  let i = ref 2 in
  while !i < n do
    if t.(!i) then begin
      let r = ref !i in
      while !r * !i < n do
        t.(!r * !i) <- false;
        incr r;
      done;
    end;
    incr i;
  done;
  t

