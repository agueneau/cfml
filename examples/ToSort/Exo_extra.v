Set Implicit Arguments. 
Require Import CFLib Exo_ml.
Require Import LibListZ Buffer.
Ltac auto_tilde ::= try solve [ intuition eauto with maths ].


(*--------------------------------------------------------*)

Definition prime k := 
  k > 1 /\ forall r d, 1 < d < k -> k <> r * d.

(*--------------------------------------------------------*)

Lemma example_let_spec : forall n,
  App example_let n;  
    \[]
    (fun (v:int) => \[v = 2*n]).
Proof using.
  intros.
  xcf.
  xret. xextract as Ea.
 (*   simpl. xextract. intros Ea. *)
  xret. xextract as Eb.
  xret. 
  xsimpl.
  math. 
Qed.


(*--------------------------------------------------------*)

Lemma example_incr_spec : forall r n,
  App example_incr r;  
    (r ~~> n)
    (fun (_:unit) => (r ~~> (n+1))).
Proof using.
  intros.
  xcf.
(*
  xapps.

  xapp as v. intros. subst v.
*)

  xlet.
(*  r ~~> n    r ~~> n  [x = n] *)

(* ?Q1  = fun () =>  [x = n]\ * r ~~> n *)

 xapp. simpl.


(*

  xapp_spec. 
 (*
  xapp_spec ml_get_spec. intros v.
  *)



  xapp as v. 
  intros Ev.
  xapp.
  intros _. 
  xsimpl.
  math. 
*)
admit.
Qed.

(** Same, with more direct tactic calls *)

Lemma example_incr_spec_2 : forall r n,
  App example_incr r;  
    (r ~~> n)
    (fun (_:unit) => (r ~~> (n+1))).
Proof using.
  xcf.
  xapps. 
  xapp.
  xsimpl.
Qed.


(*--------------------------------------------------------*)

Lemma example_incr_2_spec : forall n,
  App example_incr_2 n;  
    \[]
    (fun (v:int) => \[v = n+1]).
Proof using.
  xcf.
  xapp.
  xapp.
  xapp.
  xapp.
  xapps.
  xapps.
  xapp.
  xapps.
  xapps.
  xret.
  xsimpl.
  math.
Qed.


(*--------------------------------------------------------*)

Lemma example_loop_spec : forall (n:int),
  n >= 0 ->
  App example_loop n;  
    \[]
    (fun (r:loc) => (r ~~> n)).
Proof using.
  xcf. xapp. xapp.
  xwhile_inv (fun b m => 
    Hexists iv, (i ~~> iv) \* (r ~~> iv) \* \[iv <= n] \*
     \[b = isTrue(iv < n)] \* \[m = n - iv]).



























    { exists_all. xsimpl~. }
    { intros. xextract as iv Hi Eb Em.
      xapps. xret. xsimpl~. }
    { intros. xextract as iv Hi Eb Em. 
      xapps. xapps. xsimpl~. }
  xextract as m iv Hi Eb Em. xrets~.
Qed.

(* alternative, specifying the end of the loop *)

Lemma example_loop_spec_2 : forall (n:int),
  n >= 0 ->
  App example_loop n;  
    \[]
    (fun (r:loc) => (r ~~> n)).
Proof using.
  xcf. xapp. xapp.
  xseq (i ~~> n \* r ~~> n). (* new: specify the end of the loop *)
  xwhile_inv (fun b m => 
    Hexists iv, (i ~~> iv) \* (r ~~> iv) \* \[iv <= n] \*
     \[b = isTrue(iv < n)] \* \[m = n - iv]).
    { exists __ __. xsimpl~. }
    { intros. xextract as iv Hi Eb Em.
      xapps. xret. xsimpl~. }
    { intros. xextract as iv Hi Eb Em. 
      xapps. xapps. xsimpl~. }
    { xsimpl~. } 
  xrets~.
Qed.



(*--------------------------------------------------------*)

Lemma example_array_spec : 
  App example_array tt;  
    \[]
    (fun (t:loc) => Hexists M, (t ~> Array M) \* 
      \[length M = 3
    /\ forall k, 0 <= k < 3 -> M[k] = isTrue(k<1)]).
Proof using.
  xcf.
  xapp. autos. intros M EM. subst M.
  xapp. autos.
  xapp~ as v. intros Ev.
  xapp~.
  xret.
  xsimpl. split.
    rew_array~.
    introv Hk. rew_array~. case_ifs.
      subst v. rew_array~. case_if~. math.
      math.
      math.
  (* Optional forward reasoning after [intros Ev]:
    asserts Ev': (v = false).
      subst. rew_array~. case_if~.
      clear Ev. subst v. *)
Qed.


(*--------------------------------------------------------*)

Lemma list_product_spec : forall xs,
  App list_product xs; \[] (fun (v:int) => \[v = list_mul xs]).
Proof using.
  xcf. xapp. xapp. 
  xwhile_inv (fun b m => 
    Hexists nv xs1 xs2, (n ~~> nv) \* (q ~~> xs2) \*
     \[xs = xs1 ++ xs2] \* \[nv = list_mul xs1] \*
     \[b = isTrue(xs2 <> nil)] \* \[m = length xs2]).
    { exists_all. xsimpl~ (>> (@nil int) xs). }
    { intros. xextract as nv xs1 xs2 Exs En Eb Em.
      xapps. xret. xsimpl; intuition (eauto with maths). }
    { intros. xextract as nv xs1 xs2 Exs En Eb Em.
      xapps. xmatch.
      { xapp. xapps. xapps. xsimpl~ (xs1 & x).
        { subst nv. rew_list_mul. ring. }
        { subst m. rew_list~. } 
        { subst. rew_list. auto_tilde. } } }
  xextract as m nv xs1 xs2 Exs En Eb _. xapp. 
  intros v. xsimpl. subst. rew_list~.
Qed.


(*--------------------------------------------------------*)

Lemma list_product_iter_spec : forall xs,
  App list_product_iter xs; \[] (fun (v:int) => \[v = list_mul xs]).
Proof using.
  lets X: ml_list_iter_spec. clear X.
  xcf. xapp. xfun. xapp_nosimpl 
    (fun xs2 => Hexists xs1 nv, (n ~~> nv) \* 
        \[xs = xs1 ++ xs2] \* \[nv = list_mul xs1]).
  { intros x t. xextract as xs1 nv Exs En.
    xapp_body. xapps. xapps.
    xsimpl (xs1&x).
    { subst. rew_list_mul. ring. }
    { subst. rew_list~. } }
  { xsimpl~ (@nil int). }
  { intros xs1 nv Exs En. xapp. intros v.
    xsimpl~. subst. rew_list~. }
Qed.


(*--------------------------------------------------------*)

Lemma div_mul_self : forall d d',
  d > 0 -> (d * d') / d = d'.
Proof using. math_dia. Qed.

Lemma decompose_stop : forall M i,
  1 <= i < length M ->
  M[1] = 1 ->
  (forall i d, 1 < i < length M -> 
   d = M[i] -> prime d /\ (exists r, i = d * r)) ->
  (M[i] <> 1 <-> i > 1).
Proof using.
  introv Hi HM1 HM. tests: (i = 1).
  { rewrite HM1. math. }
  { forwards~ ((L&_)&_): HM i. } 
Qed.

Lemma decompose_spec : forall n t (M:list int), 
  1 <= n < length M ->
  (M[1] = 1) ->
  (forall i d, 1 < i < length M -> 
   d = M[i] -> prime d /\ (exists r, i = d * r)) ->
  App decompose n t;
    (t ~> Array M)
    (fun (xs:list int) => 
      \[n = list_mul xs] \* \[Forall prime xs]).
Proof using.
  introv Hn HM1 HM. xcf. xapp. xapp.
  xwhile_inv (fun b m => 
    Hexists rv xs, (r ~~> rv) \* (q ~~> xs) \* (t ~> Array M) \*
     \[1 <= rv <= n ] \*
     \[n = rv * list_mul xs] \* \[Forall prime xs] \*
     \[b = isTrue(rv > 1)] \* \[m = rv]).
    { exists_all. xsimpl~. rew_list_mul~. } 
    { intros. xextract as rv xs Ln En Exs Eb Em.
      xapps~. xapps~. xrets~. applys~ decompose_stop. }      
    { intros. xextract as rv xs Ln En Exs Eb Em.
      xapps. xapp~ as d. intros Ed.
      forwards~ (Pd&(d'&Ei)): HM Ed.
      asserts Ld: (d > 1). destruct~ Pd. 
      xapps. xapps. xapps. xapps. unfolds prime; math.
      xapps. intros _.
      asserts_rewrite (rv / d = d').
        { substs. applys~ div_mul_self. } 
      xsimpl~.
      { rew_list_mul. math_nia. }
      { math_nia. } 
      { math_nia. } }
  xextract as m rv xs Ln En Exs Eb Em. xapps. intros qs.
  xextracts. xsimpl~. math_nia.
Qed.
