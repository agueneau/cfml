
Set Implicit Arguments. 
Require Import CFLib Exo_ml.
Require Import LibListZ Buffer.
Ltac auto_tilde ::= try solve [ intuition eauto with maths ].


(*--------------------------------------------------------*)
(** Mini-manual *)

(** 
Specification syntax:
   - [App f x; H (fun (x:T) => H']

Heap syntax
   - \[]
   - \[P]  
   - H \* H'
   - r ~~> v
   - Hexists x, H

Tactics:
   - [intros], [exists v], [logics]
   - [rew_list], [rew_list_mul] for normalizing list operations
   - [autos], [math], [ring], [math_nia]
   - [xcf]
   - [xsimpl], or [xsimpl X1 .. X2] (to instantiate Hexists)
   - [xextract] or [xextract as E]
   - [xret], [xapp]. 
   - [xwhile_inv (fun b m => H \* [b = isTrue(..)] \* \[m = ..]]
   - [xseq H]
   - others: [xif], [xmatch]

Shortcut:
   - [xextracts] like [xextract; intros x; subst x]
   - [xrets] like [xret; xsimpl]
   - [xapps] like [xapp; xextracts]
   - [tactic~] like [tactic; autos]
   - [exists_all] like [exists __ .. __]
*)


(*--------------------------------------------------------*)
(** Prime numbers *)

Definition prime k := 
  k > 1 /\ forall r d, 1 < d < k -> k <> r * d.

(*--------------------------------------------------------*)
(*----
let example_let n =
  let a = n+1 in  
  let b = n-1 in
  a + b
----*)


Lemma example_let_spec : forall n,
  App example_let n;  
    \[]
    (fun (v:int) => \[v = 2*n]).
Proof using. 
  skip.
  (* use: [math], [xextract], [xsimpl], [xcf], [xret] *)
Qed.


(*--------------------------------------------------------*)
(*----
let example_incr r =
  r := !r + 1
----*)

Lemma example_incr_spec : forall r n,
  App example_incr r;  
    (r ~~> n)
    (fun (_:unit) => (r ~~> (n+1))).
Proof using.
  skip.
  (* use also: [xapp] or [xapp as x] or [xapps] (try them all) *)
Qed.


(*--------------------------------------------------------*)
(*----
let example_incr_2 n =
  let i = ref 0 in 
  let r = ref n in 
  decr r;
  incr i;
  r := !i + !r;
  !i + !r
----*)

Lemma example_incr_2_spec : forall n,
  App example_incr_2 n;  
    \[]
    (fun (v:int) => \[v = n+1]).
Proof using.
  skip.
Qed.


(*--------------------------------------------------------*)
(*----
let example_loop n =
  let i = ref 0 in  
  let r = ref 0 in
  while !i < n do
    incr i;
    incr r;  
  done;
  r
----*)

Lemma example_loop_spec : forall (n:int),
  n >= 0 ->
  App example_loop n;  
    \[]
    (fun (r:loc) => (r ~~> n)).
Proof using.
  skip.
  (* use:
  [xcf], [xsimpl], [xextract], [xapp], [xapps], [xret]

  and also:

  xwhile_inv (fun b m => 
    Hexists .., (.. ~~> ..) \* \[...] \*
     \[b = isTrue(...)] \* \[m = ...]).
  *)
Qed.


(*--------------------------------------------------------*)
(*----
let example_array () = 
  let t = Array.make 3 true in
  t.(2) <- false;
  t.(1) <- t.(2);
  t
----*)

Lemma example_array_spec : 
  App example_array tt;  
    \[]
    (fun (t:loc) => Hexists M, (t ~> Array M) \* 
      \[length M = 3
    /\ forall k, 0 <= k < 3 -> M[k] = isTrue(k<1)]).
Proof using.
  skip.
  (* to reason about arrays, use [rew_array] and [case_if]. *)
Qed.


(*--------------------------------------------------------*)
(*----
let list_product xs =
  let n = ref 1 in
  let q = ref xs in
  while !q <> [] do
    match !q with
    | [] -> assert false
    | x::t -> q := t; n := !n * x
  done;
  !n
----*)

Lemma test :forall (x:int) a b c,
  x :: (a ++ b) ++ c = (x::a) ++ (b ++ c).
Proof. intros. rew_list. auto. Qed.


Lemma list_product_spec : forall xs,
  App list_product xs; \[] (fun (v:int) => \[v = list_mul xs]).
Proof using.
  xcf. xapp. xapp. 
  xwhile_inv (fun b m => 
    Hexists nv xs1 xs2, (n ~~> nv) \* (q ~~> xs2) \*
     \[xs = xs1 ++ xs2] \* \[nv = list_mul xs1] \*
     \[b = isTrue(xs2 <> nil)] \* \[m = length xs2]).
    { exists_all. xsimpl~ (>> (@nil int) xs). 
      (* Note: to prove a goal of the form [H ==> Hexists x1 x2, H'],
       by providing v1 for x1 and v2 for x2, we use the tactic:
       [xsimpl (>> v1 v2)]  *)
    }
    { skip. }
    { skip. 

(* use [xmatch] *)
      (* [rew_list] *)

(* autorewrite with rew_list *)
      (* use [rew_list_mul] to normalize [list_mul] expressions *)
    }
  skip. 
Qed.


(*--------------------------------------------------------*)
(*----
let decompose n t =
  let r = ref n in
  let q = ref [] in
  while !r > 1 do
    let d = t.(!r) in
    q := d :: !q;
    r := !r / d;
  done;
  q
----*)

(* M[i] contains one prime factor of i, except M[1] = 1. *)

Lemma div_mul_self : forall d d',
  d > 0 -> (d * d') / d = d'.
Proof using. math_dia. Qed.

Lemma decompose_stop : forall M i,
  1 <= i < length M ->
  M[1] = 1 ->
  (forall i d, 1 < i < length M -> 
   d = M[i] -> prime d /\ (exists r, i = d * r)) ->
  (M[i] <> 1 <-> i > 1).
Proof using.
  introv Hi HM1 HM. tests: (i = 1).
  { rewrite HM1. math. }
  { forwards~ ((L&_)&_): HM i. } 
Qed.

Lemma decompose_spec : forall n t (M:list int), 
  1 <= n < length M ->
  (M[1] = 1) ->
  (forall i d, 1 < i < length M -> 
   d = M[i] -> prime d /\ (exists r, i = d * r)) ->
  App decompose n t;
    (t ~> Array M)
    (fun (xs:list int) => 
      \[n = list_mul xs] \* \[Forall prime xs]).
Proof using.
  skip.
  (* hint: at the end the loop body, simplify the division
     of the form (d * d' / d) using [div_mu_self]
     before trying to establish the invariant. *)
Qed.



