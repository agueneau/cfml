open OrderedType
(*open HeapSig*)

module SelfBalancedHeap (Element : OrderedType) (* : Heap *) =
  (* (Heap with module Element = Element)*)
struct
  module Element = Element

  exception Error

  (* todo: allow recursive def directly *)
  type heaps = Empty | Node of heaps * Element.t * heaps
  type heap = heaps

  let empty = Empty

  let is_empty = function
     | Empty -> true
     | _ -> false
  (* alternative:
  let is_empty h = 
    h = Empty
  *)

  let rec merge h1 h2 = 
     match h1, h2 with
     | _, Empty -> h1
     | Empty, _ -> h2
     | Node (l1, x1, r1), Node (l2, x2, r2) ->
         if Element.compare x1 x2 <= 0
            then Node (r1, x1, merge l1 h2)
            else Node (r2, x2, merge l2 h1)

  let insert x h = 
     merge (Node (Empty, x, Empty)) h

  let pop_min = function
     | Empty -> raise Error
     | Node (a, x, b) -> (x, merge a b)

end

