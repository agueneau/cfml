open OrderedType
(*open HeapSig*)

module MutableSelfBalancedHeap (Element : OrderedType) (* : Heap *) =
  (* (Heap with module Element = Element)*)
struct
  module Element = Element

  exception Error

  type heap = contents ref
  and contents = MEmpty | MNode of heap * Element.t * heap

  let empty () =
    ref MEmpty

  let is_empty h = 
     match !h with
     | MEmpty -> true
     | _ -> false

  (* [merge h1 h2] merges elements of [h2] into [h1] *)
  let rec merge h1 h2 = 
     match !h1, !h2 with
     | _, MEmpty -> ()
     | MEmpty, _ -> h1 := !h2
     | MNode (l1, x1, r1), MNode (l2, x2, r2) ->
         if Element.compare x1 x2 <= 0
            then merge l1 h2
            else (merge l2 h1; h1 := !h2)

  let insert x h = 
     merge h (ref (MNode (empty(), x, empty())))

  let pop_min h = 
     match !h with
     | MEmpty -> raise Error
     | MNode (a, x, b) -> 
        merge a b;
        h := !a;
        x

end

