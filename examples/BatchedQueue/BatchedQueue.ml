exception EmptyStructure

module BatchedQueue =
struct

   type 'a queue = 'a list * 'a list 

   let empty : 'a queue = ([],[])

   let is_empty : 'a queue -> bool = function  
     | [],_  -> true
     | _ -> false

   let checkf : 'a queue -> 'a queue = function
     | [],r -> (List.rev r, [])
     | q -> q

   let push ((f,r) : 'a queue) (x: 'a): 'a queue =
      checkf (f, x::r)

   let pop  : 'a queue -> ('a * 'a queue) = function
     | [], _ -> raise EmptyStructure
     | x::f, r -> (x, checkf (f,r))

   (* alternative: *)

   let head : 'a queue -> 'a = function
     | [], _ -> raise EmptyStructure
     | x::f, r -> x

   let tail : 'a queue -> 'a queue = function 
     | [], _ -> raise EmptyStructure
     | x::f, r -> checkf (f,r)

end
