

type 'a queue = {
  mutable size : int;
  mutable front : 'a list;
  mutable back : 'a list; }

let is_empty (q:'a queue) =
  (q.size = 0)

let push (x:'a) (q:'a queue) =
  q.size <- q.size + 1;
  q.back <- x :: q.back

let pop (q:'a queue) =
   q.size <- (q.size - 1);
   match q.front with
   | [] ->    
      begin match List.rev q.back with
      | [] -> assert false
      | x::f' -> q.front <- f'; q.back <- []; x
      end
   | x::f' -> q.front <- f'; x

