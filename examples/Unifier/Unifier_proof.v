Set Implicit Arguments.
Require Import CFLib LibSet LibPer LibMap LibRelation UnionFind_proof Term_ml Term_proof Unifier_ml.

(* ---------------------------------------------------------------------------- *)

(* CFML hackery. *)

(* TODO: move to CFHeaps *)

Ltac user_hcancel H := fail.

Ltac hsimpl_step tt ::=
  match goal with |- ?HL ==> ?HA \* ?HN =>
  match HN with
  | ?H \* _ => first [ user_hcancel H |
    match H with
    | \[] => apply hsimpl_empty
    | \[_] => apply hsimpl_prop
    | heap_is_pack _ => hsimpl_extract_exists tt
    | _ \* _ => apply hsimpl_assoc
    | heap_is_single _ _ => hsimpl_try_same tt
    | Group _ _ => hsimpl_try_same tt
    | ?H => hsimpl_find_same H HL 
    | hdata _ ?l => hsimpl_find_data l HL ltac:(hsimpl_find_data_post)
    | ?x ~> Id _ => check_noevar x; apply hsimpl_id
    | ?x ~> _ _ => check_noevar x; apply hsimpl_id_unify
    | ?H => apply hsimpl_keep
    end ]
  | \[] => fail 1
  | _ => apply hsimpl_starify
  end end.

Tactic Notation "hcancel" "~" := hcancel; auto_tilde.
Tactic Notation "hcancel" "*" := hcancel; auto_star.
Tactic Notation "hcancel" "~" constr(L) :=
  hcancel L; auto_tilde.
Tactic Notation "hsimpl" "*" constr(L) :=
  hcancel L; auto_star.

(* TODO: keep this here; but can we find a more modular way of proceeding? *)

(** The following tactic gives a hint to simplify goals 
    of the form [ .. * UF F B * .. ==> .. * UF ? ? * .. ]
    by unifying the cancellable arguments *)

Ltac user_hcancel H ::=
  match H with
  | UF _ _ => hsimpl_try_same tt
  end.

(* ---------------------------------------------------------------------------- *)

(* A valuation maps variables to trees. *)

Definition valuation (variable : Type) : Type :=
  variable -> tree.

(* ---------------------------------------------------------------------------- *)

(* A constraint maps valuation to propositions. This is a semantic constraint:
   it is just a set of valid valuations. *)

Definition constraint (variable : Type) : Type :=
  valuation variable -> Prop.

Definition satisfiable (variable : Type) (C : constraint variable) :=
  exists phi, C phi.

(* ---------------------------------------------------------------------------- *)

(* The abstract predicate [UNIF C] describes the unifier's state. Here, [C] is
   a semantic constraint. The predicate [UNIF C] represents the ownership of
   the unifier's state, and, at the same time, indicates that this state
   encodes the constraint [C]. *)

(* In the following, memory locations play the role of variables. *)

Implicit Type B : loc -> loc -> Prop.

Implicit Type F : loc -> structure.

Implicit Type phi : valuation loc.

Implicit Type C : constraint loc.

(* If [B] is a partial equivalence relation, then [vveqs B] is a semantic
   constraint. It is true of every valuation [phi] that respects the
   variable-variable equality constraints represented by [B]. *)

Definition vveqs B phi :=
  forall x y, B x y -> tree_eq (phi x) (phi y).

(* If [F] is a mapping of variables to structures, then [vteqs F] is a
   semantic constraint. It is true of every valuation [phi] that
   respects the variable-term equality constraints represented by [F]. *)

Definition vteqs F phi :=
  forall x, match F x with
  | V => True
  | T tm => tree_eq (phi x) (Tree (map phi tm))
  end.

(* The proposition [UNIF C] holds when we have an instance of the union-find
   algorithm, described by the proposition [UF B F], and when the constraint
   [C] is equivalent to the conjunction of the constraints [vveqs B] and
   [vteqs F]. That is, [C] reflects exactly the variable-variable equations
   encoded by the partial equivalence relation [B] and the variable-term
   equations encoded by the mapping [F] of equivalence classes to
   descriptors. *)

Notation UF :=
  (@UF structure).

Definition UNIF C : hprop :=
  Hexists B F,
  UF B F \* [
    forall phi,
    C phi <-> (vveqs B phi /\ vteqs F phi)
  ].

(* ---------------------------------------------------------------------------- *)

(* Satisfiability. *)

(* We prove that, whenever [UNIF C] holds, [C] is satisfiable. That is, as
   long as the unifier runs (without failing), its state encodes a satisfiable
   constraint. *)

(* The following definition exhibits a solution of the constraint. The
   solution is co-inductively defined in terms of [F] alone. It is very
   simple: for every variable [x], if [F x] is [V], which means that [F]
   imposes no constraint on this variable, then [x] is mapped to a (fixed)
   arbitrary tree; and if [F x] is [T tm], which means that [F] imposes some
   structure on this variable, then we follow this requirement to define the
   root of the tree, and we use [solution] (co-inductively) to define the
   sub-trees. *)

CoFixpoint solution F (x : loc) : tree :=
  match F x with
  | V =>
      dummy_tree
  | T tm =>
      Tree (map (solution F) tm)
  end.

(* By construction, the valuation [solution F] satisfies the variable-term
   equations encoded by [F]. *)

Lemma solution_vteqs:
  forall F,
  vteqs F (solution F).
Proof.
  intros F x. case_eq (F x).
  tauto.
  intros tm h. tree_decompose (solution F x). rewrite h; simpl. eauto.
Qed.

(* If [F] is compatible with [B], then the valuation [solution F] satisfies
   the variable-variable equations encoded by [B]. *)

Lemma solution_vveqs:
  forall B F,
  (forall x y, B x y -> F x = F y) ->
  vveqs B (solution F).
Proof.
  intros B F compat x y Bxy.
  tree_decompose (solution F x).
  tree_decompose (solution F y).
  rewrite <- (compat _ _ Bxy).
  eauto.
Qed.

(* Thus, if [UNIF C] holds, then [C] is satisfiable. *)

Lemma satisfiability:
  forall C,
  UNIF C ==>+ \[ satisfiable C ].
Proof.
  intro. unfold UNIF. hextract as B F h.
  (* The predicate [UF B F] guarantees that [F] is compatible with [B]. *)
  hchange (compatibility V). hextract as compat.
  hcancel; eauto 1. 
  (* Here comes the place we are supposed to show that [C] is satisfiable. *)
  (* The witness is [solution F]. *)
  exists (solution F). rewrite h. eauto using solution_vteqs, solution_vveqs.
Qed.

