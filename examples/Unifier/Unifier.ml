open UnionFind
open Term

type structure =
  | V
  | T of structure UnionFind.cell Term.term

type variable =
  structure UnionFind.cell

let rec unify v1 v2 =
  if UnionFind.same v1 v2 then
    ()
  else
    let s1 = UnionFind.find v1
    and s2 = UnionFind.find v2 in
    match s1, s2 with
    | V, _ ->
        UnionFind.union v1 v2
    | _, V ->
        UnionFind.union v2 v1
    | T t1, T t2 ->
        UnionFind.union v1 v2;
        Term.iter2 unify t1 t2

