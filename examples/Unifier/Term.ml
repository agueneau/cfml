type 'a term =
  | TUnit
  | TArrow of 'a * 'a

exception Iter2

let iter2 f t1 t2 =
  match t1, t2 with
  | TUnit, TUnit ->
      ()
  | TArrow (domain1, codomain1), TArrow (domain2, codomain2) ->
      f domain1 domain2;
      f codomain1 codomain2
  | _, _ ->
      raise Iter2

