
module type HashSig = 
sig
   type ('a, 'b) t
   val create : int -> ('a, 'b) t
   val mem : ('a, 'b) t -> 'a -> bool
   val find : ('a, 'b) t -> 'a -> 'b
   val add : ('a, 'b) t -> 'a -> 'b -> unit
end


module MemoFunc (HashMod : HashSig) = 
struct

let memo funct =
   let h = HashMod.create 0 in
   let rec f x =
      if HashMod.mem h x then begin
         HashMod.find h x
      end else begin
         let v = funct f x in
         HashMod.add h x v;
         v
      end in
   f

end
