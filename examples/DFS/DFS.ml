






(*************************************************************************)
(** Graph representation by adjacency lists *)

module Graph = struct

type t = (int list) array 

let nb_nodes (g:t) = 
  Array.length g

let iter_edges (f:int->unit) (g:t) (i:int) =
  List.iter (fun j -> f j) g.(i)

end


(*************************************************************************)
(** DFS Algorithm, Sedgewick's presentation *)

type color = White | Gray | Black

 let rec dfs_from g c i =
    c.(i) <- Gray;
    Graph.iter_edges (fun j -> 
       if c.(j) = White 
          then dfs_from g c j) g i;
    c.(i) <- Black

let dfs_main g rs =
   let n = Graph.nb_nodes g in
   let c = Array.make n White in
   List.iter (fun i -> 
     if c.(i) = White then
       dfs_from g c i) rs;
   c
