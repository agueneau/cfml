Set Implicit Arguments.
Require Export Rbase Rfunctions.
Require Import CFLib LibSet LibMap LibArray LibInt.
Require Import CapacityParamSig_ml.
Require Import SequenceSig_ml SequenceSig_proof.
Require Import BootSimpleChunkedSeq_ml.
Require Import Common_proof.

Open Scope R_scope.
Open Scope Z_scope.
Open Scope list_scope.
Open Scope heap_scope_advanced.
Require Import LibOrder.

(*------------------------------------------------------*)
(** Hypothesis "capa > 1" *)

Module Type CapaGtOneSig.

Declare Module Q : SequenceSig_ml.MLFixedCapacityS.

Parameter capacity_gt_one : Q.capacity > 1.

End CapaGtOneSig.


(*------------------------------------------------------*)
(** instantiations *)

Module MakeSpec
   (Chunk : SequenceSig_ml.MLFixedCapacityS)
   (ChunkSpec : SequenceSigFixedCapacitySpec with Module Q:=Chunk)
   (CapaGtOne : CapaGtOneSig with Module Q:=Chunk).

(** instantiations *)

Module Import Q := MLMake Chunk.

Module ChunkSpecOwn := SequenceSigOwnFixedCapacitySpec_from_SigFixedCapacitySpec (Chunk) (ChunkSpec).


(*------------------------------------------------------*)
(** representation predicates *)

Notation "'RChunk' R" := (@ChunkSpecOwn.Seq _ _ R) (at level 8).

Notation "'TF' R Rmid fo fi mid bi bo" :=
  (Q.T (RChunk R) (RChunk R) Rmid (RChunk R) (RChunk R)
  fo fi mid bi bo)
  (R at level 0, Rmid at level 0, fo at level 0, fi at level 0, mid at level 0, bi at level 0, bo at level 0, at level 30).

Notation "'TU'" := (T Id Id Id Id Id).

Notation "'length'" := (fun L => my_Z_of_nat (LibList.length L)).


(*------------------------------------------------------*)
(** invariants *)

Definition capa := Chunk.capacity.

Definition full (A:Type) (L:list A) :=
  length L = capa. 

Definition middle_conseq_ok (A:Type) (mid:list(list A)) :=
  ForallConseq (fun c1 c2 => length c1 + length c2 > capa) mid.

Definition chunk_ok {A:Type} (L:list A) :=
  0 < length L <= capa.

Definition middle_chunks_ok (A:Type) (mid:list(list A)) :=
  Forall chunk_ok mid.

Record inv (A : Type) (L fo fi : list A) mid bi bo := build_inv {
  inv_items : L = fo ++ fi ++ concat mid ++ bi ++ bo;
  inv_front_fill : fi = nil \/ full fi;
  inv_back_fill : bi = nil \/ full bi;
  inv_mid_chunks : middle_chunks_ok mid;
  inv_mid_conseq : middle_conseq_ok mid }.

Inductive tree (A : Type) : Type :=
  | tree_shallow : list A -> tree A
  | tree_deep : list A -> list A -> tree (list A) -> list A -> list A -> tree A.

Implicit Arguments tree_shallow [A].
Implicit Arguments tree_deep [A].

Inductive tree_inv (fixed:bool) : forall A, tree A -> list A -> Prop :=
  | tree_inv_shallow : forall A (L:list A),
      length L <= capa ->
      tree_inv fixed (tree_shallow L) L
  | tree_inv_deep : forall A (fo fi bi bo : list A) tmid mid L, 
      inv L fo fi mid bi bo ->
      (if fixed then length L > 1 else True) ->
      tree_inv true tmid mid ->
      tree_inv fixed (tree_deep fo fi tmid bi bo) L.

Definition item_potential := 
  ((ChunkSpec.create_cst + ChunkSpec.op_cst) / (capa - 1))%R.

Definition side_potential (A:Type) (ko ki : list A) :=
  If ki = nil then 0%R else (length ko * item_potential)%R.

Definition potential (A:Type) (fo fi bi bo : list A) :=
  (side_potential fo fi + side_potential bo bi)%R.

Fixpoint Repr (a A : Type) (R:htype A a) (T : tree A) (s : loc) :=
  match T with
  | tree_shallow L => Hexists (pc:Chunk.t a) pfi pbi,
        s ~> TU pc pfi null pbi pc 
     \* pc ~> RChunk R L
  | tree_deep fo fi mid bi bo => 
        s ~> TF R (@Repr _ _ (RChunk R)) fo fi mid bi bo
     \* $ (potential fo fi bi bo)
  end.

Definition Seq (a A : Type) (R:htype A a) (L : list A) (q : loc) :=
  Hexists (T : tree A), q ~> Repr R T \* \[tree_inv true T L].


(*------------------------------------------------------*)
(** auxiliary definitions *)

Notation "'\R'" := (Rdefinitions.R).

Notation "'RMiddle' R" := (@Repr _ _ (RChunk R)) (at level 8).


Fixpoint depth A (T:tree A) : nat :=
  match T with
  | tree_shallow _ => 0%nat
  | tree_deep _ _ tmid _ _ => (1 + (depth tmid))%nat
  end.

Definition tree_is_shallow A (T:tree A) :=
  match T with 
  | tree_shallow _ => True
  | tree_deep _ _ _ _ _ => False
  end.

Definition tree_front A (T:tree A) :=
  match T with 
  | tree_shallow L => L
  | tree_deep L _ _ _ _ => L
  end.


Definition base := ((capa + 1)/2)%R.

Definition inv_middle A L (mid:list (list A)) :=
  concat mid = L /\ middle_chunks_ok mid /\ middle_conseq_ok mid.

Definition max_depth (n:nat) : R :=
  If (n <= 1)%Z then 0%R else (1 + log_base base (n - 1))%R.

Definition max_depth_seq A (L:list A) := max_depth (LibList.length L).


Definition push_cost := 
  (ChunkSpec.op_cst + item_potential)%R.

Definition merge_cost :=  
  (push_cost + push_cost + ChunkSpecOwn.append_cst)%R.


(*------------------------------------------------------*)
(** to be generated *)

Lemma t_of_not_null : forall s a A1 A2 A3 A4 A5
  (T1:htype A1 (Chunk.t a)) (T2:htype A2 (Chunk.t a))
  (T3:htype A3 (Q.t (Chunk.t a))) 
  (T4:htype A4 (Chunk.t a)) (T5:htype A5 (Chunk.t a)) 
  x1 x2 x3 x4 x5,
  hkeep (s ~> T T1 T2 T3 T4 T5 x1 x2 x3 x4 x5) ==> \[s <> null].
Proof.
  intros. hunfold T. hextract. intros. 
  hchanges (@heap_is_single_impl_null s). auto.
Qed.


(*------------------------------------------------------*)
(** paraphrase of definitions *)

Lemma Repr_focus : forall (s : loc) a A (R:htype A a) (T : tree A),  (* not used *)
  s ~> Repr R T ==> Hexists pfo pfi pmid pbi pbo,
  s ~> TU pfo pfi pmid pbi pbo \*
  match T with
  | tree_shallow L => 
     \[pfo = pbo /\ pmid = null] \* pfo ~> RChunk R L
  | tree_deep fo fi mid bi bo => 
        pfo ~> RChunk R fo 
     \* pfi ~> RChunk R fi
     \* pmid ~> RMiddle R mid
     \* pbi ~> RChunk R bi
     \* pbo ~> RChunk R bo
     \* $ (potential fo fi bi bo)
  end.
Proof.
  intros. destruct T0.
  hunfold Repr. hsimpl~.
  hunfold Repr at 1. rewrite T_convert. hsimpl.
Admitted. (*faster*)

Lemma Repr_not_null : forall s a A (R:htype A a) (T:tree A),
  hkeep (s ~> Repr R T) ==> \[s <> null].
Proof.
  intros. destruct T0; hunfold Repr.
   hextract. intros. hchange (@t_of_not_null s). hsimpl~.
   hchange (@t_of_not_null s). hsimpl~.
Admitted. (*faster*)

Lemma Repr_unfocus_shallow : forall (s : loc) a A (R:htype A a),
   forall (pc:Chunk.t a) pfi pbi L,
   s ~> TU pc pfi null pbi pc \* pc ~> RChunk R L ==>
   s ~> Repr R (tree_shallow L).
Proof. intros. hunfolds Repr. Qed.

Lemma Repr_unfocus_deep : forall (s : loc) a A (R:htype A a),
   forall fo fi mid bi bo,
   s ~> TF R (RMiddle R) fo fi mid bi bo \* $ potential fo fi bi bo
   ==>
   s ~> Repr R (tree_deep fo fi mid bi bo).
Proof. intros. hunfolds Repr. Qed.

Lemma Seq_focus : forall (q : loc) (a A : Type) (R:htype A a) (L : list A),
  q ~> Seq R L ==> Hexists T, \[tree_inv true T L] \* q ~> Repr R T.
Proof. intros. hunfolds~ Seq. Qed.

Lemma Seq_unfocus : forall (q : loc) (a A : Type) (R:htype A a) (L : list A)  T,
  tree_inv true T L -> q ~> Repr R T ==> q ~> Seq R L.
Proof. intros. hunfolds~ Seq. Qed.



(*------------------------------------------------------*)
(** tactics *)

Hint Extern 10 (_ = _ :> list _) => subst; rew_list.
Hint Constructors Forall ForallConseq.
Hint Resolve Forall_last Forall_app.
Hint Unfold middle_chunks_ok middle_conseq_ok.

Ltac pot_simpl_core := 
  unfold potential; try simpl_credits_core; unfold side_potential; 
  repeat case_if; rew_length.

Ltac pot_simpl := 
  pot_simpl_core; try simpl_credits; 
  try match goal with (* todo: remove this *)
  | |- (item_potential >= 0)%R => auto 
  | |- (0 <= item_potential)%R => apply ineq_ge_to_le; auto
  end.


Ltac lmath :=
  unfolds chunk_ok, full, capa; try rew_length in *; math. 

Hint Extern 1 (_ = _ :> nat) => lmath.
Hint Extern 1 ((_ < _)) => lmath.
Hint Extern 1 ((_ > _)) => lmath.
Hint Extern 1 ((_ >= _)) => lmath.
Hint Extern 1 ((_ <= _)) => lmath.
Hint Extern 1 ((_ < _)%Z) => lmath.
Hint Extern 1 ((_ > _)%Z) => lmath.
Hint Extern 1 ((_ >= _)%Z) => lmath.
Hint Extern 1 ((_ <= _)%Z) => lmath.
Hint Extern 1 (chunk_ok _) => lmath.


(*------------------------------------------------------*)
(** extra tactics *)

Hint Constructors Forall.
Hint Extern 10 (_ = _ :> list _) => subst; rew_list.
Hint Extern 1 (~ full _) => simpl.
Hint Constructors ForallConseq.

Implicit Arguments nil [ [A] ].

Ltac hunfold_base E ::=
  match E with 
  | Repr => unblock Repr; hunfold_wrapper Repr
  | _ => hunfold_wrapper E
  end.

Tactic Notation "hunfoldb" constr(E) := 
  hunfold_base E.

Definition capa_gt_one := CapaGtOne.capacity_gt_one. (* capa > 1. *)
Ltac math_0 ::= hint capa_gt_one. 


(*------------------------------------------------------*)
(** extra lemmas *)

Lemma potential_shift : forall (A:Type) (fo fi bo bi : list A),
  fi <> nil -> full fo ->
  (potential fo fi bi bo >=
    potential nil fo bi bo 
  + ChunkSpec.create_cst + ChunkSpec.op_cst + item_potential)%R.
Proof.
  introv Fi Fo. unfold potential. 
  asserts_rewrite (side_potential nil fo = 0). pot_simpl; auto.
  simpl_credits. simpl_ineq. pot_simpl. rewrite Fo. unfold item_potential. 
  rewrite (@R_minus_one_plus_one capa) at 2. simpl_credits.
Qed.

Lemma middle_size_bound_ind : forall A (m:list (list A)),
  middle_chunks_ok m -> middle_conseq_ok m -> 
  (capa + 1) * (length m - 1) <= 2 * (LibList.length (concat m) - 1).
Proof.
  introv MO MC. rewrite length_concat. sets_eq n: (LibList.length m).
  gen m. induction_wf: lt_wf n. intros. subst n. destruct m as [|x m].
    unfold sum_length. rew_list. lmath.
    destruct m as [|y m].
      unfold sum_length. rew_list. inverts MO. unfolds chunk_ok. lmath.
      lets (G&MC'): ForallConseq_cons2_inv MC. unfold sum_length. rew_list.
       fold (sum_length m). inverts MO as Cx MO. inverts MO as Cy MO'.
       forwards~ C: IH MO' MC'. lmath. 
       asserts_rewrite (((capa + 1)%I * ((1 + (1 + LibList.length m))%nat - 1)%I)
         = ((capa + 1)%I + ((capa + 1)%I + (capa + 1)%I * ((LibList.length m) - 1)%I))).
         sets g: (capa+1)%I.
         repeat rewrite int_nat_add.
         repeat rewrite int_mul_sub_distrib.
         repeat rewrite int_mul_add_distrib. math.         
       rewrite double in *. math.
Qed.

Lemma middle_size_bound : forall A (m:list (list A)),
  middle_chunks_ok m -> middle_conseq_ok m -> 
  (base * (length m - 1) <= (LibList.length (concat m) - 1))%R.
Proof.
  introv HK HC. forwards* M: middle_size_bound_ind m.
  lets N: R_le_Z (rm M). autorewrite with rew_real_int in N.
  unfold base. applys R_le_mul_pos_back_l 2%R.
  apply R_two_ge_zero. rewrite R_two_halves_val.
  auto.
Qed. 

Lemma middle_log_size_bound : forall A (m:list (list A)) n, 
  middle_chunks_ok m -> middle_conseq_ok m -> 
  LibList.length (concat m) <= n ->
  (1 + log_base base (length m - 1) <= log_base base (n - 1))%R.
Proof.
  introv HK HC Le. rewrite <- log_base_mul.
  lets M: (middle_size_bound HK HC).
  lets N: log_base_increasing base (rm M). rew_ineq (rm N).
  apply log_base_increasing. apply R_le_sub_r. apply R_le_Z. math.
Qed.

Lemma middle_max_depth : forall A (m:list (list A)) L (n:nat),
  inv_middle L m -> (LibList.length L <= n) -> (n > 1)%Z -> 
  (1 + max_depth (LibList.length m) <= max_depth n)%R.
Proof.
  introv (E&HK&HC) LL G1. 
  lets N: middle_log_size_bound n HK HC __. subst L. math. 
  unfold max_depth at 2. case_if. 
  unfold max_depth. case_if.
    do 2 simpl_ineq. apply ineq_ge_to_le. apply log_base_nonneg.
     rewrite <- R_int_one. rewrite <- R_sub_int. apply R_ge_Z. math.
    simpl_ineq. rew_ineq <- N. simpl_ineq. 
Qed.

Lemma middle_max_depth' : forall A (m:list (list A)) (L':list A) L,
  inv_middle L m -> (LibList.length L <= LibList.length L') -> (LibList.length L' > 1)%Z -> 
  (1 + max_depth_seq m <= max_depth_seq L')%R.
Proof. intros. applys* middle_max_depth. Qed.

Lemma max_depth_seq_incr : forall A (L1 L2 : list A), 
  LibList.length L1 <= LibList.length L2 ->
  (max_depth_seq L1 <= max_depth_seq L2)%R.
Proof.
  introv G. unfold max_depth_seq, max_depth. case_if; case_if.
  simpl_ineq.
  forwards N: (@log_base_nonneg (LibList.length L2 - 1)%R base). 
    rewrite <- R_int_one. rewrite <- R_sub_int. apply R_ge_Z. math.
   rew_ineq N. simpl_ineq. apply R_zero_le_one. 
  false. math.
  apply R_le_add_l. apply log_base_increasing. apply R_le_sub_r. apply R_le_Z. math.
Qed.

Lemma tree_depth_bound : forall A (T:tree A) L,
  tree_inv true T L -> (depth T <= max_depth (LibList.length L))%R.
Proof.
  intros A T L H. sets_eq Tr: true. gen EQTr.
  induction H; intro_subst; simpl depth.
  (* case shallow *)
  math_rewrite (0%nat = 0%Z :> Z). rewrite R_int_zero.
   unfold max_depth. case_if.
   simpl_ineq. lets N: log_base_nonneg (LibList.length L - 1)%Z base __.
     rewrite <- R_int_one. apply R_ge_Z. math.
   rew_ineq N. autorewrite with rew_real_int. simpl_ineq. 
   apply R_zero_le_one.
  (* case deep *)
  destruct H as [EQ _ _ HK HC].
  math_rewrite (forall n:nat, S n = (1 + n)%Z :> Z).
  autorewrite with rew_real_int.
  lets K: IHtree_inv __. auto. rew_ineq K. clear K.
  applys~ middle_max_depth (concat mid).
    splits~. subst L. rew_list. math.
Qed.

Lemma max_depth_seq_nonneg : forall A (L:list A),
  (max_depth_seq L >= 0)%R.
Proof.
  intros. unfold max_depth_seq, max_depth. case_if. credits_nonneg.
  lets E: (>> log_base_nonneg (LibList.length L - 1) base __); [|rew_ineq E].
  rewrite <- R_int_one. applys R_ge_Z. math.
  rewrite R_sub_int. simpl_credits. apply R_zero_le_one.
Qed. 

Hint Resolve max_depth_seq_nonneg : credits_nonneg.


(*------------------------------------------------------*)
(** lemmas *)

Lemma inv_middle_from_inv : forall A (L:list A) fo fi mid bi bo,
  inv L fo fi mid bi bo -> inv_middle (concat mid) mid.
Admitted.

Hint Resolve inv_middle_from_inv.

Lemma not_full_nil : forall A,
  ~ full (@nil A).
Admitted.

Hint Resolve not_full_nil.

Lemma item_potential_nonneg : (item_potential >= 0)%R.
Admitted.

Lemma side_potential_nonneg : forall A (ko ki : list A),
  (side_potential ko ki >= 0)%R.
Admitted.

Hint Resolve item_potential_nonneg side_potential_nonneg.

Lemma push_cost_nonneg : (push_cost >= 0)%R.
Proof. unfold push_cost. simpl_credits. Qed.

Hint Resolve push_cost_nonneg : credits_nonneg.

Lemma merge_cost_nonneg : (merge_cost >= 0)%R.
Proof. unfold merge_cost. simpl_credits. Qed.

Hint Resolve merge_cost_nonneg : credits_nonneg.

Lemma concat_middle_eq_nil_inv : forall A (mid:list (list A)),
  concat mid = nil -> middle_chunks_ok mid -> mid = nil.
Admitted.

Lemma middle_conseq_push_front_full : forall A (fi:list A) mid,
  middle_conseq_ok mid -> 
  middle_chunks_ok mid ->
  full fi -> 
  middle_conseq_ok (fi :: mid).
Admitted.

Lemma middle_conseq_push_back_full : forall A (fi:list A) mid,
  middle_conseq_ok mid -> 
  middle_chunks_ok mid ->
  full fi -> 
  middle_conseq_ok (mid & fi).
Admitted.

Lemma middle_conseq_tail : forall A (LC:list A) mid,
  middle_conseq_ok (LC::mid) -> 
  middle_conseq_ok mid.
Admitted.

Hint Resolve middle_conseq_tail.

Lemma middle_conseq_push_back_merged : forall A mid (L1 L2:list A),
  middle_conseq_ok (mid & L1) ->
  length L1 + length L2 <= capa ->
  middle_conseq_ok (mid & (L1 ++ L2)).
Admitted.

Lemma inv_middle_push_front_full : forall A mid (ci:list A),
  inv_middle (concat mid) mid ->
  full ci ->
  inv_middle (ci ++ concat mid) (ci :: mid).
Admitted.

Lemma inv_middle_push_front_full_and_nonempty : forall A mid ci (co:list A),
  inv_middle (concat mid) mid ->
  full ci -> 0 < length co <= capa ->
  inv_middle (co ++ ci ++ concat mid) (co :: ci :: mid).
Admitted.

Lemma inv_middle_push_back_full : forall A mid (ci:list A),
  inv_middle (concat mid) mid ->
  full ci ->
  inv_middle (concat mid ++ ci) (mid & ci).
Admitted.

Lemma inv_middle_push_back_full_and_nonempty : forall A mid ci (co:list A),
  inv_middle (concat mid) mid ->
  full ci -> 0 < length co <= capa ->
  inv_middle (concat mid ++ ci ++ co) (mid & ci & co).
Admitted.

Lemma list_not_nil_length_pos : forall A (L:list A),
  L <> nil -> length L > 0.
Admitted.


(*------------------------------------------------------*)
(** verification *)

Lemma empty_spec : forall a,
  Spec empty () : (Q.t a) |B>> 
     B \[] (fun s => \[s = null]).
Proof. 
  intros. xcf_types a. xret*.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.

Lemma is_shallow_spec : forall a A,
  Spec is_shallow (s:Q.t a) |B>> 
     forall (R:htype A a) (T:tree A),
     keep B (s ~> Repr R T) (fun b => \[b = isTrue (tree_is_shallow T)]).
Proof.
(* faster
  xcf. intros s R T. destruct T.
  hunfold Repr. xextract. intros.
   xapps. xapps. xapps. hsimpl. subst. extens. rew_refl*.
  hunfold Repr. xchange (@_focus_middle s). xextract as pmid.
   xchange (@Repr_not_null pmid). xextract as N. xapps. xapps. xapps.
   hchange (@_unfocus_middle s). hsimpl. subst. 
   extens. rew_refl*.
*)
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec is_shallow) => Provide is_shallow_spec.

Lemma create_spec_repr : forall a A,
  Spec create () : (Q.t a) |B>> 
     forall (R:htype A a),
     keep B \[] (fun s => 
       (s ~> §(Repr R) (tree_shallow nil))).
Proof.
  xcf. intros. xapp. xapps. xapp. intros s. hunfoldb Repr. hsimpl.
Admitted. (* faster *)

Lemma create_spec : forall a A,
  Spec create () |B>> 
     forall (R:htype A a),
     B \[] (fun q => q ~> Seq R (@nil A)).
Proof.
  intros. xweaken create_spec_repr. introv LR HR. intros.
  xapply (rm HR). hsimpl.
  intros s. hunfold Seq. unblock. hsimpl. constructors*.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec create) => Provide create_spec.


Parameter pop_front_spec : forall a A, 
  Spec pop_front (q:Q.t a) |B>>  
     forall (R:htype A a) X L,
     B (q ~> Seq R (X::L)) (fun x => x ~> R X \* q ~> Seq R L).
(* see further *)

Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.

Parameter pop_back_spec : forall a A, 
  Spec pop_back (q:Q.t a) |B>>  
     forall (R:htype A a) X L,
     B (q ~> Seq R (L&X)) (fun x => x ~> R X \* q ~> Seq R L).
(* see further *)

Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.

Parameter push_front_spec : forall a A,
  Spec push_front (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \* $ push_cost)
       (# q ~> Seq R (X::L)).

Hint Extern 1 (RegisterSpec push_front) =>
  Provide push_front_spec.

Parameter push_back_spec : forall a A,
  Spec push_back (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \* $ push_cost)
       (# q ~> Seq R (L&X)).

Hint Extern 1 (RegisterSpec push_back) =>
  Provide push_back_spec.

Lemma is_empty_spec : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (R:htype A a), forall (L:list A),
     keep B (q ~> Seq R L) (\= isTrue (L = nil)).
Proof.
(* faster
  xcf. intros. hunfold Seq. xextract as T IT. xapps. 
  xpost (fun x => [x = (L '= nil)] \* s ~> Repr R T \* [tree_inv true T L]);
   (* todo: xposts *) [ | intros b; hsimpl~ ].
  lets IT2: IT. xif; inverts IT; tryfalse*.
  (* case shallow *)
  hunfold Repr. xextract as pc pfi pbi.
  xapps. xapps. hsimpl~.
  (* case deep *)
  xret. hsimpl~. extens. rew_reflect. iff H; tryfalse. 
  subst; rew_length in *. math.
*) skip.
Admitted. (* faster *)

Lemma is_empty_spec_repr : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (R:htype A a), forall (L:list A) T, tree_inv true T L ->
     keep B (q ~> Repr R T) (\= isTrue (L = nil)).
Admitted.

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma middle_merge_back_spec : forall a A,
  Spec middle_merge_back (m:loc) (c:Chunk.t a) |B>>  
     forall (R:htype A a) mid L LC, inv_middle L mid ->
     B (m ~> Seq (RChunk R) mid \* c ~> RChunk R LC \* $ merge_cost) 
       (# Hexists mid', m ~> Seq (RChunk R) mid' \* \[inv_middle (L++LC) mid']).
Proof.
(*
  xcf. unfold merge_cost. introv (ME&MC&MQ). xapps. xif.
  xchange (@ChunkSpecOwn.Seq_le_capacity _ c) as LCC. 
  asserts OC: (chunk_ok LC). lmath.
  xapps*. credits_split. xif.
  (* case empty middle *)
  xapp. hsimpl. rew_list. splits~.
  (* case non-empty middle *)
  destruct~ (get_last mid) as (LB&mid'&Emid). xapp. xapps. xif.
  (* subcase keep *)
  xapp. xapp. hsimpl. subst mid. splits~. applys~ ForallConseq_last.
  (* subcase merge *)
  xapp. xapp. hsimpl.
  subst mid. lets (?&?): Forall_last_inv MC.
  splits~. apply~ middle_conseq_push_back_merged. 
  (* case empty chunk *)
  asserts: (LC = nil). destruct LC. auto. lmath.
  xret. hsimpl. splits~.
*)
skip.
Admitted. (*faster*)

Hint Extern 1 (RegisterSpec middle_merge_back) =>
  Provide middle_merge_back_spec.

Axiom middle_merge_front_spec : forall a A,
  Spec middle_merge_front (m:loc) (c:Chunk.t a) |B>>  
     forall (R:htype A a) mid L LC, inv_middle L mid ->
     B (m ~> Seq (RChunk R) mid \* c ~> RChunk R LC \* $ merge_cost) 
       (# Hexists mid', m ~> Seq (RChunk R) mid' \* \[inv_middle (LC++L) mid']).

Hint Extern 1 (RegisterSpec middle_merge_front) =>
  Provide middle_merge_front_spec.

Lemma append_chunk_back_spec : forall a A,
  Spec append_chunk_back (c:Chunk.t a) (s:loc) |B>>  
     forall (R:htype A a) L LC,
     B (s ~> Seq R L \* c ~> RChunk R LC \* $(Chunk.capacity * push_cost))
       (# s ~> Seq R (L++LC)).
Proof.
 skip.
(* 
  xcf. intros. xwhile.   
  applys local_weaken_pre ((Hexists LC2 LC1, s ~> Seq R (L++LC1) \* c ~> RChunk R LC2 
    \* [LC = LC1 ++ LC2] \* $((LibList.length LC2) * push_cost))). xlocal.
  (* loop *)
  xextract as LC2. induction_wf IH: (@list_sub_wf A) LC2. intros LC1 EQ. applys (rm HR0).
  xapps. intro_subst. xret. xextract as M. xif; fold_bool; fold_prop.
    destruct LC2 as [|x LC2']; tryfalse. rew_length. rewrite R_succ_nat'.
     credits_split. xapps. xapps. xapply (>> IH LC2' (LC1&x)); eauto.
      hsimpl. rew_list~. hsimpl.
    xret. subst. rew_list. hsimpl.
  (* initialization *)
  hchange (ChunkSpecOwn.Seq_le_capacity c). hextract as LCC. (* todo: "hchange as" *) 
  hsimpl LC (@nil A); try solve [ rew_list~ ].
    apply R_le_mul_pos_r. credits_nonneg. 
    apply~ R_le_Z.
*)
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec append_chunk_back) =>
  Provide append_chunk_back_spec.

Axiom append_chunk_front_spec : forall a A,
  Spec append_chunk_front (c:Chunk.t a) (s:loc) |B>>  
     forall (R:htype A a) L LC,
     B (s ~> Seq R L \* c ~> RChunk R LC \* $(Chunk.capacity * push_cost)) 
       (# s ~> Seq R (LC++L)).

Hint Extern 1 (RegisterSpec append_chunk_front) =>
  Provide append_chunk_front_spec.

Lemma set_shallow_spec_repr : forall a A,
  Spec set_shallow (c : Chunk.t a) (s : Q.t a) |B>>   
     forall (R:htype A a) L pfo pfi pmid pbi (pbo:Chunk.t a),
     B (s ~> TU pfo pfi pmid pbi pbo \* c ~> RChunk R L) 
       (# s ~> Repr R (tree_shallow L)).
Proof.
  xcf. intros. xpost. xapps. xapps. xapps. xapps. xapps. xapps.
  hunfold Repr. hsimpl.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec set_shallow) =>  
  Provide set_shallow_spec_repr.

Lemma swap_shallow_with_other_spec : forall a A,
  Spec swap_shallow_with_other (s1:loc) (s2:loc) |B>>  
     forall (R:htype A a) L1 T2,
     B (s1 ~> §(Repr R) (tree_shallow L1) \* s2 ~> Repr R T2) 
       (# s2 ~> §(Repr R) (tree_shallow L1) \* s1 ~> Repr R T2).
Proof.
(*
  xcf. intros. unblock Repr. sets H': (s2 ~> Repr R (tree_shallow L1) \* s1 ~> Repr R T2).
  hunfold Repr at 1. xextract as pc1 pfi1 pbi1.
  destruct T2 as [TL2|]; hunfold Repr at 1.
  (* case T2 shallow *)
  xextract as pc2 pfi2 pbi2. xapps. xapps. xapps. xapps. xapps. xapps.
  xapps. xapps. xapps. xapps. xapps. xapps. unfold H'.
  hchange (@Repr_unfocus_shallow s1). hsimpl.
  (* case T2 deep *)
  xchange (@T_convert s2). xextract as pfo2 pfi2 pmid2 pbi2 pbo2.
  xapps. xapps. xapps. xapps. xapps. xapps.
  xapps. xapps. xapps. xapps. xapps. xapps.
  intros _. hchange (@T_unfocus s1). hchange (@Repr_unfocus_deep s1). 
  unfold H'. hsimpl.  
*)
skip.
Admitted. (*faster*)

Hint Extern 1 (RegisterSpec swap_shallow_with_other) =>
  Provide swap_shallow_with_other_spec.





Lemma append_spec : forall a A, 
  Spec append (q1:Q.t a) (q2:Q.t a) |B>>  
     forall (L1 L2 : list A) (R:htype A a),
     B (q1 ~> Seq R L1 \* q2 ~> Seq R L2 \*
        $ ( (3%nat) * merge_cost * (1 + min (max_depth_seq L1) (max_depth_seq L2)) 
           + (Chunk.capacity * push_cost) )%R )
      (# q1 ~> Seq R (L1 ++ L2)).
Proof.
  intros. xintros.
  cuts M:(forall n a A, forall (s1 s2 : t a) (L1 L2 : list A) (R : htype A a),
    n = floor_nat (max_depth_seq L1) ->
    app_2 append s1 s2 (s1 ~> Seq R L1 \* s2 ~> Seq R L2 \*
      $(3%nat * merge_cost * (1 + min (max_depth_seq L1) (max_depth_seq L2))
      + (Chunk.capacity * push_cost)))
    (# s1 ~> Seq R (L1 ++ L2))).
    intros. applys~ (rm M). 
  clears. intros n. induction_wf IH: lt_wf n.
  introv En. subst n. (* hide IH. *) xcf_app.
  hunfold Seq at 2. xextract as T2 Inv2. xapps. xif.

  (* case s2 shallow *)
  destruct T2; tryfalse. inverts Inv2.
  hunfold Repr. xextract as pc pfi pbi. xapps. xapp.
   do 2 simpl_ineq. credits_nonneg. skip. hsimpl.

  (* case s1 shallow *)
  hunfold Seq at 1. xextract as T1 Inv1. xapps. xif.
  destruct T1; tryfalse. inverts Inv1. xapp. unblock. auto. 
  unblock. hunfold Repr. xextract as pc pfi pbi. xapps.
  xchange* (@Seq_unfocus s1). xapp. simpl_ineq. skip. hsimpl.

  (* case both deep *)
  destruct T2 as [|fo2 fi2 tmid2 bi2 bo2]. false C; simple*. clear C.
  destruct T1 as [|fo1 fi1 tmid1 bi1 bo1]. false C0; simple*. clear C0.
  hunfold Repr. inverts Inv1 as. intros mid1 IM1 G1 Inv1.
  inverts Inv2 as. intros mid2 IM2 G2 Inv2.
  rewrite R_mul_add_one. 

  rewrite <- R_add_assoc.
  sets_eq cr: (3%nat * merge_cost * min (max_depth_seq L1) (max_depth_seq L2) + Chunk.capacity * push_cost)%R.
  do 2 rewrites R_succ_nat. rewrite R_mul_one_nat. 
  credits_split; try solve [ subst cr; credits_nonneg ]; [| skip ].

  xapp_by focus. xchange* (@Seq_unfocus m1). clears tmid1.
  xapp_by focus. xapp_by focus. xapps. 
  lets IB1: (inv_back_fill Inv1).
  xseq. xframes s1. xframes s2. xframe ($ merge_cost \* $ merge_cost \* $ cr 
    \* $ potential fo1 fi1 bi1 bo1 \* $ potential fo2 fi2 bi2 bo2).
  xpost (# Hexists mid1', 
   m1 ~> Seq (RChunk R) mid1' \* \[inv_middle (concat mid1 ++ bi1 ++ bo1) mid1']).
   xchange (ChunkSpecOwn.Seq_le_capacity co) as LCo. xif. 
     xapp*. hsimpl. rew_list~.
     unfold merge_cost. credits_split. xapp. xapps. xif.
       xapp. hsimpl. apply* inv_middle_push_back_full_and_nonempty. 
         lets: list_not_nil_length_pos C0. lmath. 
       xret. hsimpl. rew_list~. applys* inv_middle_push_back_full.
  xok. xok. xok. xextract as mid1' Imid1'. clears. clears IB1.

  xapp_by focus. xchange* (@Seq_unfocus m2). clears tmid2.
  xapp_by focus. xlet as fo. xapp_by focus. xapps. 
  lets IF2: (inv_front_fill Inv2).
  xseq. xframes s1. xframes m1. xframes s2. xframe ($ merge_cost \* $ cr
    \* $ potential fo1 fi1 bi1 bo1 \* $ potential fo2 fi2 bi2 bo2).
  xpost (# Hexists mid2', 
    m2 ~> Seq (RChunk R) mid2' \* \[inv_middle (fo2 ++ fi2 ++ concat mid2) mid2']).
    xchange (ChunkSpecOwn.Seq_le_capacity fo) as Lfo. xif. 
     xapp*. hsimpl. rew_list~.
     unfold merge_cost. credits_split. xapp. xapps. xif.
       xapp. hsimpl. apply* inv_middle_push_front_full_and_nonempty. 
         lets: list_not_nil_length_pos C0. lmath. 
       xret. hsimpl. rew_list~. applys* inv_middle_push_front_full.
  xok. xok. xok. xok. xextract as mid2' Imid2'. clears. clears IF2.

  xapp_by focus. xapp_by unfocus.
  xapp_by focus. xapp_by unfocus.
  xapps. xapps.

  forwards* D1: middle_max_depth' mid1' L1.
    rewrite (inv_items Inv1). rew_list. math.
  forwards* D2: middle_max_depth' mid2' L2.
    rewrite (inv_items Inv2). rew_list. math.

  xseq. xframes s1 s2. xframe ( $ cr \*
    $ potential fo1 fi1 bi1 bo1 \* $ potential fo2 fi2 bi2 bo2).
  xpost (# Hexists mid1f mid2f, 
    m1 ~> Seq (RChunk R) mid1f \* m2 ~> Seq (RChunk R) mid2f 
   \* \[inv_middle (concat (mid1' ++ mid2')) (mid1f ++ mid2f)
      /\ (LibList.length mid1f <= LibList.length mid1')
      /\ (LibList.length mid2f <= LibList.length mid2') ]). 
    lets (CI1&CK1&CO1): Imid1'. lets (CI2&CK2&CO2): Imid2'. xif as C.
      destruct C as [C1 C2].
      destruct~ (get_last mid1') as (B1&Mi1&E1); subst mid1'; clear C1.
      destruct mid2' as [|F2 Mi2]; tryfalse; clear C2. 
      xapp. xapps. xapp. xapps. clears. unfold merge_cost. credits_split. 
      xif.
        xapp. xapp. hsimpl. splits~. splits~. applys* ForallConseq_concat.
        xapp~. xapp. hsimpl. splits~. splits~.
          inverts CK2. forwards*: Forall_last_inv CK1.
          applys* ForallConseq_concat_fuse CO1 CO2.
      xret. hsimpl. splits~. splits~. destruct C; subst; rew_list~. 

  xok. xok. xextract as mid1f mid2f ((HI&HM&HC)&ML1&ML2). 

  lets ML1': max_depth_seq_incr (rm ML1). 
  lets ML2': max_depth_seq_incr (rm ML2).
  subst cr. xgc. xapply IH; try reflexivity.
    apply floor_nat_shift. credits_nonneg.
    rew_ineq ML1'. auto.
    hsimpl. simpl_ineq. apply R_le_mul_pos_l. credits_nonneg.
     rewrite <- min_sum_distrib. apply min_le_args.
     rew_ineq ML1'. auto. rew_ineq ML2'. auto.
  xok. intros _. hchange (@Seq_focus m1). hextract as Tm1 HTm1.
  hchange (@_unfocus_middle s1).
  credits_join. hchange (@Repr_unfocus_deep s1).
    unfold potential. rewrite R_add_assoc. simpl_ineq. 
     forwards In1: side_potential_nonneg. rew_ineq <- In1.
     forwards In2: side_potential_nonneg. rew_ineq <- In2. simpl_ineq. (* improve *)
  hunfold Seq. hsimpl. constructors*. constructors~.
    rewrite HI. rew_concat. rewrite (proj31 Imid1'). rewrite (proj31 Imid2').
     rewrite (inv_items Inv1). rewrite~ (inv_items Inv2).
    apply (inv_front_fill Inv1).
    apply (inv_back_fill Inv2).

Qed.






















Lemma push_front_spec' : forall a A,
  Spec push_front (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \*
        $ (ChunkSpec.op_cst + item_potential))
       (# q ~> Seq R (X::L)).
Proof.
  intros. xintros. 
  intros x q R X L. hunfold Seq at 1. xextract as T. 
  gen_eq n:(depth T). gen a A x q R X L. 
  induction_wf IH: lt_wf n. hide IH.
  intros a A T. introv EQn IT. xcf_app.
  xlet (fun b => \[ bool_of (full (tree_front T)) b] \* 
    (q ~> Repr R T \* x ~> R X \* $ (ChunkSpec.op_cst + item_potential))).
  inverts IT; hunfold Repr.
    xextract as pc pfi pbi. xapps. xapps. 
     xsimpl. boolofs*.
    xapp_by focus. xapp. intros b. hchange (@_unfocus_front_outer q). 
     hsimpl. boolofs*.
  intros Rb. xseq. 
  xframe (x ~> R X \* $ (ChunkSpec.op_cst + item_potential)).
  xpost (#Hexists (T':tree A), q ~> Repr R T' 
    \* \[~ full (tree_front T')] \* \[tree_inv true T' L]).
  xif; [ | xret; hsimpl~ ]. xapps. xif.
  (* case shallow *)
  destruct T; tryfalse. hunfold Repr. xextract as pc pfi pbi. simpls.
  xapps. xapps. xapps. xapps. xapp_spec create_spec_repr.
  xapps. xapps. xapps. intros r. 
  unblock Repr. hchange (@T_unfocus q). hchange (@Repr_unfocus_deep q).
    rewrite <- credits_zero. hsimpl. pot_simpl.
  clears. inverts IT as LT. hsimpl~.
    constructors~.
      constructors~. rew_list~.
      constructors~.
  (* case deep *)
  destruct T; tryfalse*. inverts IT as IT LL IN. (* todo: order of names *)
  hunfold Repr. xapp_by focus. xapp_by focus. xapp. xapps. xif.
  (* case deep, front inner empty *)
  xapp. intros _. hchange (@_unfocus_front_inner q) (@_unfocus_front_outer q).
  hchange (@Repr_unfocus_deep q). pot_simpl.
  hsimpl~. constructors*. destruct IT. constructors~.
  (* case deep, front inner not empty *)
  simpl in EQn. subst n.
  destruct IT as [MI MF MB MN MC]. destruct MF; tryfalse.
  forwards Po: potential_shift; 
   [ | | xchange (credits_ge Po); [ try apply R_ge_refl | ] ]; 
   auto~. clear Po.
  credits_split. xlet as pmid. xapp_by focus. simpl.
  xseq. xapply* (>> (rm IH) (depth T0) T0).
    math. credits_split. hsimpl.
  xapp. xapp. hchange (@_unfocus_front_inner q) (@_unfocus_front_outer q).
  hunfold Seq. hextract as T' IT'. hchange (@_unfocus_middle q).
  hchange (@Repr_unfocus_deep q). hsimpl~. constructors*. constructors~.
    applys* middle_conseq_push_front_full.
  (* push front *)
  xok. clears n IT. clears. xextract. intros T NF IT. credits_split.
  inverts IT as; hunfold Repr; simpl in NF.
    introv LL. xextract as pc pfi pbi. xapps. xapps. simpl_ineq.
     intros _. hchange (@Repr_unfocus_shallow q).
     hunfold Seq. xsimpl. constructors~. 
    introv IM IT LL. credits_split. 
     xapp_by focus. xframe ($ item_potential \* $ potential fo fi bi bo).
     xapp. simpl_ineq. hchange (@_unfocus_front_outer q).
     credits_join. hchange (@Repr_unfocus_deep q). pot_simpl.
     hunfold Seq. hsimpl. constructors*. destruct IT; constructors~.
Admitted. (* faster *)       


(*------------------------------------------------------*)
(* pop *)


Parameter pop_front_spec : forall a A, 
  Spec pop_front (q:Q.t a) |B>>  
     forall (R:htype A a) X L,
     B (q ~> Seq R (X::L)) (fun x => x ~> R X \* q ~> Seq R L).

Lemma set_shallow_if_needed_spec : forall a A,
  Spec set_shallow_if_needed (s:Q.t a) |B>> 
     forall (R:htype A a) (T:tree A) L, 
     ~ tree_is_shallow T -> tree_inv false T L ->
     B (s ~> Repr R T) (# s ~> Seq R L).
Proof.
  xcf. intros s R T L NS IT. 
  destruct T as [|fo fi mid bi bo]; tryfalse*. clear NS.
  hunfold Repr. xchange (@T_focus s). xextract as pfo pfi pmid pbi pbo.
  inverts IT as. intros midL _ ITmid Inv.
  xapps. xapps. xapps. xapps. xapps. xapps. xapps. xapps. xapps.
  xlet. xret. xextract as EQn.
  xapp_spec* (@is_empty_spec_base (chunk a) (list A)). intro_subst. 
   (* todo: xapps_spec* *)
  xif as C. 
  (* case n <= 1 /\ mid = nil *)
  destruct Inv as [MI MF MB MN MC].
  destruct MF as [?|F]; [ | rewrite F in EQn; false; skip ].
  destruct MB as [?|F]; [ | rewrite F in EQn; false; skip ].
  subst bi fi. destruct C. subst midL. rew_list in MI.
  xapps. xlet.
    xframe - \[]. xpost (fun c =>
    \[ (c = pbo /\ L = bo) \/ (c = pfo /\ L = fo)]).
    xif; xrets.
      left. split~. 
      right. split~. cuts: (bo = nil). subst. rew_list~.
       destruct~ bo. destruct fo; tryfalse.
       rew_length in EQn. skip.
    xok.
  xextract as [[Ec EL]|[Ec EL]]; subst c L.
    xapps. hsimpl.
    xapps. hsimpl.
  (* case n = 0 *)
  set_eq n': n in EQn. xif as C'. (* todo: prevent subst ? *)
  destruct C. false. skip. (* math *) 
  destruct midL as [|LF midL']. false.
  xchange (@Seq_unfocus pmid). eauto. (* xchange* missing *)
  xapp_spec pop_front_spec. xapps. xapps.
  hunfold Seq at 1. xextract as Tmid' ITmid'. clear ITmid. 
  subst n'. destruct fo; [ | rew_list in C'; false; skip ].
  destruct fi; [ | rew_list in C'; false; skip ].
  destruct bi; [ | rew_list in C'; false; skip ].
  destruct bo; [ | rew_list in C'; false; skip ].
  lets LEQ: inv_items Inv.
  xif as C''.
    destruct C''. subst midL'. xapp. hsimpl. rew_list~ in LEQ.
    xapp. intros _. hchange (@T_unfocus s) (@Repr_unfocus_deep s).
     pot_simpl. hunfolds Seq. constructors*.
      destruct Inv as [MI MF MB MN MC]. constructors*. inverts~ MN.
      subst L. rew_app. rew_concat. rew_length. destruct C''.
        skip.
        lets IM: inv_mid_chunks Inv. (* use LF and midL' wf *)
         inverts IM. (* factorize *) skip.
  (* case other *)
  xret. hunfold Seq. hchange (@T_unfocus s).
  hchange (@Repr_unfocus_deep s). hsimpl.
   destruct Inv as [MI MF MB MN MC]. constructors*.
    skip.
    subst L. rew_length. destruct C.
      skip. (* length >= n > 1. *)
      asserts: (len (concat midL) > 0). 
        destruct midL; tryfalse. rew_list. inverts MN as LM _.
        unfolds in LM. skip.
       asserts: (n > 0). skip.
       skip. (* length >= n + |concat mid| >= 1 + 1 > 1 *)

Admitted.



--- Lemma pop_front_spec : forall A,
  Spec pop_front (q:Q.t A) |B>>  
     forall (X:A) L,
     B (q ~> Seq (X::L)) (fun r => \[r = X] \* q ~> Seq L).
Proof.
  Hint Resolve build_inv.
  xcf. intros. hunfold Seq at 1. xextract as fo fi mid bi bo Inv.
  xapp_by focus. xapps. destruct Inv as [MI MF MB MN MC]. xif. 
  (* case co *)
  destruct fo as [|Y fo2]; tryfalse. rew_list in MI. inverts MI. 
  xapp. hchange (@_unfocus_front_outer s). hunfolds~ Seq. 
  (* case ci *)
  xapp_by focus. xapp_by focus. xapps. xif.
  xchange (@_unfocus_middle s).
  destruct fi as \[|Y fi2]; tryfalse. rew_list in MI. inverts MI. 
  xapp. xapp_by unfocus. xapps. xapp.
  hchange (@_unfocus_front_outer s). hunfolds~ Seq.
  (* case mid *) 
  xapps. xif. 
  xchange (@_unfocus_front_inner s).
  destruct mid as [|LC mid2]; tryfalse.
  inverts MN. destruct LC as [|Y LC2]; [ rew_length in *; math | ].
  rew_list in MI. inverts MI. xapp. xapp. xapps. xapp.
  hchange (@_unfocus_middle s) (@_unfocus_front_outer s).
  hunfolds* Seq.
  (* case bo *)
  xchange (@_unfocus_front_inner s). xchange (@_unfocus_middle s).
  xapp_by focus. xapps. xif.
  destruct bi as [|Y bi2]; tryfalse. rew_list in MI. inverts MI. 
  xapps. xapp. xapp_by unfocus. xapps. xapp.
  hchange (@_unfocus_front_outer s). hunfold Seq. hsimpl~.
  (* case bi *)
  xchange (@_unfocus_back_inner s). xchange (@_unfocus_front_outer s).
  xapp_by focus. xapp. hchange (@_unfocus_back_outer s).
  hunfold Seq. hsimpl~.
Admitted. (* faster *)


Lemma convert_deep_to_shallow_if_needed_spec : forall A,
  Spec convert_deep_to_shallow_if_needed (q:Q.t A) |B>>  
     forall T L, tree_inv false T L ->
     B (q ~> Repr T) (# q ~> Seq L).
Proof.
Admitted. (* faster *)




(* not used
Lemma is_empty_spec' : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (R:htype A a), forall (L:list A),
     keep B (q ~> Seq R L) (\= isTrue (L = nil)).
Proof.
  (* todo: 
    intros. xweaken create_spec_repr. introv LR HR. intros.
     xapply (rm HR). hsimpl.
     intros s. hunfold Seq. unblock. hsimpl. constructors*.
  *)
Admitted. (* faster *)
*)



(*
Axiom append_spec' : forall a A, 
  Spec append (q1:Q.t a) (q2:Q.t a) |B>>  
     forall (R:htype A a) (L1' L2' : list A),
     B (q1 ~> Seq R L1' \* q2 ~> Seq R L2' \*
        $ ( (3%nat) * merge_cost * (1 + min (max_depth_seq L1') (max_depth_seq L2')) )%R )
      (# q1 ~> Seq R (L1' ++ L2')).
  xapp_spec append_spec'. 
*) 



