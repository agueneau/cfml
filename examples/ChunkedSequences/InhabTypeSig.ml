module type InhabType =
  sig
    type t
    val inhab : t
  end
