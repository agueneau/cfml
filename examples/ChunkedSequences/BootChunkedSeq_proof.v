(* todo: hextract; intros; subst => replace *)
(* todo: remove xclean where not needed *)
(* todo: caseIf and caseif *)
(* todo: do not hide cleared hyp *)
(* todo: boolof sur is_empty => r�parer / tester les boolofs *)
(* - rename mid into m*)
(* todo: append_chunk_back_spec swap args?*)
(* todo: clears should not hide the body of definitions in context *)
(*todo: tactic for creating zero potential in hsimpl *)
(* todo: t_of_not_null should be automatically generate *)
(* todo: tactic   [clears *] *)
(* todo : factorize end of append using push_or_merge_chunk_back *)
(* todo: fix ordering  of functor arguments *)
(* later: spec de pop_back lorsque item en focus ==> repasser par repr *)
(* todo: use xframes 3 *)
(* todo: list lemma get_head *)
(* todo: xif with xseq and xpost *) 
(* todo: avoid xframe on credits *)
(* todo: xframe on a heap predicate which is a definition *)
(* todo: deprecate hdata_simpl by xunfold *)
(* todo: replace hchange=>xchange, hsimpl=>xsimpl *)

Fixpoint depth A (T:tree A) : int :=
  match T with
  | tree_shallow _ => 0%Z
  | tree_deep _ _ tmid _ _ => (1 + (depth tmid))%Z
  end.



(*------------------------------------------------------*)
(** demo swap let *)


Axiom local_wgframe' : forall H2 H' B (F:~~B) H H1 Q1 Q,
  is_local F -> 
  F H1 Q1 -> 
  H ==> H1 \* H2 -> 
  Q1 \*+ H2 ===> Q \*+ H' ->
  F H Q.


Lemma test : forall A1 A2 A3 (F1:~~A1) (F2:A1->~~A2) (F3:A2->~~A3) H Q,
  is_local F1 ->
  (forall x, is_local (F2 x)) ->
  (forall y, is_local (F3 y)) ->
  (Let y := (Let x := F1 in F2 x) in (F3 y)) H Q ->
  (Let x := F1 in (Let y := F2 x in (F3 y))) H Q.
Proof.
  introv L1 L2 L3 M. hnf. introv Hh. specializes M Hh.
  destruct M as (H1&H2&Q1&H'&E1&(Q2&E2&E3)&E4).
  destruct E1 as (h1&h2&Hh1&Hh2&HD&HP).
  exists (= h1) (= h2) Q1 H'. splits~.
    exists h1 h2. splits~. Focus 1.
    specializes E2 Hh1.
     destruct E2 as (H1'&H2'&Q1'&H''&E1'&(Q2'&E2'&E3')&E4').
     destruct E1' as (h1'&h2'&Hh1'&Hh2'&HD'&HP').
     exists (Q2' \*+ (= h2')). split.
       applys* local_wgframe' (=h2').
         intro k. intro_subst. subst h1. hnf. exists h1' h2'. splits*.
         intro r. xok.
       intros x. specializes E3' x.
        intros h' Hh'. exists (Q2' x \* H2') \[] (Q1 \*+ H'').
         esplit. splits~.
          rewrite star_neutral_r.
           gen Hh'. gen h'. 
            cuts~: ((Q2' x \* (= h2')) ==> (Q2' x \* H2')).
             hsimpl. intro k. intro_subst~.
          exists (Q1' \*+ H2'). split~.
          applys* local_wframe.
          intros y. specializes E4' y. xchange~ E4'.
          rewrite star_neutral_r. specializes E3 y. 
          applys* local_wframe.
          intros r. hsimpl.  
    applys trans_elim E4. skip. hsimpl.
     intros k. intro_subst. auto.
Qed.


Notation "'Let' x ':=' F1 'in' F2" :=
  (!T (fun H Q => exists Q1, F1 H Q1 /\ forall x, F2 (Q1 x) Q))
  (at level 69, a at level 0, x ident, right associativity,
  format "'[v' '[' 'Let'  x  ':='  F1  'in' ']'  '/'  '[' F2 ']' ']'") : charac.

Notation "'Let' x ':' T ':=' F1 'in' F2" :=
  (!T (fun H Q => exists Q1, F1 H Q1 /\ forall x:T, F2 (Q1 x) Q))
  (at level 69, a at level 0, x ident, right associativity, only parsing) : charac.




capa > 0



(*
Lemma capa_pos : capa > 0.
Proof. apply ChunkSpec.capacity_spec. Qed.
*)

eapply xframe_lemma with (H1 := Pot). xlocal. unfold Pot. hsimpl. (* todo *)




Ltac hsimpl_wand_base tt ::=  
  let H1 := find_wand tt in
  (* hchange (heap_wand_elim H1); [ hsimpl | ].*)
  hchange_core (heap_wand_elim H1) __ ltac:(idcont) ltac:(idcont);  [ hcancel_cont tt | ].


Lemma middle_merge_back_spec : forall A,
  Spec middle_merge_back (m:Middle.t (Chunk.t A)) (c:Chunk.t A) |B>>  
     forall mid L LC, inv_middle L mid -> 
     B (m ~> RMiddle mid \* c ~> RChunk LC 
       \* $(MiddleSpecOwn.op_cst + MiddleSpecOwn.op_cst + ChunkSpec.append_cst))  (* todo: by 2 *)
       (# Hexists mid', m ~> RMiddle mid' \* \[inv_middle (L++LC) mid']).
Proof.
  xcf. introv (ME&MC&MQ). xapps. xif.
  xchange (ChunkSpec.Seq_le_capacity c) as LCC. 
  asserts OC: (chunk_ok LC). lmath.
  xapps. credits_split. xif.
  (* case empty middle *)
  xapp. hsimpl. rew_list. splits~.
  (* case non-empty middle *)
  destruct~ (get_last mid) as (LB&mid'&Emid). xapp. xapps. 
  rewrite capacity_cf. xif. 
  (* subcase keep *)
  xapp. hsimpl_wand. hsimpl. hsimpl. subst mid. splits~.
    applys~ ForallConseq_last. lmath.
  (* subcase merge *)
  xapp. hsimpl_wand; hsimpl. xapp. xapp. hsimpl. 
  subst mid. lets (?&?): Forall_last_inv MC. splits~.
    apply~ Forall_last. lmath.
    apply~ middle_conseq_push_back_merged. lmath.
  (* case empty chunk *)
  asserts: (LC = nil). destruct LC. auto. lmath.
  xret. hsimpl. splits~. (* todo: factorize *)
Admitted. (*faster*)

Hint Extern 1 (RegisterSpec middle_merge_back) =>
  Provide middle_merge_back_spec.





Parameter front_spec : forall A, 
  Spec front (q:Q.t A) |B>>  
     forall X L,
     B (q ~> Seq (X::L) \* $ op_cst) (fun r => \[r = X] \* q ~> Seq (X::L)).

Parameter back_spec : forall A, 
  Spec back (q:Q.t A) |B>>  
     forall X L,
     B (q ~> Seq (L&X) \* $ op_cst) (fun r => \[r = X] \* q ~> Seq (L&X)).


Hint Extern 1 (RegisterSpec front) => Provide front_spec.
Hint Extern 1 (RegisterSpec back) => Provide back_spec.

Hint Extern 1 (RegisterSpec iter) => Provide iter_spec.

(* LATER:
Parameter carve_back_at_spec : forall A, 
  Spec carve_back_at (i:int) (q:Q.t A) |B>>  
     forall L, 0 <= i <= my_Z_of_nat (LibList.length L) ->
     B (q ~> Seq L) (fun q2 => Hexists L1 L2,
       \[L = L1 ++ L2] \* \[LibList.length L1 = i :> int]
       \* q ~> Seq L1 \* q2 ~> Seq L2).

Hint Extern 1 (RegisterSpec carve_back_at) => Provide carve_back_at_spec.
*)


Parameter iter_spec : forall A,
  Spec iter (f:func) (q:Q.t A) |B>>  
    forall L (I:list A->hprop),
     (forall X K, Mem X L -> App f X; (I K) (# I (K&X))) -> (* todo: strengthen... *)
     B (q ~> Seq L \* I nil) (# q ~> Seq L \* I L).


Parameter front_spec : forall a A,
  Spec front (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (X::L) \* $ op_cst) 
       (fun x => x ~> R X \*==>* q ~> Seq R (X::L)).

Parameter back_spec : forall a A,
  Spec back (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (L&X) \* $ op_cst) 
       (fun x => x ~> R X \*==>* q ~> Seq R (L&X)).

Hint Extern 1 (RegisterSpec front) => Provide front_spec.
Hint Extern 1 (RegisterSpec back) => Provide back_spec.

(* LATER:
  Parameter carve_back_at_spec : forall A, 
    Spec carve_back_at (i:int) (q:Q.t A) |B>>  
       forall L, 0 <= i <= my_Z_of_nat (LibList.length L) ->
       B (q ~> Seq L) (fun q2 => Hexists L1 L2,
         \[L = L1 ++ L2] \* \[LibList.length L1 = i :> int]
         \* q ~> Seq L1 \* q2 ~> Seq L2).
  Hint Extern 1 (RegisterSpec carve_back_at) => Provide carve_back_at_spec.
*)



Parameter front_spec : forall a A,
  Spec front (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (X::L) \* $ op_cst) 
       (fun x => x ~> R X \*==>* q ~> Seq R (X::L)).

Parameter back_spec : forall a A,
  Spec back (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (L&X) \* $ op_cst) 
       (fun x => x ~> R X \*==>* q ~> Seq R (L&X)).
Hint Extern 1 (RegisterSpec front) => Provide front_spec.


Hint Extern 1 (RegisterSpec back) => Provide back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.





(**************************************************)
(** demo iter *)




Lemma iter_spec' : forall a A,
  Spec Chunk.iter (f:func) (q:Chunk.t a) |B>>  
    forall (I:list A->hprop) L (R:htype A a),
     (forall x X K, Mem X L -> App f x; (I K \* x ~> R X) (# I (K&X))) ->
     B (q ~> CSeqOf R L \* I nil) (# Hexists M, q ~> ChunkSpec.Seq M \* I L).
Proof.
  intros. xweaken ChunkSpec.iter_spec. simpl. intros f q B LB HB. introv Hf.
  hunfold CSeqOf. xextract as M EM.
  xapply (>> (rm HB) M (fun k => Hexists (K:list A) (S:list A) (s:list a), 
    \[M = k ++ s /\ L = K ++ S /\ LibList.length S = LibList.length S] \*
    I K \* reduce sep_monoid (fun p : a * A => let '(x, X) := p in x ~> R X)
     (combine s K))).
  introv MX. xextract as K' S' s' (M1&M2&M3). xapply Hf.
    skip. (* mem *)
skip.
 hsimpl. skip. (* todo *)
Admitted.







(** Push a buffer to the back of the middle sequence, 
    possibly merging it with the back chunk in the middle sequence *)

let middle_merge_back m c =
   let sc = Chunk.length c in
   if sc > 0 then begin
      if Middle.is_empty m then begin
         Middle.push_back c m
      end else begin
         let b = Middle.back m in
         let sb = Chunk.length b in
         if sc + sb > Chunk.capacity then begin
            Middle.push_back c m
         end else begin
            let b = Middle.pop_back m in 
            Chunk.append b c;
            Middle.push_back b m;
         end
      end
   end

(** -- Symmetric to above *)

let middle_merge_front m c =
   assert false

(** Append to the back of s1 the items of s2; s2 becomes invalid *)

let append s1 s2 =
   let m1 = s1.middle in
   let ci = s1.back_inner in
   let co = s1.back_outer in
   if Chunk.is_empty ci then begin
      middle_merge_back m1 co
   end else begin
      Middle.push_back ci m1;
      if not (Chunk.is_empty co)
         then Middle.push_back co m1;
   end;
   let m2 = s2.middle in
   let fi = s2.front_inner in
   let fo = s2.front_outer in
   if Chunk.is_empty fi then begin
      middle_merge_front m2 fo
   end else begin
      Middle.push_front fi m2;
      if not (Chunk.is_empty fo)
         then Middle.push_front fo m2;
   end;
   s1.back_inner <- s2.back_inner;
   s1.back_outer <- s2.back_outer;
   if   not (Middle.is_empty m1)
     && not (Middle.is_empty m2) then begin
      let c1 = Middle.back m1 in
      let sc1 = Chunk.length c1 in
      let c2 = Middle.front m2 in
      let sc2 = Chunk.length c2 in
      if sc1 + sc2 <= Chunk.capacity then begin
         let c1 = Middle.pop_back m1 in
         let c2 = Middle.pop_front m2 in
         Chunk.append c1 c2;
         Middle.push_back c1 m1;
      end
   end;
   Middle.append m1 m2

end










Lemma is_shallow_spec' : forall A,
  Spec is_shallow (s:Q.t A) |B>> 
     forall (T:tree A),
     keep B (s ~> Repr T) (fun b => \[b = isTrue (tree_is_shallow T)]).
Proof.
  xcf. intros s T. xchange (@Repr_focus_mid s) as.
  intros H pfo pfi pmid pbi pbo. xextract as NU HU. 
  xapps. xapps. xapps. hchange HU. subst. hsimpl.
  extens. rew_refl*.
Admitted.




(*
Lemma Seq_focus : forall (s : loc) a A (R:htype A a) (L : list A),  
  s ~> Seq R L ==> Hexists pfo pfi pmid pbi pbo,
  s ~> TU pfo pfi pmid pbi pbo \*
  If pmid = null then 
        \[length L <= capa /\ pfo = pbo] 
     \* pfo ~> RChunk R L
  else

  | tree_deep fo fi tmid bi bo => 
        \[inv L fo fi mid bi bo /\ length L > 1 /\ ]
     \* pfo ~> RChunk R fo 
     \* pfi ~> RChunk R fi
     \* pmid ~> Seq R tmid
     \* pbi ~> RChunk R bi
     \* pbo ~> RChunk R bo
     \* $ (potential fo fi bi bo)
  end.
Proof.
  intros. destruct T0.
  hunfold Repr. hsimpl~.
  hunfold Repr at 1. rewrite T_convert. hsimpl.
Admitted. (*faster*)


  | tree_inv_deep : forall A (fo fi bi bo : list A) tmid mid L, 
      
      (if fixed then  else True) ->
      tree_inv true tmid mid ->
      tree_inv fixed (tree_deep fo fi tmid bi bo) L.


*)


(* not used

Axiom hexists_give_hd : forall A (x:A) H (J:A->hprop),
  H ==> J x -> H ==> heap_is_pack J.


Lemma Repr_focus_mid : forall (s : loc) (a A : Type) (R:htype A a) (T : tree A),  
  s ~> Repr R T ==> 
   Hexists (H:Chunk.t a->Chunk.t a->(Q.t (Chunk.t a))->Chunk.t a->Chunk.t a->hprop), 
   Hexists pfo pfi pmid pbi pbo, 
   let Hs := s ~> TU pfo pfi pmid pbi pbo in
   let Hr := H pfo pfi pmid pbi pbo in
     \[(pmid = null) <-> (tree_is_shallow T)] 
     \* Hs \* Hr \* \[Hr \* Hs ==> s ~> Repr R T].
Proof.
  Hint Unfold tree_is_shallow. 
  intros. 
  applys hexists_give_hd (fun pfo pfi pmid pbi pbo => match T0 with
  | tree_shallow L => 
     \[pfo = pbo /\ pmid = null] \* pfo ~> RChunk R L
  | tree_deep fo fi mid bi bo => 
        pfo ~> RChunk R fo 
     \* pfi ~> RChunk R fi
     \* pmid ~> RMiddle R mid
     \* pbi ~> RChunk R bi
     \* pbo ~> RChunk R bo
     \* $ (potential fo fi bi bo)
  end). destruct T0.
  hunfold Repr. hsimpl~. hsimpl. iff*.
  hunfold Repr at 1. rewrite T_convert.
  hextract. hchange (@Repr_not_null x1). hsimpl~.
   hunfold Repr at 2. hextract. hchange (@T_unfocus s). hsimpl.
   iff; tryfalse.
Admitted.
*)