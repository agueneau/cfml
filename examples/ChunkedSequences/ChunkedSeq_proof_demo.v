Set Implicit Arguments.
Require Export Rbase Rfunctions.
Require Import CFLib LibSet LibMap LibArray LibInt.
Require Import Common_proof.
Require Import SequenceSig_ml SequenceSig_proof.
Require Import ChunkedSeq_ml.

Open Scope R_scope.
Open Scope Z_scope.
Open Scope list_scope.
Require Import LibOrder.

(*------------------------------------------------------*)
(** instantiations *)

Module MakeSpec
   (Chunk : SequenceSig_ml.MLFixedCapacityS)
   (ChunkSpec : SequenceSigFixedCapacitySpec with Module Q:=Chunk)
   (Middle : SequenceSig_ml.MLS) 
   (MiddleSpec : SequenceSigSpec with Module Q:=Middle).


Module Import Q := MLMake Middle Chunk.

Module MiddleSpecOwn := SequenceSigOwnSpec_from_SigSpec (Middle)(MiddleSpec).

(*------------------------------------------------------*)
(** representation predicates *)

Notation "'RChunk'" := (@ChunkSpec.Seq _).

Notation "'RMiddle'" := (MiddleSpecOwn.Seq RChunk).

Notation "'RMiddleOf' R" := (MiddleSpecOwn.Seq R) (at level 10).

Notation "'Tof'" := (Q.T).

Notation "'TofAll' fo fi mid bi bo" :=
  (Q.T RChunk RChunk RMiddle RChunk RChunk fo fi mid bi bo)
  (fo at level 0, fi at level 0, mid at level 0, bi at level 0, bo at level 0, at level 30).

Notation "'TU_'" := (T Id Id Id Id Id).

Notation "'length'" := (fun L => my_Z_of_nat (LibList.length L)).

(*------------------------------------------------------*)
(** invariants *)

Definition capa := Chunk.capacity.

Definition full (A:Type) (L:list A) :=
  length L = capa. 

Definition chunk_ok {A:Type} (L:list A) :=
  0 < length L <= capa.

Definition middle_chunks_ok (A:Type) (mid:list(list A)) :=
  Forall chunk_ok mid.

Record inv (A : Type) (L fo fi : list A) mid bi bo := build_inv {
  inv_items : L = fo ++ fi ++ concat mid ++ bi ++ bo;
  inv_front_fill : fi = nil \/ full fi;
  inv_back_fill : bi = nil \/ full bi;
  inv_middle_chunks : middle_chunks_ok mid }.

Definition Seq (A : Type) (L : list A) (s : loc) :=
  Hexists fo fi mid bi bo,
     s ~> (Tof RChunk RChunk (RMiddleOf RChunk) RChunk RChunk
           fo fi mid bi bo)
  \* \[inv L fo fi mid bi bo].


(*------------------------------------------------------*)
(** tactics *)

Ltac lmath :=
  hint ChunkSpec.capacity_spec;
  unfolds full, chunk_ok, capa; try rew_length in *; math.

Hint Unfold middle_chunks_ok.
Hint Constructors Forall ForallConseq.
Hint Resolve Forall_last Forall_app.
Hint Extern 10 (_ = _ :> list _) => subst; rew_list.
Hint Extern 1 ((_ > _)) => lmath.
Hint Extern 1 ((_ >= _)) => lmath.
Hint Extern 1 ((_ <= _)) => lmath.
Hint Extern 1 ((_ > _)%Z) => lmath.
Hint Extern 1 ((_ >= _)%Z) => lmath.
Hint Extern 1 ((_ <= _)%Z) => lmath.
Hint Extern 1 (chunk_ok _) => lmath.


(*------------------------------------------------------*)
(** lemmas *)

Lemma not_full_nil : forall A,
  ~ full (@nil A).
Proof. introv H. unfolds in H. rew_list in *. lmath. Qed.

Hint Resolve not_full_nil.


(*------------------------------------------------------*)
(** FOR THE DEMO: specifications without time credits *)

Parameter chunk_create_spec : forall A,
  Spec Chunk.create () |B>> 
     B \[] (fun q => q ~> ChunkSpec.Seq (@nil A)).

Parameter chunk_push_front_spec : forall A,
  Spec Chunk.push_front (X:A) (q:Chunk.t A) |B>>  
     forall L,
     B (q ~> ChunkSpec.Seq L) (# q ~> ChunkSpec.Seq (X::L)).

Parameter middle_push_front_spec : forall a A,
  Spec Middle.push_front (x:a) (q:Middle.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> MiddleSpecOwn.Seq R L \* x ~> R X) (# q ~> MiddleSpecOwn.Seq R (X::L)).

Hint Extern 1 (RegisterSpec Chunk.create) => Provide chunk_create_spec.
Hint Extern 1 (RegisterSpec Chunk.push_front) => Provide chunk_push_front_spec.
Hint Extern 1 (RegisterSpec Middle.push_front) => Provide middle_push_front_spec.


(*------------------------------------------------------*)
(** verification *)

Theorem push_front_spec : forall A X (L:list A) (q:Q.t A),
  (App push_front X q;) (q ~> Seq L) (# q ~> Seq (X::L)).
Proof.
  intros. xcf_app. intros. rename q into s. hunfold Seq at 1.
  xextract as fo fi mid bi bo Inv. xapp_by focus. xapps. xseq.
  xif (# Hexists (fo2 fi2 : list A), Hexists mid2,
       s ~> TofAll fo2 fi2 mid2 bi bo \* \[inv L fo2 fi2 mid2 bi bo]
    \* \[~ full fo2]).
  (* case full *) 
  xapp_by focus. xapp_by unfocus. xapps. xif.
  (* case inner empty *)
  xapp_by unfocus. hsimpl~. destruct Inv. constructor~.
  (* case inner full *)
  Focus 1. xapp_by focus. destruct Inv as [MI MF MB MN MC].
  destruct MF; tryfalse. xapp~. xapp. xapp_by unfocus.
  hchange (@_unfocus_middle s). hsimpl~. constructor~.
  (* case not full *)
  xret. hchange (@_unfocus_front_outer s). hsimpl~. 
  (* push front *)
  clears Inv. clears. xextract as fo fi mid Inv Nfo.
  xapp_by focus. xapp. hchange (@_unfocus_front_outer s).
  hunfold Seq. hsimpl. destruct Inv. constructor~. 
Qed.

(* Code associated with case "inner full":

         Middle.push_front ci s.middle;
         s.front_outer <- Chunk.create();

   Normalized as:

         let x12 = s.middle in
         Middle.push_front ci x12;
         let x15 = Chunk.create() in
         s.front_outer <- x15;

*)





End MakeSpec.