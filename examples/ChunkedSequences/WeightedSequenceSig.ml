open Weighted (* todo: "require" rather than "open" *)

module type S = 
sig
   type 'a witem = 'a Weighted.t 
   type weight_type = Weighted.weight_type
   type 'a t   (* a sequence of objects, each of type ['a Weighted.t] *)
   val create : unit -> 'a t
   val is_empty : 'a t -> bool
   val weight : 'a t -> weight_type
   val back : 'a t -> 'a witem
   val front : 'a t -> 'a witem
   val push_front :'a witem -> 'a t -> unit
   val pop_front : 'a t -> 'a witem
   val push_back : 'a witem -> 'a t -> unit
   val pop_back : 'a t -> 'a witem
   val append : 'a t -> 'a t -> unit
   val carve_back_at : weight_type -> 'a t -> 'a witem * 'a t
   val iter : ('a witem -> unit) -> 'a t -> unit
   (* 
   val fold_left : ('a -> witem -> 'a) -> 'a -> t -> 'a
   val fold_right : (witem -> 'a -> 'a) -> t -> 'a -> 'a
   val to_list : t -> witem list 
   *)
end



(*---------------------------------------------------------------*)
(** functor version *)


module type SItem = 
sig
   type item
   type weight_type = Weighted.weight_type
   type witem = item Weighted.t 
   type t
   val create : unit -> t
   val is_empty : t -> bool
   val weight : t -> weight_type
   val back : t -> witem
   val front : t -> witem
   val push_front : witem -> t -> unit
   val pop_front : t -> witem
   val push_back : witem -> t -> unit
   val pop_back : t -> witem
   val append : t -> t -> unit
   val carve_back_at : weight_type -> t -> witem * t
   val iter : (witem -> unit) -> t -> unit
   (* 
   val fold_left : ('a -> witem -> 'a) -> 'a -> t -> 'a
   val fold_right : (witem -> 'a -> 'a) -> t -> 'a -> 'a
   val to_list : t -> witem list 
   *)
end



