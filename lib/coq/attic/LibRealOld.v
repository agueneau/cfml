Set Implicit Arguments.
Require Import LibTactics LibInt.
Require Export Rbase Rfunctions.
Open Scope comp_scope. (* for inequalities *)
Open Scope R_scope.


Implicit Types a b c x y z : R.
Implicit Types n m : nat.
Implicit Types p q r : Z.


(********************************************************************)
(** LibTactics: extend [nat_from_number]  *)

Ltac ltac_nat_from_R x :=
  match x with
  | 0%R => constr:(0%nat)
  | 1%R => constr:(1%nat)
  | 2%R => constr:(2%nat)
  | 3%R => constr:(3%nat)
  | 4%R => constr:(4%nat)
  | 5%R => constr:(5%nat)
  | 6%R => constr:(6%nat)
  | _ => fail 100 "ltac_nat_from_R only supports numbers up to 6"
  end.

Ltac nat_from_number N ::=
  match type of N with
  | nat => constr:(N)
  | BinInt.Z => let N' := constr:(ltac_nat_from_int N) in eval compute in N'
  | R => ltac_nat_from_R N
  end.


(********************************************************************)
(** Coercions *)

Coercion IZR : Z >-> R.


(********************************************************************)
(** Algebra *)

Axiom R_add_zero_l : forall a,
  0 + a = a.

Axiom R_add_zero_r : forall a,
  a + 0 = a.

Axiom R_add_comm : forall a b,
  a + b = b + a.

Axiom R_add_assoc : forall a b c,
  a + (b+c) = a + b + c.

Axiom R_mul_zero : forall a,
  0*a = 0.

Axiom R_mul_one : forall a,
  1*a = a.

Axiom R_mul_assoc : forall a b c,
  a * (b*c) = a * b * c.

Axiom R_distrib_add_mul : forall a b c,
  (a+b)*c = a*c + b*c.

Axiom R_distrib_add_div : forall a b c,
  (a+b) / c = (a / c) + (b / c).

Axiom R_mul_div_assoc : forall a b c,
  a * (b / c) = (a * b) / c.

Axiom R_div_self : forall a,
  a / a = 1.

Axiom R_sub_zero : forall a,
  a - 0 = a.

Axiom R_distrib_sub_add : forall a b c,
  a - (b+c) = a - b - c.

Axiom R_distrib_sub_mul : forall a b c,
  (a-b)*c = a*c - b*c.

(* derived *)

Axiom R_mult_div_same : forall a b,
  a*(b/a) = b.

Axiom R_minus_one_plus_one : forall a,
  (a = ((a-1)+1))%R. 

Axiom R_mul_add_one : forall a b,
  (a * (1 + b) = a + a*b)%R.

Axiom R_two_halves_val : forall a b, (2*(a/2 * b) = a * b)%R.


(********************************************************************)
(** Inequality *)


Axiom R_le_ge : forall a b,
  b <= a ->
  a >= b.

Axiom R_two_ge_zero : (2 >= 0)%R. (* todo: flip *)

Axiom R_zero_le_one : (0 <= 1)%R.

Axiom R_le_refl : forall a,
  a <= a.

Axiom R_ge_refl : forall a,
  a >= a.

Axiom R_le_add_l : forall a b c,
  a <= b -> 
  c + a <= c + b.

Axiom R_le_add_r : forall a b c,
  a <= b -> 
  a + c <= b + c.

Axiom R_le_sub_r : forall a b c,
  a <= b -> 
  a - c <= b - c.

Axiom R_mul_pos : forall a b,
  a >= 0 -> 
  b >= 0 -> 
  a * b >= 0.

Axiom R_div_pos : forall a b,
  a >= 0 -> 
  b > 0 -> 
  a/b >= 0.

Axiom R_le_mul_pos_l : forall a b c,
  c >= 0 -> 
  a <= b -> 
  c * a <= c * b.

Axiom R_le_mul_pos_r : forall a b c,
  c >= 0 -> 
  a <= b -> 
  a * c <= b * c.

Axiom R_le_mul_pos_back_l : forall a b c,
  c >= 0 -> 
  c * a <= c * b -> 
  a <= b.

(*
Axiom R_ge_nat : forall n,
  n >= 0.

Axiom R_add_same_div : forall n m p,
  n/p + m/p = (n+m)/p.
*)


(********************************************************************)
(** Nat *)

Axiom R_add_nat : forall n m,
  (n+m)%nat = n + m :> R.

Axiom R_succ_nat' : forall n a,
  (1+n)%nat * a = a + n*a.

Axiom R_succ_nat : forall (n:nat) (a:R),
  ((S n)%nat * a = a + n*a)%R.

Axiom R_mul_one_nat : forall (a:R),
  ((1)%nat * a)%R = a.

Axiom R_nat_pos : forall (n:nat),
  (IZR (my_Z_of_nat n) >= 0)%R.


(********************************************************************)
(** Int *)

Axiom R_int_zero :
  (0%Z) = 0 :> R.

Axiom R_int_one :
  (1%Z) = 1 :> R.

Axiom R_int_two :
  (2%Z) = 2 :> R.

Axiom R_int_three :
  (3%Z) = 3 :> R.

Axiom R_int_four :
  (4%Z) = 4 :> R.

Axiom R_add_int : forall p q,
  (p+q)%Z = p + q :> R.

Axiom R_mul_int : forall p q,
  (p*q)%Z = p * q :> R.

Axiom R_neg_int : forall p,
  (-p)%Z = - p :> R.

Axiom R_sub_int : forall p q,
  (p-q)%Z = p - q :> R.

Axiom R_le_Z : forall p q,
  (p <= q)%Z -> 
  (p <= q)%R.

Axiom R_ge_Z : forall p q,
  (p >= q)%Z -> 
  (p >= q)%R.

Axiom R_gt_Z : forall p q,
  (p > q)%Z -> 
  (p > q)%R.

Axiom R_le_Z_eq : forall p q,
  (p <= q)%Z = 
  (p <= q)%R.

Axiom R_ge_Z_eq : forall p q,
  (p >= q)%Z = 
  (p >= q)%R.

Axiom R_gt_Z_eq : forall p q,
  (p > q)%Z = 
  (p > q)%R.

Axiom R_lt_Z_eq : forall p q,
  (p < q)%Z = 
  (p < q)%R.

Axiom R_eq_Z_eq : forall p q,
  (p = q)%Z = 
  (p = q)%R.


(* todo: move elsewhere *)

Axiom int_mul_add_distrib : forall p q r,
  p * (q + r) = p * q + p * r.

Axiom int_mul_sub_distrib : forall p q r,
  p * (q - r) = p * q - p * r.

Axiom int_nat_add : forall n m,
  (n + m)%nat = (n + m)%Z :> Z.



(********************************************************************)
(** Floor *)

Axiom floor_nat : R -> nat.

Axiom floor_nat_shift : forall a b,
  a >= 0 -> 
  1 + a <= b ->
  (LibOrder.lt (floor_nat a) (floor_nat b)).


(********************************************************************)
(** Log *)

Axiom log : R -> R.

Axiom log_mul : forall x y,
  log (x * y) = log x + log y.

Definition log_base b x := 
  log x / log b.

Axiom log_base_nonneg : forall x b,
  x >= 1 -> log_base b x >= 0.

Axiom log_base_increasing : forall x y b,
  x <= y -> log_base b x <= log_base b y.

Lemma log_base_mul : forall b x,
  log_base b (b * x) = 1 + log_base b x.
Proof.
  intros. unfold log_base. rewrite log_mul. rewrite R_distrib_add_div.
  rewrite~ R_div_self.
Qed.


(********************************************************************)
(* R downto -- not used currently *)

(*
Definition R_down_to (a:R) (x y:R) :=
  (a <= x)%R /\ (1 + x <= y)%R.

Parameter R_down_to_wf : forall a, 
  wf (R_down_to a).
*)

(********************************************************************)
(* min *)

Parameter min : R -> R -> R.

Parameter min_le_args : forall a b a' b',
  a <= a' -> 
  b <= b' ->
  min a b <= min a' b'.
  
Parameter min_sum_distrib : forall a b c,
  min (c + a) (c + b) = c + min a b.


(********************************************************************)
(** Tactic [rew_real] *)

(* [rew_real] normalizes expressions involving only real operations *)

Hint Rewrite R_mul_assoc R_add_assoc 
  R_int_zero R_int_one R_add_zero_l R_add_zero_r
  R_distrib_add_mul R_mult_div_same R_mul_zero R_mul_one
  R_add_nat R_succ_nat R_succ_nat' : rew_real.

Hint Rewrite R_sub_zero R_distrib_sub_add R_distrib_sub_mul : rew_real.

Tactic Notation "rew_real" := 
  autorewrite with rew_real.
Tactic Notation "rew_real" "~" := 
  rew_real; auto_tilde.
Tactic Notation "rew_real" "*" := 
  rew_real; auto_star.

Tactic Notation "rew_real" "in" hyp(H) := 
  autorewrite with rew_real in H.
Tactic Notation "rew_real" "~" "in" hyp(H) := 
  rew_real in H; auto_tilde.
Tactic Notation "rew_real" "*" "in" hyp(H) := 
  rew_real in H; auto_star.

Tactic Notation "rew_real" "in" "*" := 
  autorewrite with rew_real in *.
Tactic Notation "rew_real" "~" "in" "*" := 
  rew_real in *; auto_tilde.
Tactic Notation "rew_real" "*" "in" "*" := 
  rew_real in *; auto_star.

Tactic Notation "rew_reals" := 
  autorewrite with rew_real in *.
Tactic Notation "rew_reals" "~" := 
  rew_reals; auto_tilde.
Tactic Notation "rew_reals" "* " := 
  rew_reals; auto_star.


(********************************************************************)
(** Tactic [rew_real_int] *)

(* [rew_real_int] changes integer operations into real operations *)

Hint Rewrite R_int_four  R_int_three  R_int_zero R_int_one R_int_zero
  R_add_int R_mul_int R_sub_int R_int_two : rew_real_int.

Tactic Notation "rew_real_ints" := 
  autorewrite with rew_real_int in *.
Tactic Notation "rew_real_int" := 
  autorewrite with rew_real_int.
Tactic Notation "rew_real_int" "in" hyp(H) := 
  autorewrite with rew_real_int in H.
Tactic Notation "rew_real_int" "in" "*" := 
  autorewrite with rew_real_int in *.


(********************************************************************)
(** Tactic [rew_real_to_int] *)

(* [rew_real_to_int] changes real operations into integer operations *)

Hint Rewrite <- R_int_four  R_int_three  R_int_zero R_int_one R_int_zero
   R_add_int R_mul_int R_sub_int   R_le_Z_eq  R_ge_Z_eq  R_lt_Z_eq  
  R_gt_Z_eq R_eq_Z_eq : rew_real_to_int.

Tactic Notation "rew_real_to_ints" := 
  autorewrite with rew_real_int in *.
Tactic Notation "rew_real_to_int" := 
  autorewrite with rew_real_to_int.
Tactic Notation "rew_real_to_int" "in" hyp(H) := 
  autorewrite with rew_real_to_int in H.
Tactic Notation "rew_real_to_int" "in" "*" := 
  autorewrite with rew_real_to_int in *.




(********************************************************************)
(** Tactic [simpl_ineq] *)

(** [simpl_ineq] simplifies inequalities of the form:
    [a1 + a2 - a3 + a4 <= b1 + b2 - b3]
    or (with >=), by cancelling out identical terms on both sides *)

Section SimplIneq.

Axiom ineq_add_assoc : forall x y z, 
  x + (y + z) = (x + y) + z.

Axiom ineq_le_to_ge : forall x y, 
  x <= y -> 
  y >= x.

Axiom ineq_ge_to_le : forall x y, 
  x >= y -> 
  y <= x.

Axiom ineq_sub_to_add : forall x y, 
  x - y = x + (-y).

Axiom ineq_remove_zero_r : forall x, 
  x + 0 = x.

Axiom ineq_remove_zero_l : forall x, 
  0 + x = x.

Axiom simpl_ineq_start_1 : forall x1 y, 
  0 + x1 <= y ->
  x1 <= y.

Axiom simpl_ineq_start_2 : forall x1 x2 y, 
  0 + x1 + x2 <= y ->
  x1 + x2 <= y.

Axiom simpl_ineq_start_3 : forall x1 x2 x3 y,
  0 + x1 + x2 + x3 <= y ->
  x1 + x2 + x3 <= y.

Axiom simpl_ineq_start_4 : forall x1 x2 x3 x4 y,
  0 + x1 + x2 + x3 + x4 <= y ->
  x1 + x2 + x3 + x4 <= y.

Axiom simpl_ineq_zero_r1 : forall x y1, 
  x <= y1 + 0 ->
  x <= y1.

Axiom simpl_ineq_zero_r2 : forall x y1 y2, 
  x <= 0 + y1 + y2 ->
  x <= y1 + y2.

Axiom simpl_ineq_skip : forall x y1 y2 y3, 
  x <= y1 + (y2 + y3) ->
  x <= (y1 + y2) + y3.

Axiom simpl_ineq_zero : forall x y1 y3, 
  x <= y1 + y3 ->
  x <= (y1 + 0) + y3.

Axiom simpl_ineq_cancel_0 : forall y1 y2 y3, 
  0 <= y1 + y3 ->
  y2 <= y1 + y2 + y3.

Axiom simpl_ineq_cancel_1 : forall x0 y1 y2 y3, 
  x0 <= y1 + y3 ->
  x0 + y2 <= y1 + y2 + y3.

Axiom simpl_ineq_cancel_2 : forall x0 x1 y1 y2 y3, 
  x0 + x1 <= y1 + y3 ->
  x0 + y2 + x1 <= y1 + y2 + y3.

Axiom simpl_ineq_cancel_3 : forall x0 x1 x2 y1 y2 y3, 
  x0 + x1 + x2 <= y1 + y3 ->
  x0 + y2 + x1 + x2 <= y1 + y2 + y3.

Axiom simpl_ineq_cancel_4 : forall x0 x1 x2 x3 y1 y2 y3, 
  x0 + x1 + x2 + x3 <= y1 + y3 ->
  x0 + y2 + x1 + x2 + x3 <= y1 + y2 + y3.

End SimplIneq.


Hint Rewrite ineq_sub_to_add : ineq_sub_to_add_rew.
Hint Rewrite <- ineq_sub_to_add : ineq_add_to_sub_rew.

Ltac simpl_ineq_left_zero tt :=
  match goal with |- (?HL <= _)%R =>
  match HL with
  | (_ + _ + _ + _)%R => apply simpl_ineq_start_4
  | (_ + _ + _)%R  => apply simpl_ineq_start_3
  | (_ + _)%R => apply simpl_ineq_start_2
  | _ => apply simpl_ineq_start_1
  end end.

Ltac simpl_ineq_setup tt :=
  autorewrite with ineq_sub_to_add_rew;
  try simpl_ineq_left_zero tt;
  try (match goal with |- (_ >= _)%R => apply ineq_le_to_ge end);
  apply simpl_ineq_zero_r1.

Hint Rewrite ineq_remove_zero_r ineq_remove_zero_l ineq_add_assoc
  : simpl_ineq_clean_rew.

Ltac simpl_ineq_cleanup tt :=
  autorewrite with simpl_ineq_clean_rew;
  autorewrite with ineq_add_to_sub_rew;
  try apply R_le_refl. 

Ltac simpl_ineq_find_same Y VL :=
  match VL with
  | Y => apply simpl_ineq_cancel_0
  | (_ + Y)%R => apply simpl_ineq_cancel_1
  | (_ + Y + _)%R => apply simpl_ineq_cancel_2
  | (_ + Y + _ + _)%R => apply simpl_ineq_cancel_3
  | (_ + Y + _ + _ + _)%R => apply simpl_ineq_cancel_4
  end.

Ltac simpl_ineq_step tt :=
  match goal with |- (?VL <= ?VF + ?VR)%R =>
  match VF with
  | (_ + ?VA)%R =>
    match VA with
    | (0%R) => apply simpl_ineq_zero
    | ?Y => simpl_ineq_find_same Y VL
    | _ => apply simpl_ineq_skip
    end
  | 0%R => fail 1
  | _ => apply simpl_ineq_zero_r2
  end end.

Ltac simpl_ineq_main tt :=
  simpl_ineq_setup tt;
  (repeat (simpl_ineq_step tt));
  simpl_ineq_cleanup tt.

Tactic Notation "simpl_ineq" := simpl_ineq_main tt.
Tactic Notation "simpl_ineq" "~" := simpl_ineq; auto_tilde.
Tactic Notation "simpl_ineq" "*" := simpl_ineq; auto_star.



Ltac simpl_ineq_mul_core :=
  try apply R_le_ge;
  repeat (first 
    [ apply R_le_mul_pos_r
    | apply R_le_mul_pos_l ]).

Tactic Notation "simpl_ineq_mul" :=
  simpl_ineq_mul_core.


(********************************************************************)
(** Tactic [rew_ineq] *)

(** [rew_ineq H] where H is of the form [x <= y] applies to a goal
    of the form [a1 + x + a2 <= b] and replaces it with [a1 + y + a2 <= b].
    (works also with >= in the other direction; [rew_ineq <- H] also works) *)
 
Section RewIneq.

Axiom rew_ineq_l0 : forall x0 x0', 
  x0 <= x0' -> forall y,
  x0' <= y ->
  x0 <= y.

Axiom rew_ineq_l1f : forall x0 x0', 
  x0 <= x0' -> forall x1 y,
  x0' + x1 <= y ->
  x0 + x1 <= y.

Axiom rew_ineq_l2f : forall x0 x0', 
  x0 <= x0' -> forall x1 x2 y,
  x0' + x1 + x2 <= y ->
  x0 + x1 + x2 <= y.

Axiom rew_ineq_l3f : forall x0 x0', 
  x0 <= x0' -> forall x1 x2 x3 y,
  x0' + x1 + x2 + x3 <= y ->
  x0 + x1 + x2 + x3 <= y.

Axiom rew_ineq_l1 : forall x0 x0', 
  x0 <= x0' -> forall x1 y,
  x1 + x0' <= y ->
  x1 + x0 <= y.

Axiom rew_ineq_l2 : forall x0 x0', 
  x0 <= x0' -> forall x1 x2 y,
  x1 + x0' + x2 <= y ->
  x1 + x0 + x2 <= y.

Axiom rew_ineq_l3 : forall x0 x0', 
  x0 <= x0' ->  forall x1 x2 x3 y,
  x1 + x0' + x2 + x3 <= y ->
  x1 + x0 + x2 + x3 <= y.

Axiom rew_ineq_r0 : forall x0 x0', 
  x0 <= x0' -> forall y,
  y <= x0 ->
  y <= x0'.

Axiom rew_ineq_r1f : forall x0 x0', 
  x0 <= x0' -> forall x1 y,
  y <= x0 + x1 ->
  y <= x0' + x1.

Axiom rew_ineq_r2f : forall x0 x0', 
  x0 <= x0' -> forall x1 x2 y,
  y <= x0 + x1 + x2 ->
  y <= x0' + x1 + x2.

Axiom rew_ineq_r3f : forall x0 x0', 
  x0 <= x0' ->  forall x1 x2 x3 y,
  y <= x0 + x1 + x2 + x3 ->
  y <= x0' + x1 + x2 + x3.

Axiom rew_ineq_r1 : forall x0 x0', 
  x0 <= x0' -> forall x1 y,
  y <= x1 + x0 ->
  y <= x1 + x0'.

Axiom rew_ineq_r2 : forall x0 x0', 
  x0 <= x0' -> forall x1 x2 y,
  y <= x1 + x0 + x2 ->
  y <= x1 + x0' + x2.

Axiom rew_ineq_r3 : forall x0 x0', 
  x0 <= x0' -> forall x1 x2 x3 y,
  y <= x1 + x0 + x2 + x3 ->
  y <= x1 + x0' + x2 + x3.

End RewIneq.

Ltac ineq_ensure_goal_le tt :=
  try (match goal with |- (_ >= _)%R => apply ineq_le_to_ge end).

Ltac ineq_ensure_le H :=
  match type of H with
  | (_ >= _)%R => constr:(@ineq_ge_to_le _ _ H)
  | (_ <= _)%R => constr:(H)
  end.

Ltac rew_ineq_l H' :=
  ineq_ensure_goal_le tt; 
  let H := ineq_ensure_le H' in
  first 
  [ apply (@rew_ineq_l0 _ _ H)
  |  apply (@rew_ineq_l1 _ _ H)
  |  apply (@rew_ineq_l2 _ _ H)
  |  apply (@rew_ineq_l3 _ _ H)
  |  apply (@rew_ineq_l1f _ _ H)
  |  apply (@rew_ineq_l2f _ _ H)
  |  apply (@rew_ineq_l3f _ _ H)
  ].

Ltac rew_ineq_r H' :=
  ineq_ensure_goal_le tt; 
  let H := ineq_ensure_le H' in
  first 
  [ apply (@rew_ineq_r0 _ _ H)
  |  apply (@rew_ineq_r1 _ _ H)
  |  apply (@rew_ineq_r2 _ _ H)
  |  apply (@rew_ineq_r3 _ _ H)
  |  apply (@rew_ineq_r1f _ _ H)
  |  apply (@rew_ineq_r2f _ _ H)
  |  apply (@rew_ineq_r3f _ _ H)
  ].

Tactic Notation "rew_ineq" constr(H) :=
  rew_ineq_l H.
Tactic Notation "rew_ineq" "->" constr(H) :=
  rew_ineq_l H.
Tactic Notation "rew_ineq" "<-" constr(H) :=
  rew_ineq_r H.


(********************************************************************)
(** Demos of tactics *)

Section Rineq_demos.

Lemma simpl_ineq_demo_1 : forall x1 x2 x3 x4 x5,
  x1 + x2 + x3 + x4 <= x2 + x3 + x5.
Proof.
  intros. dup.
  simpl_ineq_setup tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  try simpl_ineq_step tt.
  simpl_ineq_cleanup tt.
  skip.
  simpl_ineq.
Admitted.

Lemma simpl_ineq_demo_2 : forall x1 x2 x3 x4 x5,
  0 + x2 + x4 - x1 - x2/x3 + 0 + x5 >= x1 - x2/x3 + x4.
Proof.
  intros. dup.
  simpl_ineq_setup tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  simpl_ineq_step tt.
  simpl_ineq_cleanup tt.
  skip.
  simpl_ineq.
Admitted.

Lemma rew_ineq_demo : forall x1 x2 x3 x4 x5 x6 x7 x8,
  x1 <= x2 ->
  x3 <= x4 ->
  x5 + x1 + x6 <= x7 + x4 + x8.
Proof.
  introv H1 H2. rew_ineq H1. rew_ineq <- H2.
Admitted.

End Rineq_demos.



(********************************************************************)
(** Tactics [simpl_credits] *)

(** [simpl_credits] to simplify an expression or inequality involving credits *)

Ltac simpl_credits_core :=
  simpl_ineq.

Tactic Notation "simpl_credits" := 
  simpl_credits_base.
Tactic Notation "simpl_credits" "~" :=
  simpl_credits; auto_tilde.
Tactic Notation "simpl_credits" "*" :=
  simpl_credits; auto_star.


(** Demo *)

Lemma simpl_credits_demo : forall (n:nat) (x y z : int), 
  (1+n)%nat * x + y >= x + z + x * n.
Proof. intros. simpl_credits. admit.  ... (* demo *) Qed.  
