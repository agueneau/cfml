Set Implicit Arguments.
Require Import LibTactics.
Require CFPrint.

(* re-export this notation in the charac scope *)

(* DEPRECATED
Notation "'Register' D T" := (ltac_database (boxer D) (boxer T) _) 
  (at level 69, D at level 0, T at level 0) : cfheader_scope.
*)

Notation "'RegisterCF' T" := (ltac_database (boxer CFPrint.database_cf) (boxer T) _) 
  (at level 69, T at level 0) : cfheader_scope.


Ltac CFHeader_Provide T := Provide T.

