Require Export CFLibCredits.

Ltac xpay_core tt ::= xpay_nat_core tt.

Notation "'\$' x" := (\$_nat x) 
  (at level 40, format "\$ x") : heap_scope.

