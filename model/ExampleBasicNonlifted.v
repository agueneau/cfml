(**

This file formalizes basic examples in plain Separation Logic, 
both using triples directly and using characteristic formulae.

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Require Import LambdaSep LambdaCF TLCbuffer LambdaStruct.
Generalizable Variables A B.

Ltac auto_star ::= jauto.

Implicit Type p q : loc.
Implicit Types n : int.


(* ********************************************************************** *)
(* * Basic functions *)

(* From LambdaStruct

Definition val_incr :=
  Vars P, N, M in
  ValFun P := 
    Let N := val_get P in
    Let M := val_add N 1 in
    val_set P M.
*)

(* ---------------------------------------------------------------------- *)
(** Increment function -- details *)

(** Low-level proof *)

Lemma rule_incr_1 : forall p n, 
  triple (val_incr p)
    (p ~~~> n) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1))).
Proof using.
  intros. applys rule_app_fun. reflexivity. simpl.
  applys rule_let. { applys rule_get. } simpl.
  intros x. apply rule_extract_hprop. intros E. subst.
  applys rule_let.
  { applys rule_frame_consequence (p ~~~> n).
    { hsimpl. }
    { applys rule_add. } 
    { hsimpl. } }
  simpl. intros y. apply rule_extract_hprop. intros E. subst.
  applys rule_consequence.
  { hsimpl. }
  { applys rule_set. }
  { intros r. applys himpl_hprop_l. intros E. subst.
    applys himpl_hprop_r. { auto. } { auto. } }
Qed.

(** Same proof using [xapply], [xapplys] and [xpull] *)

Lemma rule_incr_2 : forall p n, 
  triple (val_incr p)
    (p ~~~> n) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1))).
Proof using.
  intros. applys rule_app_fun. reflexivity. simpl.
  applys rule_let. { applys rule_get. } simpl.
  intros x. xpull. intros E. subst.
  applys rule_let. { xapplys rule_add. }
  simpl. intros y. xpull. intro_subst.
  xapplys rule_set. auto.
Qed.

(** Same proof using characteristic formulae without tactics *)

Lemma rule_incr_3 : forall p n, 
  triple (val_incr p)
    (p ~~~> n) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1))).
Proof using.
  intros. applys triple_app_fun_of_cf_iter 20%nat. reflexivity. simpl. 
  applys local_erase. esplit. split.
  { applys local_erase. xapplys rule_get. }
  intros x. xpull. intros E. subst.
  applys local_erase. esplit. split. 
  { applys local_erase. xapplys rule_add. }
  intros y. xpull. intros E. subst.
  applys local_erase. xapplys rule_set. auto.
Qed.

(** Same proof using support for nary functions *)

Lemma rule_incr_4 : forall p n, 
  triple (val_incr p)
    (p ~~~> n) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1))).
Proof using.
  intros. rew_nary. unfold val_incr. 
  rew_nary. rew_vals_to_trms. (* show coercion *)
  applys triple_apps_funs_of_cf_iter 20%nat.
  { reflexivity. }
  { reflexivity. }
  simpl. 
  (* then continue as before *)
Abort.

(** Same proof using characteristic formulae with tactics *)

Lemma rule_incr_5 : forall p n, 
  triple (val_incr p)
    (p ~~~> n) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1))).
Proof using.
  xcf. xlet as x. xapp. xpull. intro_subst.
  xlet as y. xapp. xpull. intro_subst.
  xapp. hsimpl. auto.
Qed.

(** Same proof using characteristic formulae with more tactics *)

Lemma rule_incr_6 : forall p n, 
  triple (val_incr p)
    (p ~~~> n) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1))).
Proof using.
  xcf. xapp as x. intro_subst.
  xapp as y. intro_subst.
  xapps. hsimpl~.
Qed.

(** Same proof using characteristic formulae with yet more
  powerful tactics *)

Lemma rule_incr__7 : forall p n, 
  triple (val_incr p)
    (p ~~~> n) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1))).
Proof using. 
  xcf. xapps. xapps. xapps. hsimpl~.
Qed.


(* ---------------------------------------------------------------------- *)
(** Calling incr from a larger context *) 

Lemma rule_incr_with_other_1 : forall p n q m, 
  triple (val_incr p)
    (p ~~~> n \* q ~~~> m) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1)) \* q ~~~> m).
Proof using.
  intros. applys rule_frame_consequence (q ~~~> m).
  { hsimpl. }
  { rew_heap. apply rule_incr_5. }
  { intros r. hsimpl. auto. }
Qed.

Lemma rule_incr_with_other_2 : forall p n q m, 
  triple (val_incr p)
    (p ~~~> n \* q ~~~> m) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1)) \* q ~~~> m).
Proof using.
  intros. xapply rule_incr_5.
  { hsimpl. }
  { intros r. hsimpl. auto. }
Qed.

Lemma rule_incr_with_other : forall p n q m, 
  triple (val_incr p)
    (p ~~~> n \* q ~~~> m) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1)) \* q ~~~> m).
Proof using. intros. xapps. hsimpl~. Qed.

Lemma rule_incr_with_frame : forall p n H, 
  triple (val_incr p)
    (p ~~~> n \* H) 
    (fun r => \[r = val_unit] \* (p ~~~> (n+1)) \* H).
Proof using. intros. xapps. hsimpl~. Qed.


(* ---------------------------------------------------------------------- *)
(** Swap function *)

Definition val_swap :=
  Vars P, Q, X, Y in
  ValFun P Q := 
    Let X := val_get P in
    Let Y := val_get Q in
    val_set P Y ;;
    val_set Q X.

Lemma rule_swap_neq : forall p q v w, 
  triple (val_swap p q)
    (p ~~~> v \* q ~~~> w) 
    (fun r => \[r = val_unit] \* p ~~~> w \* q ~~~> v).
Proof using.
  xcf. xapps. xapps. xapp~. xapps. hsimpl~.
Qed.

Lemma rule_swap_eq : forall p v, 
  triple (val_swap p p)
    (p ~~~> v) 
    (fun r => \[r = val_unit] \* p ~~~> v).
Proof using.
  xcf. xapps. xapps. xapp~. xapps. hsimpl~.
Qed.


(* ---------------------------------------------------------------------- *)
(** Succ function using incr *)

Definition val_succ_using_incr :=
  Vars N, P, X in
  ValFun N := 
    Let P := val_ref N in
    val_incr P ;;
    Let X := val_get P in 
    X.

Lemma rule_succ_using_incr : forall n, 
  triple (val_succ_using_incr n)
    \[] 
    (fun r => \[r = n+1]).
Proof using.
  xcf. xapp as p. intros; subst. xapp~. xapps~.
  (* not possible: applys local_erase. unfold cf_val. hsimpl. *)
  xvals~.
Qed.




