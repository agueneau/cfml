(**

This file formalizes mutable list examples in plain Separation Logic, 
using both triples and characteristic formulae.

Author: Arthur Charguéraud.
License: MIT.

*)

Set Implicit Arguments.
Require Import LambdaCF LambdaStruct.
Generalizable Variables A B.

Ltac auto_star ::= jauto.

Implicit Types p q : loc.
Implicit Types n : int.
Implicit Types v : val.


(* ********************************************************************** *)
(* * List cells *)

(* ---------------------------------------------------------------------- *)
(** Representation *)

(** Identification of head and tail fields *)

Definition hd : field := 0%nat.
Definition tl : field := 1%nat.

(** [Mcell v q p] describes one list cell allocated at address [p],
  with head value [v] and tail value [q]. *)

Definition MCell (v:val) (q:val) (p:loc) :=
  (p `.` hd ~~~> v) \* (p `.` tl ~~~> q).


(* ---------------------------------------------------------------------- *)
(** Tactics *)

(** Tactic hack to make [hsimpl] able to cancel out [hfield]
    and [Mcell] predicates in heap entailment.
    Later won't be needed when using [~>] notation. *)

Ltac hcancel_hook H ::=
  match H with 
  | hsingle _ _ => hcancel_try_same tt 
  | hfield _ _ _ => hcancel_try_same tt 
  | MCell _ _ _ => hcancel_try_same tt
  end.


(* ---------------------------------------------------------------------- *)
(** Properties of list cells *)

Lemma MCell_eq : forall (v:val) (q:val) (p:loc),
  MCell v q p = (p `.` hd ~~~> v) \* (p `.` tl ~~~> q).
Proof using. auto. Qed.

Lemma MCell_inv_not_null : forall p v q,
  (MCell v q p) ==+> \[p <> null].
  (* i.e. (MCell v q p) ==> (MCell v q p) \* \[p <> null]. *)
Proof using.
  intros. unfold MCell, hd.  
  hchange~ (hfield_not_null p hd). hsimpl~.
Qed.

Implicit Arguments MCell_inv_not_null [].

Lemma MCell_null_false : forall v q,
  (MCell v q null) ==> \[False].
Proof using. intros. hchanges~ (MCell_inv_not_null null). Qed.

Implicit Arguments MCell_null_false [].

Lemma MCell_hstar_MCell_inv : forall p1 p2 x1 x2 y1 y2,
  MCell x1 y1 p1 \* MCell x2 y2 p2 ==+> \[p1 <> p2].
Proof using.
  intros. do 2 rewrite MCell_eq. tests C: (p1 = p2).
  { hchanges (@hstar_hfield_same_loc_disjoint p2 hd). }
  { hsimpl~. } 
Qed.


(* ---------------------------------------------------------------------- *)
(** Access to list cells *)

(** Read to head *)

Definition val_get_hd := val_get_field hd.

Lemma rule_get_hd : forall p v q,
  triple (val_get_hd p) 
    (MCell v q p)
    (fun r => \[r = v] \* (MCell v q p)).
Proof using.
  intros. unfold MCell. xapplys rule_get_field. auto.
Qed.

Hint Extern 1 (Register_spec val_get_hd) => Provide rule_get_hd.

(** Read to tail *)

Definition val_get_tl := val_get_field tl.

Lemma rule_get_tl : forall p v q,
  triple (val_get_tl p) 
    (MCell v q p)
    (fun r => \[r = q] \* (MCell v q p)).
Proof using.
  intros. unfold MCell.
  xapplys rule_get_field. auto.
Qed.

Hint Extern 1 (Register_spec val_get_tl) => Provide rule_get_tl.

(** Write to head *)

Definition val_set_hd := val_set_field hd.

Lemma rule_set_hd : forall p v' v vq,
  triple (val_set_hd p v) 
    (MCell v' vq p)
    (fun r => \[r = val_unit] \* MCell v vq p).
Proof using.
  intros. unfold MCell. xapplys (rule_set_field v'). auto.
Qed.

Hint Extern 1 (Register_spec val_set_hd) => Provide rule_set_hd.

(** Write to tail *)

Definition val_set_tl := val_set_field tl.

Lemma rule_set_tl : forall p v q vq',
  triple (val_set_tl p q) 
    (MCell v vq' p)
    (fun r => \[r = val_unit] \* MCell v q p).
Proof using.   
  intros. unfold MCell. xapplys (rule_set_field vq'). auto.
Qed. 

Hint Extern 1 (Register_spec val_set_tl) => Provide rule_set_tl.


(* ---------------------------------------------------------------------- *)
(** Allocation of list cells *)

Definition val_new_cell := 
  Vars X, Y, P in
  ValFun X Y := 
    Let P := val_alloc 2 in
    val_set_hd P X;;
    val_set_tl P Y;;
    P.

Lemma rule_alloc_cell : 
  triple (val_alloc 2) 
    \[]
    (fun r => Hexists (p:loc), Hexists v1 v2,
              \[r = p] \* MCell v1 v2 p).
Proof using. 
  xapply rule_alloc. { math. } { hsimpl. } 
  { intros r. hpull ;=> l (E&N). subst.
    simpl_abs. rew_Alloc. hpull ;=> v1 v2.
    unfold MCell. rewrite hfield_eq_fun_hsingle.
    unfold hd, tl. hsimpl~ l v1 v2.
    math_rewrite (l + 1 = S l)%nat. 
    math_rewrite (l+0 = l)%nat. hsimpl. }
Qed.

Lemma rule_new_cell : forall v q,
  triple (val_new_cell v q)
    \[] 
    (fun r => Hexists p, \[r = val_loc p] \* MCell v q p).
Proof using.
  intros. xcf. xapp rule_alloc_cell.
  intros p p' v' q'. intro_subst.
  xapps~. xapps~. xvals~.
Qed.

(* TODO: update?
Lemma rule_new_cell : forall v q,
  triple (val_new_cell v q)
    \[] 
    (fun r => Hexists p, \[r = val_loc p] \* MCell v q p).
Proof using.
  intros. applys rule_app_fun2. reflexivity. auto. simpl.
  applys rule_let. { applys rule_alloc_cell. }
  intros p. xpull ;=> p' v' q'. intro_subst. simpl.  
  applys rule_seq. { xapplys rule_set_hd. }
  applys rule_seq. { xapplys rule_set_tl. }
  applys rule_val. hsimpl. auto.
Qed.
*)

Hint Extern 1 (Register_spec val_new_cell) => Provide rule_new_cell.

Global Opaque MCell_eq.


(* ********************************************************************** *)
(* * Mutable lists *)

(** Layout in memory:

  type 'a mlist = { mutable hd : 'a ; mutable tl : 'a mlist }
  // empty lists represented using null

*)

(* ---------------------------------------------------------------------- *)
(** Representation *)

Fixpoint MList (L:list val) (p:loc) : hprop :=
  match L with
  | nil => \[p = null]
  | x::L' => Hexists (p':loc), (MCell x p' p) \* (MList L' p')
  end.


(* ---------------------------------------------------------------------- *)
(** Tactic *)

(** Tactic hack to make [hsimpl] able to cancel out [MList] in 
   heap entailments. Later won't be needed when using [~>] notation. *)

Ltac hcancel_hook H ::=
  match H with 
  | hsingle _ _ => hcancel_try_same tt 
  | hfield _ _ _ => hcancel_try_same tt 
  | MCell _ _ _ => hcancel_try_same tt
  | MList _ _ => hcancel_try_same tt
  end.


(* ---------------------------------------------------------------------- *)
(** Properties *)

Section MListProperties.
Implicit Types L : list val.

(** For empty lists *)

Lemma MList_nil_eq : forall p,
  MList nil p = \[p = null].
Proof using. intros. unfolds~ MList. Qed.

Lemma MList_null_eq : forall L,
  MList L null = \[L = nil].
Proof using. 
  intros. destruct L.
  { xunfold MList. applys himpl_antisym; hsimpl~. }
  { xunfold MList. applys himpl_antisym. 
    { hpull ;=> p'. hchanges (MCell_null_false v p'). }
    { hpull. } } 
Qed.  

Lemma MList_null_inv : forall L,
  MList L null ==+> \[L = nil].
Proof using. intros. rewrite MList_null_eq. hsimpl~. Qed.

Lemma MList_nil : 
  \[] ==> MList nil null.
Proof using. intros. rewrite MList_nil_eq. hsimpl~. Qed.

(** For nonempty lists *)

Lemma MList_cons_eq : forall p x L',
  MList (x::L') p = 
  Hexists (p':loc), MCell x p' p \* MList L' p'.
Proof using. intros. unfold MList at 1. simple~. Qed.

Lemma MList_cons : forall p p' x L',
  MCell x p' p \* MList L' p' ==> MList (x::L') p.
Proof using. intros. rewrite MList_cons_eq. hsimpl. Qed.

Lemma MList_not_null_inv_not_nil : forall p L,
  p <> null -> 
  MList L p ==+> \[L <> nil].
Proof using. 
  introv N. destruct L.
  { rewrite (MList_nil_eq p). hsimpl. }
  { hsimpl. auto_false. }
Qed.

Lemma MList_not_null_inv_cons : forall p L,
  p <> null -> 
    MList L p ==> 
    Hexists (p':loc), Hexists x L', 
       \[L = x::L'] \* MCell x p' p  \* MList L' p'.
Proof using.
  intros. hchange~ (@MList_not_null_inv_not_nil p).
  hpull. intros. destruct L; tryfalse.
  { hchanges~ (MList_cons_eq p). }
Qed.

End MListProperties.

Implicit Arguments MList_null_inv [].
Implicit Arguments MList_cons_eq [].
Implicit Arguments MList_cons [].
Implicit Arguments MList_not_null_inv_cons [].
Implicit Arguments MList_not_null_inv_not_nil [].

Global Opaque MList.


(* ********************************************************************** *)
(* * Length of a mutable list *)

(* ---------------------------------------------------------------------- *)
(** Implementation *)

Definition val_mlist_length : val :=
  Vars F, P, Q, N in
  ValFix F P :=   
    If_ val_neq P null Then (
      Let Q := val_get_tl P in
      Let N := F Q in
      val_add N 1
    ) Else (
      0
    ).


(* ---------------------------------------------------------------------- *)
(** Verification using triples. *)

(* TODO: fix the proof. it is meant to give an idea of the length.

  Lemma rule_mlist_length_1 : forall L (p:loc), 
    triple (val_mlist_length p) 
      (MList L p)
      (fun r => \[r = (length L : int)] \* MList L p).
  Proof using.
    intros L. induction_wf: list_sub_wf L. intros p. 
    applys rule_app_fix. reflexivity. simpl.
    applys rule_if'. { xapplys rule_neq. }
    simpl. intros X. xpull. intros EX. subst X.
    case_if as C1; case_if as C2; tryfalse.
    { inverts C2. xchange MList_null_inv.
      xpull. intros EL. applys rule_val. hsimpl. subst~. }
    { xchange (MList_not_null_inv_cons p). { auto. }
      xpull. intros x p' L' EL. applys rule_let.
      { xapplys rule_get_tl. }
      { simpl. intros p''. xpull. intros E. subst p''. 
        applys rule_let.
        { simpl. xapplys IH. { subst~. } }
        { simpl. intros r. xpull. intros Er. subst r. 
          xchange (MList_cons p p' x L'). 
          xapplys rule_add_int.
          { intros. subst L. hsimpl. }
          { intros. subst. rew_length. fequals. math. } } } }
  Qed.

*)

(* ---------------------------------------------------------------------- *)
(** Verification using characteristic formulae, but no tactics. *)

(** Observe how, in the proof below, the deep embedding is never revealed. *)

Lemma rule_mlist_length_2 : forall L p, 
  triple (val_mlist_length p)
    (MList L p)
    (fun r => \[r = val_int (length L)] \* MList L p).
Proof using.
  intros L. induction_wf: list_sub_wf L. intros p. 
  applys triple_app_fix_of_cf_iter 20%nat. reflexivity.
  simpl. applys local_erase. esplit. split. 
  { applys local_erase. xapplys rule_neq. }
  intros X. xpull. intros EX. subst X.
  applys local_erase. esplit. splits. eauto.
  { intros C. logics. xchange (MList_not_null_inv_cons p). { auto. }  
    xpull. intros p' x L' EL.
    applys local_erase. esplit. split.
    { applys local_erase. xapplys rule_get_tl. }
    intros p''. xpull. intros E. subst p''.
    applys local_erase. esplit. split.
    { applys local_erase. xapplys IH. { subst~. } }
    { intros r. xpull. intros Er. xchange (MList_cons p). 
      subst r L. applys local_erase. xapplys rule_add.
      { intros. subst. rew_length. fequals. math. } } }  
  { intros C. logics. applys local_erase. unfolds. inverts C.
    xchange MList_null_inv. hpull. intros EL.
    hsimpl. subst~. }
Qed.


(* ---------------------------------------------------------------------- *)
(** Verification using characteristic formulae, and basic tactics. *)

Lemma rule_mlist_length_3 : forall L p, 
  triple (val_mlist_length p)
    (MList L p)
    (fun r => \[r = val_int (length L)] \* MList L p).
Proof using.
  intros L. induction_wf: list_sub_wf L. intros p. xcf.
  xlet as b. { xapp. } xpull ;=> Eb. subst.
  xif ;=> C.
  { xchange (MList_not_null_inv_cons p). { auto. }
    xpull ;=> p' x L' EL.
    xlet as p''. { xapp. }
    xpull ;=> E. subst p''.
    xlet as r. { xapp IH. { subst~. } }
    xpull ;=> Er. subst r L. xchange (MList_cons p). 
    xapp. { hsimpl. intros. subst. rew_length. fequals. math. } }  
  { xval. inverts C. xchange MList_null_inv.  
    hpull ;=> EL. hsimpl. subst~. }
Qed.


(* ---------------------------------------------------------------------- *)
(** Verification using characteristic formulae, and advanced tactics. *)

Lemma rule_mlist_length_1 : forall L p, 
  triple (val_mlist_length p)
    (MList L p)
    (fun r => \[r = val_int (length L)] \* MList L p).
Proof using.
  intros L. induction_wf: list_sub_wf L. xcf.
  xapps. xif ;=> C.
  { xchanges~ (MList_not_null_inv_cons p) ;=> x p' L' EL. subst L.
    xapps. xapps~ IH.
    xchange (MList_cons p). 
    xapps. hsimpl. isubst. rew_length. fequals. math. }  
  { inverts C. xchanges MList_null_inv ;=> EL. subst. xvals~. }
Qed.


(* ********************************************************************** *)
(* * Length of a mutable list using a loop *)

(* ---------------------------------------------------------------------- *)
(** Implementation *)

Definition val_mlist_length_loop : val :=
  Vars P0, R, P, Q, N in
  ValFun P0 :=   
    Let R := val_ref P0 in
    Let N := val_ref 0 in
    While (Let P := val_get R in 
           val_neq P null) Do
      val_incr N ;;
      Let P := val_get R in 
      Let Q := val_get_tl P in
      val_set R Q 
    Done ;;
    val_get N.

Lemma rule_mlist_length_loop : forall L p, 
  triple (val_mlist_length_loop p)
    (MList L p)
    (fun r => \[r = val_int (length L)] \* MList L p).
Proof using.
  xcf. xapp ;=> V r E; subst V. xapp ;=> V n E; subst V.
  xwhile as R.
  { cuts K: (forall (nacc:int),
       R (r ~~~> p \* MList L p \* n ~~~> nacc)
         (fun r' => \[r' = val_unit] \* MList L p \* n ~~~> (nacc + length L))).
    { xapplys* K. }
    gen p. induction_wf: list_sub_wf L; intros. applys (rm HR).
    xlet. { xapps. xapps. } xpulls. xif ;=> C.
    { xchanges~ (MList_not_null_inv_cons p) ;=> p' x L' EL. xseq.
      { xseq. xapp~. xapps.
        xapps. xapps~. } 
      { xapply (>> IH L'). { subst~. } { hsimpl. }
        { hpull. isubst. hchange (MList_cons p). subst. rew_list.
          math_rewrite (forall x (y:nat), (x+1)+y = x+(1+y)%nat). hsimpl~. } } }
          (* todo: cancel on ~~> *)
    { inverts C. xchanges MList_null_inv ;=> EL. subst. rew_list.
      math_rewrite (nacc+0%nat = nacc). xvals~. } }
  { xapp. hsimpl. isubst. fequals. }
Qed.



(* ********************************************************************** *)
(* * Mutable lists Segments *)

(* ---------------------------------------------------------------------- *)
(** Representation *)

Fixpoint MListSeg (q:loc) (L:list val) (p:loc) : hprop :=
  match L with
  | nil => \[p = q]
  | x::L' => Hexists (p':loc), (MCell x p' p) \* (MListSeg q L' p')
  end.

(* ---------------------------------------------------------------------- *)
(** Hack *)

(** Tactic hack to make [hsimpl] able to cancel out [MList] 
    in heap entailment. *)

Ltac hcancel_hook H ::=
  match H with 
  | hsingle _ _ => hcancel_try_same tt 
  | hfield _ _ _ => hcancel_try_same tt 
  | MCell _ _ _ => hcancel_try_same tt
 (* TODO  | MList _ _ => hcancel_try_same tt *)
  | MListSeg _ _ _ => hcancel_try_same tt
  end.


(* ---------------------------------------------------------------------- *)
(** Properties *)

Section Properties.
Implicit Types L : list val.

Lemma MListSeg_nil_eq : forall p q,
  MListSeg q nil p = \[p = q].
Proof using. intros. unfolds~ MListSeg. Qed.

Lemma MListSeg_cons_eq : forall p q x L',
  MListSeg q (x::L') p = 
  Hexists (p':loc), MCell x p' p \* MListSeg q L' p'.
Proof using. intros. unfold MListSeg at 1. simple~. Qed.

Global Opaque MListSeg.

Lemma MListSeg_nil : forall p,
  \[] ==> MListSeg p nil p.
Proof using. intros. rewrite MListSeg_nil_eq. hsimpl~. Qed.

Lemma MListSeg_cons : forall p p' q x L',
  MCell x p' p \* MListSeg q L' p' ==> MListSeg q (x::L') p.
Proof using. intros. rewrite MListSeg_cons_eq. hsimpl. Qed.

Lemma MListSeg_one : forall p q x,
  MCell x q p ==> MListSeg q (x::nil) p.
Proof using. 
  intros. hchange (@MListSeg_nil q). hchange MListSeg_cons. hsimpl. 
Qed.

Lemma MListSeg_concat : forall p1 p2 p3 L1 L2,
  MListSeg p2 L1 p1 \* MListSeg p3 L2 p2 ==> MListSeg p3 (L1++L2) p1.
Proof using.
  intros. gen p1. induction L1 as [|x L1']; intros.
  { rewrite MListSeg_nil_eq. hpull ;=> E. subst. rew_list~. }
  { rew_list. hchange (MListSeg_cons_eq p1). hpull ;=> p1'.
    hchange (IHL1' p1'). hchanges (@MListSeg_cons p1). }
Qed.

Lemma MListSeg_last : forall p1 p2 p3 x L,
  MListSeg p2 L p1 \* MCell x p3 p2 ==> MListSeg p3 (L&x) p1.
Proof using.
  intros. hchange (@MListSeg_one p2). hchanges MListSeg_concat.
Qed.

Lemma MListSeg_then_MCell_inv_neq : forall p q L v1 v2,
  MListSeg q L p \* MCell v1 v2 q ==> 
  MListSeg q L p \* MCell v1 v2 q \* \[L = nil <-> p = q].
Proof using.
  intros. destruct L.
  { rewrite MListSeg_nil_eq. hsimpl*. split*. (* TODO: why not proved? *) }
  { rewrite MListSeg_cons_eq. hpull ;=> p'. tests: (p = q).
    { hchanges (@MCell_hstar_MCell_inv q). } 
    { hsimpl. split; auto_false. } }
Qed.

End Properties.

Implicit Arguments MListSeg_then_MCell_inv_neq [].


(* ********************************************************************** *)
(* * Mutable queue *)

(* ---------------------------------------------------------------------- *)
(** Representation *)

Definition MQueue (L:list val) (p:loc) :=
  Hexists (pf:loc), Hexists (pb:loc), Hexists (vx:val), Hexists (vy:val),
    MCell pf pb p \* MListSeg pb L pf \* MCell vx vy pb.

Definition val_create :=
  Vars R, V in
  ValFun V := 
    Let R := val_alloc 2 in
    val_new_cell R R.

Lemma rule_create : 
  triple (val_create val_unit)
    \[]
    (fun r => Hexists p, \[r = val_loc p] \* MQueue nil p).
Proof using. 
  xcf. unfold MQueue. 
  xapp rule_alloc_cell as r. intros p v1 v2. intro_subst.
  xapp~. hpull ;=> r x E. hsimpl~.
  { rewrite MListSeg_nil_eq. hsimpl~. }
Qed.


(* ---------------------------------------------------------------------- *)
(** Is empty *)

Definition val_is_empty :=
  Vars P, X, Y in
  ValFun P := 
    Let X := val_get_hd P in
    Let Y := val_get_tl P in
    val_eq X Y.

Lemma rule_is_empty : forall p L,
  triple (val_is_empty p)
    (MQueue L p)
    (fun r => \[r = isTrue (L = nil)] \* MQueue L p).
Proof using.
  xcf. unfold MQueue. xpull ;=> pf pb vx vy.
  xapps. xapps. 
  xchanges (MListSeg_then_MCell_inv_neq pf pb) ;=> R.
  (* xchange (MListSeg_then_MCell_inv_neq pf pb). xpull ;=> R. *)
  xapp. hsimpl. isubst. fequals. logics. rewrite R. iff; congruence.
Qed.

Hint Extern 1 (Register_spec val_is_empty) => Provide rule_is_empty.


(* ---------------------------------------------------------------------- *)
(** Push back *)

Definition val_push_back := 
  Vars V, P, Q, R in
  ValFun V P := 
    Let Q := val_get_tl P in
    Let R := val_alloc 2 in
    val_set_hd Q V ;;
    val_set_tl Q R ;;
    val_set_tl P R.

Lemma rule_push_back : forall v p L,
  triple (val_push_back v p)
    (MQueue L p)
    (fun r => \[r = val_unit] \* MQueue (L&v) p).
Proof using.
  xcf. unfold MQueue. xpull ;=> pf pb vx vy.
  xapps. xapp rule_alloc_cell as r. intros pb' v1 v2. intro_subst.
  xapp~. xapp~. xapp~. hchanges~ MListSeg_last.
Qed.


(* ---------------------------------------------------------------------- *)
(** Push front *)

Definition val_push_front :=
  Vars V, P, Q, R in 
  ValFun V P := 
    Let Q := val_get_hd P in
    Let R := val_new_cell V Q in
    val_set_hd P R.

Lemma rule_push_front : forall v p L,
  triple (val_push_front v p)
    (MQueue L p)
    (fun r => \[r = val_unit] \* MQueue (v::L) p).
Proof using.
  xcf. unfold MQueue. xpull ;=> pf pb vx vy.
  xapps. xapp as r. intros x. intro_subst.
  xapp. hsimpl~. isubst. hchanges (@MListSeg_cons x).
Qed.


(* ---------------------------------------------------------------------- *)
(** Pop front *)

Definition val_pop_front := 
  Vars V, P, Q, R, X in
  ValFun V P := 
    Let Q := val_get_hd P in
    Let X := val_get_hd Q in  
    Let R := val_get_tl Q in
    val_set_hd P R;;
    X. 

Lemma rule_pop_front : forall v p L,
  L <> nil ->
  triple (val_pop_front v p)
    (MQueue L p)
    (fun v => Hexists L', \[L = v::L'] \* MQueue L' p).
Proof using.
  xcf. unfold MQueue. xpull ;=> pf pb vx vy.
  destruct L as [|x L']; tryfalse.
  rewrite MListSeg_cons_eq. xpull ;=> pf'.
  xapps. xapps. xapps. xapp~. xvals~.
Qed.


(* ---------------------------------------------------------------------- *)
(** Transfer *)

Definition val_transfer := 
  Vars P1, F1, B1, X1, N1 in
  Vars P2, F2, B2, X2, N2 from 5%nat in
  Vars E2 from 10%nat in
  ValFun P1 P2 := 
    Let E2 := val_is_empty P2 in
    If_ val_not E2 Then
       Let B1 := val_get_tl P1 in
       Let F2 := val_get_hd P2 in
       Let X2 := val_get_hd F2 in
       Let N2 := val_get_tl F2 in
       Let B2 := val_get_tl P2 in
       val_set_tl P1 B2 ;;
       val_set_hd B1 X2 ;;
       val_set_tl B1 N2 ;;
       val_set_tl P2 F2
    End.

Lemma rule_transfer : forall p1 p2 L1 L2,
  triple (val_transfer p1 p2)
    (MQueue L1 p1 \* MQueue L2 p2)
    (fun r => \[r = val_unit] \* MQueue (L1 ++ L2) p1 \* MQueue nil p2).
Proof using.
  xcf. xapps. xapps. xif ;=> C; fold_bool; logics. (* todo cleanup *)
  { unfold MQueue. xpull ;=> pf2 pb2 vx2 vy2 pf1 pb1 vx1 vy1.
    destruct L2 as [|x L2']; tryfalse.
    rewrite MListSeg_cons_eq. xpull ;=> pf2'.
    xapps. xapps. xapps. xapps.
    xapps~. xapps~. xapps~. xapps~. xapps~.
    intros r. hchange (MListSeg_last pf1). 
    hchange (MListSeg_concat pf1 pf2' pb2). rew_list.
    hchange (MListSeg_nil pf2). hsimpl~. } 
  { subst. rew_list. xvals~. }
Qed.


